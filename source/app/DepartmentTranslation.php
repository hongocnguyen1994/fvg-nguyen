<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentTranslation extends Model
{
    //
    public $timestamps = false;
	protected $fillable = ['name', 'description'];
    public function addNew($department_id,$data) {
        $department = new DepartmentTranslation;
        $department->department_id = $department_id;
        $department->name = $data->name;
        $department->description = $data->description;
        $department->locale = app()->getLocale();
		$department->save();
    }
    public function editDepartment($department_id,$data) {
        $department = self::where('department_id', $department_id)->where('locale', app()->getLocale())->get();
        if (sizeof($department) == 0){
            $department =new DepartmentTranslation;
            $department->department_id = $department_id;
            $department->name = $data->name;
            $department->description = $data->description;
            $department->locale = app()->getLocale();
            $department->save();
        } else {
            $department = $department[0];
			$department->name = $data->name;
			$department->description = $data->description;
			$department->save();
        }
    }
}

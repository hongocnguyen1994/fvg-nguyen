<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultiImage extends Model
{
    public static function addImage($url, $model, $model_id, $size, $status)
    {
        $image = new self();
        $image->image = $url;
        $image->model_type = $model;
        $image->model_id = $model_id;
        $image->size = $size;
        $image->position = self::getNewPosition($model, $model_id);
        $image->status = $status;
        $image->save();
    }

    public static function getNewPosition($model, $id)
    {
        $images = MultiImage::where('model_type', $model)
            ->where('model_id', $id)
            ->orderBy('position', 'desc')
            ->first();
        if(empty($images)){
            $position = 1;
        } else {
            $position = $images->position + 1;
        }
        return $position;
    }

}

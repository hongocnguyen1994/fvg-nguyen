<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoCompanyTranslation extends Model
{
    //
    public $timestamps = false;
    public function InfoCompany ()
    {
        return $this->belongsTo('App\InfoCompany', 'info_id','id');
    }
}

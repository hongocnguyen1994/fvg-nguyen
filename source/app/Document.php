<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Document extends Model
{
    //
    use SoftDeletes;
    public function Category() {
    	return $this->belongsTo(DocumentCategory::class,'category_id','id');
    }
}

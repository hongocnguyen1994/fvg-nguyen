<?php

namespace App;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    use Translatable;



    const ACTIVE = 1;
    const UNACTIVE = 2;
    const PAGINATE = 10;

    const UNPUBLISH = 0;
    const PUBLISH_NOW = 1;

    protected $dates = ['deleted_at'];
    public $translatedAttributes = ['name', 'description', 'content'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function images()
    {
        return $this->morphMany(MultiImage::class, 'model');
    }

    public static function getListPosts()
    {
        return self::with('category')->orderBy('category_id', 'desc')->orderBy('position', 'asc')->paginate(self::PAGINATE);
    }

    public static function getListWithCateLimit($cateId, $quantity)
    {
        return self::where('category_id', '=', $cateId)->limit($quantity)->get();
    }

    public static function getNewPosition($categoryId)
    {
        $post = self::where('category_id', $categoryId)
            ->orderBy('position', 'desc')
            ->first();
        if(empty($post)){
            $position = 1;
        } else {
            $position = $post->position + 1;
        }
        return $position;
    }

    public static function create($data)
    {
        $post = new self();
        $post->category_id = $data->category;
        if(!empty($data->image)){
            $post->image = $data->image;
        } else {
            $post->image = 'default-featured-image.png';
        }
        $post->status = self::ACTIVE;
        $post->position = self::getNewPosition($data->category);
        $post->save();

        PostTranslation::create($data, $post->id);
    }

    public static function edit($data, $id)
    {
        $post = self::getPostFromID($id);
        $post->category_id = $data->category;
        if(!empty($data->image)){
            $post->image = $data->image;
        }
        $post->status = self::ACTIVE;
        $post->save();

        PostTranslation::edit($data, $id);
    }

    public static function getPostFromID($id)
    {
        return self::findOrFail($id);
    }

    public static function publish($id)
    {
        $post = self::getPostFromID($id);
        $post->published_at = Carbon::now();
        $post->save();
    }

    public static function unpublish($id)
    {
        $post = self::getPostFromID($id);
        $post->published_at = null;
        $post->save();
    }

    public static function search($keyword, $category)
    {
        $keyword = !empty($keyword) ? $keyword : "";

        $posts = self::whereIn('id', function ($query) use ($keyword) {
           $query->from('post_translations')->select('post_id')
                ->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%'.$keyword.'%')
                        ->orWhere('description', 'like', '%'.$keyword.'%')
                        ->orWhere('content', 'like', '%'.$keyword.'%');
                });
        });

        if(!empty($category) && $category != 0){
            $posts = $posts->where('category_id', $category);
        }

        return $posts->with('category')->orderBy('id', 'desc')->paginate(self::PAGINATE);
    }


    public static function getListDeletedPost()
    {
        return self::onlyTrashed()->paginate(self::PAGINATE);
    }

    public static function getDeletedPostFromID($id)
    {
        return self::onlyTrashed()->findOrFail($id);
    }



}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'type', 'description', 'detail'];


    public static function create($data, $productID)
    {
        $product = new self();
        $product->product_id = $productID;
        $product->name = $data->name;
        $product->type = $data->type;
        $product->description = $data->description;
        $product->detail = $data->detail;
        $product->locale = app()->getLocale();
        $product->save();
    }

    public static function edit($data, $productID)
    {

        if(!Product::getProductFromID($productID)->hasTranslation(app()->getLocale())){
            $product = new self();
            $product->product_id = $productID;
            $product->name = $data->name;
            $product->type = $data->type;
            $product->description = $data->description;
            $product->detail = $data->detail;
            $product->locale = app()->getLocale();
            $product->save();
        } else {
            $product = self::where('product_id', $productID)->where('locale', app()->getLocale())->firstOrFail();
            $product->name = $data->name;
            $product->type = $data->type;
            $product->description = $data->description;
            $product->detail = $data->detail;
            $product->save();
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;
use Carbon\Carbon;

class DocumentCategory extends Model
{
    //
    use Translatable;
    use SoftDeletes;
    public $translatedAttributes = ['name','description'];
    public function Translations()
    {   
        
        return $this->hasMany('App\DocumentCategoryTraslation','document_category_id','id');
    }
    public function index() {
        return view ('admin.document.document_category');
    }
    public function changestatus(){
        $this->status = abs(1-$this->status);
        $this->save();
    }



}

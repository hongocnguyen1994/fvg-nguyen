<?php

namespace App\Mail;

use App\ContactReply;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyContact extends Mailable
{
    use Queueable, SerializesModels;
    protected $reply;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactReply $reply)
    {
        $this->reply = $reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.contact.reply_mail')->with('reply', $this->reply);
    }
}

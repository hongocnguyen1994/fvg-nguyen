<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
	public $timestamps = false;
	protected $fillable = ['name', 'description'];

	public static function create($data, $categoryID)
	{
		$category = new self();
		$category->name = $data->name;
		$category->description = $data->description;
		$category->category_id = $categoryID;
		$category->locale = app()->getLocale();
		$category->save();
	}

	public static function edit($data, $categoryID)
	{

		if(!Category::getCategoryFromID($categoryID)->hasTranslation(app()->getLocale())){
			$category = new self();
			$category->name = $data->name;
			$category->description = $data->description;
			$category->category_id = $categoryID;
			$category->locale = app()->getLocale();
			$category->save();
		} else {
			$category = self::where('category_id', $categoryID)->where('locale', app()->getLocale())->firstOrFail();
			$category->name = $data->name;
			$category->description = $data->description;
			$category->save();
		}
	}
}

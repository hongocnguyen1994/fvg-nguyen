<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactReply extends Model
{
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public static function reply($data, $id)
    {
        // self::where('contact_id', '<>', $id)->firstOrFail();
        $reply = new self();
        $reply->content = $data->content;
        $reply->contact_id = $id;
        $reply->save();
        return $reply;
    }
}

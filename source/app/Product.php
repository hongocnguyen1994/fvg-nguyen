<?php

namespace App;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
	use SoftDeletes;
	use Translatable;

	const ACTIVED = 1;
    const UNACTIVE = 2;
	const HOT = 1;
	const NOTHOT = 0;
	const PAGINATE = 10;

	protected $dates = ['deleted_at'];
	protected $fillable = ['code'];
	public $translatedAttributes = ['name', 'type', 'description', 'detail'];


	public function category()
	{
		return $this->belongsTo(ProductCategory::class,'product_category_id','id');	
	}

	public function images()
	{
		return $this->morphMany(MultiImage::class, 'model');
	}

	public static function getListProducts()
	{
		return self::with('category')->orderBy('product_category_id', 'desc')->orderBy('position', 'asc')->paginate(self::PAGINATE);
	}

	public static function getListWithCateLimit($cateId, $quantity)
	{
		return self::where('product_category_id', '=', $cateId)->limit($quantity)->get();
	}

	public static function getNewPosition($categoryId)
	{
		$product = self::where('product_category_id', $categoryId)
		->orderBy('position', 'desc')
		->first();
		if(empty($product)){
			$position = 1;
		} else {
			$position = $product->position + 1;
		}
		return $position;
	}

	public static function create($data)
	{
		$product = new self();
		$product->product_category_id = $data->product_category;
		$product->code = $data->code;
		if(!empty($data->image)){
			$product->image = $data->image;
		} else {
			$product->image = 'default-featured-image.png';
		}
		$product->is_hot = $data->is_hot;
		$product->status = self::ACTIVED;
		$product->position = self::getNewPosition($data->product_category);
		$product->save();

		ProductTranslation::create($data, $product->id);
	}

	public static function edit($data, $id)
	{
		$product = self::getProductFromID($id);
		$product->product_category_id = $data->product_category;
		$product->code = $data->code;
		if(!empty($data->image)){
			$product->image = $data->image;
		}
		$product->is_hot = $data->is_hot;
		$product->status = self::ACTIVED;
		$product->save();

		ProductTranslation::edit($data, $id);
	}

	public static function getProductFromID($id)
	{
		return self::findOrFail($id);
	}
	
	public static function search($keyword, $category)
	{
		$keyword = !empty($keyword) ? $keyword : "";

		$products = self::whereIn('id', function ($query) use ($keyword) {
			$query->from('product_translations')->select('product_id')
			->where(function ($query) use ($keyword) {
				$query->where('name', 'like', '%'.$keyword.'%')
				->orWhere('description', 'like', '%'.$keyword.'%')
				->orWhere('type', 'like', '%'.$keyword.'%')
				->orWhere('detail', 'like', '%'.$keyword.'%');
			});
		});

		if(!empty($category) && $category != 0){
			$products = $products->where('product_category_id', $category);
		}

		return $products->with('category')->orderBy('id', 'desc')->paginate(self::PAGINATE);
	}


	public static function getListDeletedProduct()
	{
		return self::onlyTrashed()->paginate(self::PAGINATE);
	}

	public static function getDeletedProductFromID($id)
	{
		return self::onlyTrashed()->findOrFail($id);
	}

}

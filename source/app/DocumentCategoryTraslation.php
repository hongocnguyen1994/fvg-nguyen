<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentCategoryTraslation extends Model
{
    //
    public $timestamps = false;
    public function addNew($category_id,$data) {
        $documentCategory = new DocumentCategoryTraslation;
        $documentCategory->document_category_id = $category_id;
        $documentCategory->name = $data->name;
        $documentCategory->description = $data->description;
        $documentCategory->locale = app()->getLocale();
		$documentCategory->save();
    }
    public function editCategory($category_id,$data) {
        $documentCategory = self::where('document_category_id', $category_id)->where('locale', app()->getLocale())->get();
        if (sizeof($documentCategory) == 0){
            $documentCategory =new DocumentCategoryTraslation;
            $documentCategory->document_category_id = $category_id;
            $documentCategory->name = $data->name;
            $documentCategory->description = $data->description;
            $documentCategory->locale = app()->getLocale();
            $documentCategory->save();
        } else {
            $category = $documentCategory[0];
			$category->name = $data->name;
			$category->description = $data->description;
			$category->save();
        }
    }
}

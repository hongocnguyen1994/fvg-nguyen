<?php 
function Recursion($data, $parent_id, $str, $select = 0)
{
	foreach ($data as $value) {
		$id = $value->id;
		$name = $value->name;
		if ($value->parent_id == $parent_id) {
			if ($select != 0 && $id == $select) {
				echo '<option class="category" value="'.$id.'" selected>'.$str.$name.'</option>';
			} else {
				echo '<option class="category" value="'.$id.'">'.$str.$name.'</option>';
			}
			Recursion($data, $id, $str."-- ", $select);
		}
	}
}
function RecursionEit($data, $parent_id, $str, $select = 0, $cateEditId)
{
	foreach ($data as $value) {
		$id = $value->id;
		$name = $value->name;
		if ($value->parent_id == $parent_id && $id != $cateEditId) {
			if ($select != 0 && $id == $select) {
				echo '<option class="category" value="'.$id.'" selected>'.$str.$name.'</option>';
			} else {
				echo '<option class="category" value="'.$id.'">'.$str.$name.'</option>';
			}
			Recursion($data, $id, $str."-- ", $select);
		}
	}
}
function ListCate($data, $parent, $str) {
	foreach ($data as $value) {
        if(!$value->hasTranslation('vi')){
            $lang = 'en';
        } else if(!$value->hasTranslation('en')){
            $lang = 'vi';
        } else {
            $lang = app()->getLocale();
        }
		$id = $value->id;
		$name = $value->{'name:'.$lang};
		if ($value->parent_id == $parent) {
			echo '
			<tr>';
			if ($value->parent_id == 0) {
				echo '<td><a href="'.url("category/$id").'"><b>'.$str.$name.'</b></a></td>';
			} else {
				echo '<td><a href="'.url("category/$id").'">'.$str.$name.'</a></td>';
			}
			echo
				'<td>'.$value->{'description:'.$lang}.'</td>						
				<td>'.\Carbon\Carbon::createFromTimestamp(strtotime($value->created_at))->diffForHumans().'</td>
				<td>'.$value->updated_at.'</td>
				<td>';
				if ($value->status == 1) {
					echo '<span class="label label-success">'.trans('admin.status.active').'</span>';
				} else {
					echo '<span class="label label-danger">Unactive</span>';
				}
				echo
				'</td>
				<td><a href="'. route('category.edit', $value).'" class="btn btn-warning">Edit</a></td>
				<td>
					<form action="'.route('category.destroy', $value).'" method="post">
						'. csrf_field() .'
						'. method_field("delete").'
						<button type="submit" class="btn btn-danger delete">Delete</button>
					</form>
				</td>
				<td>';
					if ($value->hasTranslation(app()->getLocale())) {
						echo 'Available';
					} else {
						echo 'Unavailable';
					}
                echo '</td>
			</tr>
			';
			ListCate($data, $id, $str."* ");
		}
		
	}
}
function ListSearch($data, $str) {
	foreach ($data as $value) {
		$id = $value->id;
		$name = $value->name;
			echo '
			<tr>';
			if ($value['parent_id'] == 0) {
				echo '<td><a href="'.url("category/$id").'"><b>'.$str.$name.'</b></a></td>';
			} else {
				echo '<td><a href="'.url("category/$id").'">'.$str.$name.'</a></td>';
			}
			echo
				'<td>'.$value->description.'</td>						
				<td>'.$value->created_at.'</td>
				<td>'.$value->updated_at.'</td>
				<td>';
				if ($value->status == 1) {
					echo '<span class="label label-success">Active</span>';
				} else {
					echo '<span class="label label-danger">Unactive</span>';
				}
				echo
				'</td>
				<td><a href="'. route('category.edit', $value).'" class="btn btn-warning">Edit</a></td>
				<td>
					<form action="'.route('category.destroy', $value).'" method="post">
						'. csrf_field() .'
						'. method_field("delete").'
						<button type="submit" class="btn btn-danger delete">Delete</button>
					</form>
				</td>
				<td>';
					if ($value->hasTranslation(app()->getLocale())) {
						echo 'Available';
					} else {
						echo 'Unavailable';
					}
                echo '</td>
			</tr>
			';
	}
}
?>
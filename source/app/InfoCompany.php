<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;
use Carbon\Carbon;

class InfoCompany extends Model
{
    //
    use Translatable;
    public $translatedAttributes = ['address','office'];
    public function Translations()
    {   
        
        return $this->hasMany('App\InfoCompanyTranslation','info_id','id');
    }
}

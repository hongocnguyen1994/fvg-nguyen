<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'content'];


    public static function create($data, $postID)
    {
        $post = new self();
        $post->name = $data->name;
        $post->description = $data->description;
        $post->content = $data->content;
        $post->post_id = $postID;
        $post->locale = app()->getLocale();
        $post->save();
    }

    public static function edit($data, $postID)
    {

        if(!Post::getPostFromID($postID)->hasTranslation(app()->getLocale())){
            $post = new self();
            $post->name = $data->name;
            $post->description = $data->description;
            $post->content = $data->content;
            $post->post_id = $postID;
            $post->locale = app()->getLocale();
            $post->save();
        } else {
            $post = self::where('post_id', $postID)->where('locale', app()->getLocale())->firstOrFail();
            $post->name = $data->name;
            $post->description = $data->description;
            $post->content = $data->content;
            $post->save();
        }
    }


}

<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'user_id', 'content', 'fileUpload'
    ];
    protected $dates = ['deleted_at'];

    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public  function Comment() {
        return $this->hasMany(Comment::class,'message_id','id');
    }
}

<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;

class ClientController extends Controller
{
     public function __construct(Post $post, Category $category)
    {
        $this->post = $post;
        $this->category = $category;
    }
    public function index(){
    	return view('client.index');
    }

    public function showPost($category, $slug, $id){

        $post = $this->post->getPostFromID($id);
        $category = $this->category->getCategoryFromID($post->category_id);
        if(count($category->posts) == 1){
            return view('client.show-post', compact('post'));
        } else {
            return view('client.show-post-with-sidebar', compact('post', 'category'));
        }
    }
}

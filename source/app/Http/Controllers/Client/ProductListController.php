<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
class ProductListController extends Controller
{
    public function view(Request $request) {
        $category_id = $request->category_id; 
        $product = $this->getProducthot($category_id);
        $category = $this->getCategory($category_id);
    	return view('client.productList')->with(['listproduct'=>$product,'category'=>$category]);
    }

    public function getProducthot($category_id){
        $product = App\Product::where('product_category_id',$category_id)->get();
        return $product;
    }

    public function getCategory($category_id){
        $category = App\ProductCategory::where('id',$category_id)->get();
        return $category[0];
    }
}

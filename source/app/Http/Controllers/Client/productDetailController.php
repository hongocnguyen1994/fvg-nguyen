<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App; 
use DB;
class productDetailController extends Controller
{
    public function index(Request $request){
        $product_id  = $request->id;
        $category_id = App\Product::select('product_category_id')->where('id',$product_id)->get();
        $category_id = $category_id[0]->product_category_id;
        $listproduct = $this->getProducthot($category_id);
        $category = $this->getCategory($category_id);
        $product = App\Product::find($product_id);
    	return view('client.productDetail')->with(['listproduct'=>$listproduct,'category'=>$category,'product'=>$product]);
    }

    public function getProducthot($category_id){
        $product = App\Product::where('product_category_id',$category_id)->get();
        return $product;
    }

    public function getCategory($category_id){
        $category = App\ProductCategory::where('id',$category_id)->get();
        return $category[0];
    }
}
    
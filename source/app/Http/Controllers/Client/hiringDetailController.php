<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class hiringDetailController extends Controller
{
    public function view($id) {
    	$posts = DB::table('posts')	->where('post_translations.id', '=', $id)
    								->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
    								->select('posts.*', 'post_translations.*')
    								->get();

    	$postsRelate = DB::table('posts')	->where([
    											['category_id', '=', 1],
    											['post_translations.id', '<>', $id],
											])
    										->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
    										->select('posts.*', 'post_translations.*')
    										->orderBy('published_at','DESC')
    										->limit(3)
    										->get();
    	return view('client.hiringDetail')->with('posts', $posts)
    									->with('postsRelate', $postsRelate);
    }
}

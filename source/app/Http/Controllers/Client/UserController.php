<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use DB;
class UserController extends Controller
{
    //
    public $list = 15;
    public $limit = 5;
    public function loadStaffBar(){
        $user = DB::table('users')->limit(5)->get();
        return $user;
    }

    public function getStaffActive(Request $request) {
      
        $department_id = $request->department_id;
        $last_staff = $request->last_staff;
        $staff = App\User::join('departments','users.department_id','=','departments.id')->where('departments.id',$department_id)->where('users.id','>',$last_staff)->select('users.*')->limit($this->list)->get();   
        return ($staff);
    }
}

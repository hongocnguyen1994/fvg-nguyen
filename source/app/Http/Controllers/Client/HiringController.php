<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HiringController extends Controller
{
    //
    public function index(){
    	$posts = DB::table('posts')	->where([
                                        ['category_id', '=', 1],
                                        ['published_at', '<>', NULL],
                                    ])
    								->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
    								->select('posts.*', 'post_translations.*')
    								->orderBy('published_at','DESC')
    								->limit(3)
    								->get();
    	// return response()->json(['data'=>$posts]);
        return view('client/hiring-list')->with('posts',$posts);
    }
    public function loadDataAjax(Request $request) {
        $id = $request->id;
        $posts = DB::table('posts')	->where([
    									['category_id', '=', 1],
    									['published_at', '<', $id],
                                        ['published_at', '<>', NULL],
    								])
    								->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
    								->select('posts.*', 'post_translations.*')
    								->orderBy('published_at','DESC')
    								->limit(3)
    								->get();
    	return response()->json(['data'=>$posts]);
    }

}

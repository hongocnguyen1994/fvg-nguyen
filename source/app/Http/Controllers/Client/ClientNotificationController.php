<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class ClientNotificationController extends Controller
{
    //
    
    public function run() {
        $checkpoint = $this->CheckNotificationPoint();
        $data = $this->checkNotification($checkpoint);
        $this->SendEvent($data);
    }

    public function CheckNotificationPoint () {
        
        $result = DB::table('notifications')->where('user_id',Auth::user()->id)->distinct()->get();
        if (sizeof($result) <= 0) {
            $notification =new App\Notification;
            $notification->user_id = Auth::user()->id;
            $notification->checked_at = Carbon::now();
            $notification->save();
            $result = DB::table('notifications')->where('user_id',Auth::user()->id)->distinct()->get();
        }

        return $result[0]->checked_at;
    }

    public function checkNotification($checkpoint){
        
        $count = 0;
        $timepoint =new Carbon($checkpoint);
        $user_id = Auth::user()->id;
        $message = DB::select("SELECT users.avatar,users.first_name,users.last_name,messages.id
                                    FROM messages 
                                    INNER JOIN users ON users.id = messages.user_id
                                    WHERE messages.created_at > '$timepoint' AND user_id <> $user_id"); 
        $count += sizeof($message);

        //New Comment on User's message
        $UsersMessage = DB::table('messages')->where('user_id',Auth::user()->id)->get();
        $comments = array();
        foreach($UsersMessage as $Message){

            $comments[$Message->id] = DB::select("SELECT users.avatar,users.first_name,users.last_name,message_id,comments.id 
                                                FROM comments 
                                                INNER JOIN users ON comments.user_id = users.id 
                                                WHERE comments.created_at > '$timepoint' AND comments.message_id = $Message->id And comments.user_id <> $user_id"); 

            $count += sizeof($comments[$Message->id]);
        }
        
        // //New Post 
        $documents = DB::table('documents')->where('created_at','>',$timepoint)->get();
        $count += sizeof($documents);
        return ['messages'=>$message,'comments'=>$comments,'documents'=>$documents,'count'=>$count];
    }
    public function SendEvent($data) {
        header('Connection: keep-alive');
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        echo "data:".json_encode($data)."\n\n";
        flush();
    }
}

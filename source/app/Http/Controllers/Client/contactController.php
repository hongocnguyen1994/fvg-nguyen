<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
class contactController extends Controller
{
    public function view() {
    	return view('client.contact');
    }

    public function clientSend(Request $request) {
        $contact = new App\Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->title = $request->title;
        $contact->content = $request->content;
        $contact->save();
    }
}
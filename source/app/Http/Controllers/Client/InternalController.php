<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App;
use Illuminate\Support\Facades\Auth;
class InternalController extends Controller
{
    //
    public $MessageLimit = 4;
    public function index(){
        return view('client.internal');
    }

    //Message's Functions and Comment's Functions
    public function loadMessage(Request $request) {
        $MessageLimit = $this->MessageLimit;
        $LastMessage = $request->LastMessage;
        $lastId = $LastMessage;
        if ($LastMessage != Null) {
            $result = App\Message::join('users','users.id','=','messages.user_id')->where('messages.id','<',$LastMessage)
                                    ->select('users.avatar','users.first_name','users.last_name','messages.*')
                                    ->orderBy('messages.id','desc')->limit($MessageLimit)->get();
        } else {
            $result = App\Message::join('users','users.id','=','messages.user_id')
                                    ->select('users.avatar','users.first_name','users.last_name','messages.*')
                                    ->orderBy('messages.id','desc')->limit($MessageLimit)->get();
        }
        if (count($result)) {
        $lastId = $result[sizeof($result)-1]->id;
        }
        return response()->json(['data'=>$result,'lastId'=>$lastId]);
    }

    public function loadCommentlv1(Request $request) {
        $result = DB::select("SELECT comments.id,users.first_name,users.last_name,users.avatar,user_id,message_id,comment_id,content,comments.created_at 
                                FROM comments 
                                INNER JOIN users ON users.id = comments.user_id
                                WHERE message_id = $request->message_id AND comment_id is null");
        return $result;
    }

    public function loadCommentlv2(Request $request){
        $result = DB::select("SELECT comments.id,users.first_name,users.last_name,users.avatar,user_id,message_id,comment_id,content,comments.created_at 
                                FROM comments 
                                INNER JOIN users ON users.id = comments.user_id
                                WHERE comment_id = $request->comment_id");
        return $result;
    }
    
    public function addMessage(Request $request) { 
        $message = new App\Message;
        $message->content = $request->content;
        $file = $request->file('fileUpload');
        $message->user_id = Auth::user()->id;
        if ($file != NULL) {
            $dir = 'public/files/';
            $OriginalName = $file->getClientOriginalName();
            $filename = uniqid().'_'.time().'_'.date('Ymd').'.ORIGINALNAME.'.$OriginalName;
            $message->fileUpload = $filename;
            $file->storeAs($dir,$filename);
        }
        $message->save();
    }

    public function Comment(Request $request) {
        $comment = new App\Comment;
        $comment->user_id = Auth::user()->id;
        $comment->message_id = $request->message_id;
        $comment->comment_id = $request->comment_id;
        if ($comment->comment_id != "null"){
            $message_id  = DB::select(" SELECT message_id FROM comments WHERE id = $request->comment_id");
            $comment->message_id = $message_id[0]->message_id;
        } else {
            $comment->comment_id = null;
        }
        $comment->content = $request->content;
        $comment->save();
    }

    public function deleteMessage(Request $request){
        $message = App\Message::find($request->message_id);
        if ($message->user_id == Auth::user()->id) {
            $message->delete();
        }
    }

    public function editMessage(Request $request){
        $message = App\Message::find($request->Message_id);
        $message->content = $request->content;
        $message->save();
    }
    
    
    public function listEmployee(){
        $Employees = App\User::All();
        return $Employees;
    }

    //Comment
    

    public function SubComment(Request $request){
        $result = DB::select("SELECT comments.id,users.last_name,user_id,message_id,comment_id,content,comments.created_at 
                                FROM comments 
                                INNER JOIN users ON users.id = comments.user_id 
                                WHERE comment_id = $request->id");
        return $result;
    }
    public function CommentToMessage(Request $request){
        $comment = new App\Comment;
        $comment->user_id = Auth::user()->id;
        $comment->message_id = $request->message_id;
        $comment->content = $request->content;
        $comment->save();
    }

    public function replyComment(Request $request){
        $message_id = DB::select("SELECT message_id FROM comments WHERE id = $request->comment_id ");
        $comment = new App\Comment;
        $comment->user_id = Auth::user()->id;
        $comment->message_id =$message_id[0]->message_id;
        $comment->comment_id = $request->comment_id;
        $comment->area = 2;
        $comment->content = $request->content;
        $comment->save();
        return $comment;
    }

    public function searchDocument(Request $request){
        $category_id = $request->category_id;
        $character = $request->character;
        $Document = App\Document::where('category_id',$category_id)->where('name', 'like', "%$character%")->get();
        return $Document;
    }

    public function searchUser(Request $request){
        $department_id = $request->department_id;
        $character = $request->character;
        $User = DB::select("SELECT * FROM users Where deleted_at is null and  department_id = $department_id AND (first_name like '%$character%' OR last_name like '%$character%' OR email like '%$character%') ");
        return $User;
    }
}  
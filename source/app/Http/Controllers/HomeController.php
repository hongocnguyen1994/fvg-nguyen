<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = DB::table('partners')->select('partners.*')->get();
        $brand_product = App\Post::where('category_id',4)->get();
        $brand_intro = App\Post::where('category_id',5)->get();
        //return view('client.homepage');
        return view('client.homepage')->with(['partners'=> $partners,'brand_product'=>$brand_product,'brand_intro'=>$brand_intro]);
    }
}

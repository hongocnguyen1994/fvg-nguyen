<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use File;
use App\partners;
use Illuminate\Support\Facades\Storage;

class PartnerController extends Controller
{
    public function index() {
    	$partners = DB::table('partners')	->select('partners.*')
    										->get();
    	return view('admin.partner.index')->with('partners', $partners);
    }

    public function create() {
    	return view('admin.partner.create');
    }

    public function submit(Request $request) {
    	$this->validate($request, [
    		'name' => 'required',
    		'link' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
    		'image' => 'required',
    	]);

    	$image = "";
        if($request->hasFile('image')) {
            $file = $request->image;
            $fileName = strtotime('now') . '_' . $file->getClientOriginalName();
            Storage::putFileAs(
                'public/', $request->file('image'), $fileName
            );
            $image = $fileName;
        }

    	$partner = new partners;

    	$partner->name = $request->input('name');
    	$partner->link = $request->input('link');
    	$partner->image = $image;

    	$partner->save();

    	return redirect()->route('admin.partner.index');
    }

    public function destroy($id) {
    	DB::table('partners')->where('id', '=', $id)->delete();
    	return redirect()->back();
    }

    public function edit($id) {
    	$partner = partners::find($id);

        return view('admin.partner.edit', compact('partner'));
    }

    public function editSubmit(Request $request, $id) {
    	$this->validate($request, [
    		'name' => 'required',
    		'link' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
    	]);

    	$partner = partners::find($id);

        $image = "";
        if($request->hasFile('image')) {
            $image_path = 'storage/'.$partner->image;
            File::delete($image_path);

            $file = $request->image;
            $fileName = strtotime('now') . '_' . $file->getClientOriginalName();
            Storage::putFileAs(
                'public', $request->file('image'), $fileName
            );
            $image = $fileName;
        }

    	$partner->name = $request->input('name');
    	$partner->link = $request->input('link');
    	if($request->hasFile('image')) {
            $partner->image = $image;
        }
    	$partner->update();

    	return redirect()->route('admin.partner.index');
    }

    public function show(Request $request, $id) {
        $partners = partners::find($id);
        return view('admin.partner.show')->with('partners', $partners);
    }

}

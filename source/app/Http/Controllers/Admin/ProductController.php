<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class ProductController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->getListProducts();
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->product->create($request);
        return redirect()->route('admin.product.index')->with('product_success', 'Ok')->with('success',
            ['type' => 'success', 'message' => trans('admin.message.product.create'), 'title' => 'Message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->getProductFromID($id);
        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product->getProductFromID($id);
        Session::flash('url.intended', URL::previous());
        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $this->product->edit($request, $id);
        if(Session::has('url.intended')){
            return redirect()->intended(Session::get('url.intended'))->with('success',
                ['type' => 'success', 'title' =>  'Message', 'message' => trans('admin.message.product.edit')]);
        } else {
            return redirect()->back()->with('success',
                ['type' => 'success', 'title' =>  'Message', 'message' => trans('admin.message.product.edit')]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product->getProductFromID($id)->delete();
        return redirect()->back()->with('success',
            ['type' => 'success', 'message' => trans('admin.message.product.delete'), 'title' => 'Message']);
    }

    public function search(Request $request)
    {
        $products = $this->product->search($request->keyword, $request->category);
        return view('admin.product.search', compact('products', 'request'));
    }

    public function getDeletedProduct()
    {
        $products = $this->product->getListDeletedProduct();
        return view('admin.product.restore', compact('products'));
    }

    public function restore($id)
    {
        $this->product->getDeletedProductFromID($id)->restore();
        return redirect()->back()->with('success',
            ['type' => 'success', 'message' => trans('admin.message.product.restore'), 'title' => 'Message']);
    }

    public function productPosition(Request $request)
    {
        $product = Product::getProductFromID($request->id);
        $product->position = $request->position;
        $product->save();
    }
}

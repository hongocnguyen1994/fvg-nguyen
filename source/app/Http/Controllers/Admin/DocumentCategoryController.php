<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use DB;
class DocumentCategoryController extends Controller
{
    //
    public function index() {
        return view ('admin.document.document_category');
    }

    public function addCategory(Request $request) {
        $maxRecord = DB::Select("SELECT COUNT(id) as COUNT FROM document_categories");
        $Category = new App\DocumentCategory;
        //$Category->name = $request->name;
        $Category->position = $maxRecord[0]->COUNT;
        $Category->save();
        $CategoryTranslation = new App\DocumentCategoryTraslation;
        $CategoryTranslation->addNew($Category->id,$request);
    }

    public function changestatus(Request $request){
        $category = App\DocumentCategory::find($request->id);
        $category->changestatus();
    }
    public function delete(Request $request) {
        $category = App\DocumentCategory::find($request->id); 
        $category->delete();
    }
    public function update(Request $request){
        $category = App\DocumentCategory::find($request->id);
        $category->position = $request->position;
        $category->save();
        $CategoryTranslation = new App\DocumentCategoryTraslation;
        $CategoryTranslation->editCategory($category->id,$request);
    }
    public function trash(){
        $category = App\DocumentCategory::onlyTrashed()->get();
        return response()->json(['data'=>$category]);
    }
    public function restore(Request $request) {
        $category = App\DocumentCategory::onlyTrashed()->find($request->id); 
        $category->restore();
    }
}

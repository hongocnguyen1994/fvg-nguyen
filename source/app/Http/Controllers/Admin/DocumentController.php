<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
class DocumentController extends Controller
{
    //
    public function index() {
        return view('admin.document.document');
    }

    public function add(Request $request) {
        $document = new App\Document;
        $document->name = $request->name;
        $file = $request->file('fileUpload');
        $document->category_id =  $request->category_id;
        $dir = 'public/files/';
        $OriginalName = $file->getClientOriginalName();
        $filename = uniqid().'_'.time().'_'.date('Ymd').'.ORIGINALNAME.'.$OriginalName;
        $file->storeAs($dir,$filename);
        $document->filename = $filename;
        $document->save();
    }
    public function edit(){
        ;
    }

}

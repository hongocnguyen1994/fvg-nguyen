<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\ContactReply;
use App\Mail\ReplyContact;
use Illuminate\Support\Facades\Mail;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::getListContact();
        return view('admin.contact.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::getContactFromID($id);
        return view('admin.contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::getContactFromID($id)->delete();
        return redirect()->back()->with('success', [
            'type' => 'success', 'message' => trans('admin.message.delete_contact'), 'title' => 'Message'
        ]);
    }
    public function postReply(Request $request)
    {
        $reply = ContactReply::reply($request, $request->id);
        $contact = Contact::getContactFromID($request->id);
        $contact->status = Contact::REPLIED;
        $contact->save();
        $replyMail = new ReplyContact($reply);
        Mail::to($contact->email)->send($replyMail);
        // Mail::send('mailfb', array('name'=>$input["name"],'email'=>$input["email"], 'content'=>$input['comment']), function($message){
        //     $message->to('plachym.it@gmail.com', 'Visitor')->subject('Visitor Feedback!');
        // });
        return redirect()->back()->with('success', [
            'type' => 'success', 'message' => trans('admin.mail.reply_done_message'), 'title' => 'Message'
        ]);
    }
}

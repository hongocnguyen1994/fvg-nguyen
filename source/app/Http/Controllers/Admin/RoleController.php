<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateEditRoleRequest;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->role->getListRoles();
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditRoleRequest $request)
    {
        $this->role->create($request->all());
        return redirect()->route('admin.role.index')->with('success',
            ['type' => 'success', 'message' => 'Add Role Done ! ', 'title' => 'Message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = $this->role->getRoleFromID($id);
        return view('admin.role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->role->getRoleFromID($id);
        return view('admin.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditRoleRequest $request, $id)
    {
        $role = $this->role->getRoleFromID($id);
        $role->update($request->all());

        return redirect()->route('admin.role.index')->with('success',
            ['type' => 'success', 'message' => 'Update Role Done ! ', 'title' => 'Message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->role->getRoleFromID($id)->delete();

        return redirect()->route('admin.role.index')->with('success',
            ['type' => 'success', 'message' => 'Delete Role Done! ', 'title' => 'Message']);
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use DB;
class DepartmentController extends Controller
{
    //
    public function index() {
        return view('admin.department.department');
    }

    public function add(Request $request) {
        $maxRecord = DB::Select("SELECT COUNT(id) as COUNT FROM departments");
        $department = new App\Department;      
        $department->position = $maxRecord[0]->COUNT;
        $department->status = 1;
        $department->save();
        $departmentTranslation = new App\DepartmentTranslation;
        $departmentTranslation->addNew($department->id,$request);
    }

    public function list() {
        $department = App\Department::all(); 
        return $department;
    }

    // public function changestatus(Request $request){
    //     $category = App\Department::find($request->id);
    //     $category->changestatus();
    // }
    public function delete(Request $request) {
        $category = App\Department::find($request->id); 
        $category->delete();
    }
    public function update(Request $request){
        $department = App\Department::find($request->id);
        $department->position = $request->position;
        $department->save();
        $departmentTranslation = new App\DepartmentTranslation;
        $departmentTranslation->editDepartment($department->id,$request);
    }

}

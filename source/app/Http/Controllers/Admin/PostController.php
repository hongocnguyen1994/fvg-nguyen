<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateEditPostRequest;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;


class PostController extends Controller
{
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post->getListPosts();
        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditPostRequest $request)
    {
        $this->post->create($request);
        return redirect()->route('admin.post.index')->with('post_success', 'Ok')->with('success',
            ['type' => 'success', 'message' => trans('admin.message.post.create'), 'title' => 'Message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post->getPostFromID($id);
        return view('admin.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post->getPostFromID($id);
        Session::flash('url.intended', URL::previous());
        return view('admin.post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditPostRequest $request, $id)
    {
        $this->post->edit($request, $id);
        if(Session::has('url.intended')){
            return redirect()->intended(Session::get('url.intended'))->with('success',
                ['type' => 'success', 'title' =>  'Message', 'message' => trans('admin.message.post.edit')]);
        } else {
            return redirect()->back()->with('success',
                ['type' => 'success', 'title' =>  'Message', 'message' => trans('admin.message.post.edit')]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->post->getPostFromID($id)->delete();
        return redirect()->back()->with('success',
            ['type' => 'success', 'message' => trans('admin.message.post.delete'), 'title' => 'Message']);
    }

    public function postPublishPost($id)
    {
        $this->post->publish($id);
        return redirect()->back()->with('success',
            ['type' => 'success', 'message' => trans('admin.message.post.publish'), 'title' => 'Message']);
    }

    public function postUnPublishPost($id)
    {
        $this->post->unpublish($id);
        return redirect()->back()->with('success',
            ['type' => 'success', 'message' => trans('admin.message.post.un_publish'), 'title' => 'Message']);
    }

    public function search(Request $request)
    {
        $posts = $this->post->search($request->keyword, $request->category);
        return view('admin.post.search', compact('posts', 'request'));
    }

    public function getDeletedPost()
    {
        $posts = $this->post->getListDeletedPost();
        return view('admin.post.restore', compact('posts'));
    }

    public function restore($id)
    {
        $this->post->getDeletedPostFromID($id)->restore();
        return redirect()->back()->with('success',
            ['type' => 'success', 'message' => trans('admin.message.post.restore'), 'title' => 'Message']);
    }

    public function postPosition(Request $request)
    {
        $post = Post::getPostFromID($request->id);
        $post->position = $request->position;
        $post->save();
    }

}

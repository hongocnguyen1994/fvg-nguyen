<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Requests\ProductCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
    public $category;

    public function __construct(ProductCategory $category)
    {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\readdir()sponse
     */
    public function index()
    {
        $this->category->softCategory($this->category->getListCategories(), null, '');
        $categories = ProductCategory::$lists;
        return view('admin.product-category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->category->softCategory($this->category->getListCategories(), null, '');
        $categories = ProductCategory::$lists;
        return view('admin.product-category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request)
    {
        $this->category->locationInsertion($request->parent_id, $request->position);
        $this->category->create($request);
        return redirect('product-category')->with('success',
            ['type' => 'success', 'message' => 'Created product category success!', 'title' => 'Message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = $this->category->getListCategories();
        $category = $this->category->getCategoryFromID($id);
        return view('admin.product-category.show', compact('category', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->getCategoryFromID($id);
        $data = $this->category->getListCategories();
        $this->category->softCategory($this->category->getListCategories(), null, '');
        $categories = ProductCategory::$lists;
        return view('admin.product-category.edit', compact('category', 'categories'), ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $this->category->getCategoryFromID($id);
        $this->category->locationEdit($request->parent_id, $request->position, $category->id);
        $this->validate($request, [
            'parent_id' => 'nullable|numeric|min:0',
            'name' =>   [ 
                            'required',
                            'string',
                            'max:255',
                            Rule::unique('product_category_translations')->ignore($category->id, 'product_category_id'),
                        ],
            'description' => 'nullable|string',
            'position' => 'nullable|numeric|min:1',
            'status' => 'required|digits_between: 0, 1',
        ]);
        $category->update($request->all());
        return redirect('product-category')->with('success',
            ['type' => 'success', 'message' => 'Update product category success ! ', 'title' => 'Message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->category->getCategoryFromID($id)->delete();
        return redirect()->route('product-category.index')->with('success',
            ['type' => 'success', 'message' => 'Delete category success!', 'title' => 'Message']);
    }

    /**
     * [getDeletedUser description]
     * @return [type] [description]
     */
    public function getDeletedCategories()
    {
        $list = $this->category->getAllCategories();
        $categories = $this->category->getDeletedCategories();
        return view('admin.product-category.restore', compact('categories', 'list'));
    }

    /**
     * [restore description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function restore($id)
    {
        $this->category->getDeletedCategoryFromID($id)->restore();
        return redirect()->route('product-category.index')->with('success',
            ['type' => 'success', 'message' => 'Restore product category success!', 'title' => 'Message']);
    }

    /**
     * [search description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {
        $categories = $this->category->search($request->keyword);
        return view('admin.product-category.search', compact('request', 'categories'));
    }
    public function getCategoriesByParent($parentId){
        if ($parentId == 0) {
            $parentId = null;
        }
        $this->category->softCategory($this->category->getListCategories(), $parentId, '');
        $categories = ProductCategory::$lists;
        return response()->json($categories);
    }
}

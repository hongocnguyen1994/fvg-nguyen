<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\ProductCategory;
use App\Http\Requests\MultiImageRequest;
use App\MultiImage;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Product;
class MultiImageController extends Controller
{

    const THUMB = [
        'width' =>  200,
        'height' => 200
    ];

    const ACTIVE = 1;


    public function getListImage($model, $id)
    {
        switch ($model){
            case 'posts':
            $images = Post::findOrFail($id)->images;
            break;
            case 'categories':
            $images = Category::findOrFail($id)->images;
            break;
            case 'products':
            $images = Product::findOrFail($id)->images;
            break;
            case 'product_categories':
            $images = ProductCategory::findOrFail($id)->images;
            
            break;
            
        }
        return view('admin.image.index', compact('model', 'id', 'images'));
    }

    public function postUploadImage(MultiImageRequest $request)
    {
        $img = Image::make($_FILES['image']['tmp_name']);
        $newName = md5($_FILES['image']['tmp_name'].time());
        $img->save(public_path('assets/images/'.$newName.'.'.$request->image->getClientOriginalExtension()));
//        $img->resize(self::THUMB['width'], null, function ($constraint) {
//            $constraint->aspectRatio();
//        });
        $img->resize(self::THUMB['width'], self::THUMB['height']);
        $img->save(public_path('assets/images/'.'thumb_'.$newName.'.'.$request->image->getClientOriginalExtension()));
        $imagePath = $newName.'.'.$request->image->getClientOriginalExtension();
        MultiImage::addImage($imagePath, $request->model, $request->id, "", self::ACTIVE);
        return redirect()->back()->with('success', [
            'type' => 'success', 'message' => trans('admin.message.upload_image'), 'title' => 'Message'
        ]);
    }

    public function putChangePosition(Request $request)
    {
        foreach ($request->position as $key => $position)
        {
            $image = MultiImage::find($key);
            $image->position = $position;
            $image->save();
        }
        return redirect()->back()->with('success', [
            'type' => 'success', 'message' => trans('admin.message.image_position'), 'title' => 'Message'
        ]);
    }
}

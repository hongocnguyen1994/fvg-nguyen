<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Category;

class CategoryController extends Controller
{
    public $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\readdir()sponse
     */
    public function index()
    {
        $this->category->softCategory($this->category->getListCategories(), null, '');
        $categories = Category::$lists;
        //dd($categories[0]->name);
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->category->softCategory($this->category->getListCategories(), null, '');
        $categories = Category::$lists;
        return view('admin.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $this->category->locationInsertion($request->parent_id, $request->position);
        $this->category->create($request);
        return redirect('category')->with('success',
            ['type' => 'success', 'message' => 'Created category success!', 'title' => 'Message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = $this->category->getListCategories();
        $category = $this->category->getCategoryFromID($id);
        return view('admin.category.show', compact('category', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->getCategoryFromID($id);
        $data = $this->category->getListCategories();
        $this->category->softCategory($this->category->getListCategories(), null, '');
        $categories = Category::$lists;
        return view('admin.category.edit', compact('category', 'categories'), ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $this->category->getCategoryFromID($id);
        $this->category->locationEdit($request->parent_id, $request->position, $category->id);
        $this->validate($request, [
            'parent_id' => 'nullable|numeric|min:0',
            'name' =>   [ 
                            'required',
                            'string',
                            'max:255',
                            Rule::unique('category_translations')->ignore($category->id, 'category_id'),
                        ],
            'description' => 'nullable|string',
            'position' => 'nullable|numeric|min:1',
            'status' => 'required|digits_between: 0, 1',
        ]);
        $category->update($request->all());
        return redirect('category')->with('success',
            ['type' => 'success', 'message' => 'Update category success ! ', 'title' => 'Message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->category->getCategoryFromID($id)->delete();

        return redirect()->route('category.index')->with('success',
            ['type' => 'success', 'message' => 'Delete category success!', 'title' => 'Message']);
    }

    /**
     * [getDeletedUser description]
     * @return [type] [description]
     */
    public function getDeletedCategories()
    {
        $list = $this->category->getAllCategories();
        $categories = $this->category->getDeletedCategories();
        return view('admin.category.restore', compact('categories', 'list'));
    }

    /**
     * [restore description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function restore($id)
    {
        $this->category->getDeletedCategoryFromID($id)->restore();
        return redirect()->route('category.index')->with('success',
            ['type' => 'success', 'message' => 'Restore category success!', 'title' => 'Message']);
    }

    /**
     * [search description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {
        $categories = $this->category->search($request->keyword);
        return view('admin.category.search', compact('request', 'categories'));
    }
    public function getCategoriesByParent($parentId){
        if ($parentId == 0) {
            $parentId = null;
        }
        $this->category->softCategory($this->category->getListCategories(), $parentId, '');
        $categories = Category::$lists;
        return response()->json($categories);
    }

}
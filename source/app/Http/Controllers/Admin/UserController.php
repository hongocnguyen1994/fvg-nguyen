<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =  $this->user->getListUsers();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
        $code = md5($request->username.time());
        $code = substr($code, 0, 12);
        $password = bcrypt($code);
        $request->request->add(['phone' => $code, 'password' => $password]);
        User::create($request->all());
        return redirect()->route('admin.user.index')->with('success', [
            'type' => 'success', 'message' => trans('admin.message.user.create'), 'title' => 'Message'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user->getUserFromID($id);
        return view('admin.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->getUserFromID($id);
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user->getUserFromID($id);
       
        $user->update($request->all());
        $user->password = bcrypt($request->password);
        $user->update();
        return redirect()->route('admin.user.index')->with('success',
            ['type' => 'success', 'message' => trans('admin.message.user.edit'), 'title' => 'Message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->getUserFromID($id)->delete();

        return redirect()->route('admin.user.index')->with('success',
            ['type' => 'success', 'message' => trans('admin.message.user.delete'), 'title' => 'Message']);
    }

    public function getDeletedUser()
    {
        $users = $this->user->getDeletedUsers();
        return view('admin.user.restore', compact('users'));
    }

    public function restore($id)
    {
        $this->user->getDeletedUserFromID($id)->restore();
        return redirect()->route('admin.user.index')->with('success',
            ['type' => 'success', 'message' => trans('admin.message.user.restore'), 'title' => 'Message']);
    }

    public function search(Request $request)
    {
        $users = $this->user->search($request->keyword);
        return view('admin.user.search', compact('users', 'request'));
    }

    public function getProfile()
    {
        $user = Auth::user();
        return view('admin.user.profile', compact('user'));
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = User::getUserFromID(Auth::user()->id);

        if($request->hasFile('image_file')){
            $img = Image::make($_FILES['image_file']['tmp_name']);
            $newName = md5($_FILES['image_file']['tmp_name'].time());
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path('assets/images/'.$newName.'.'.$request->image_file->getClientOriginalExtension()));
            $request->request->add(['avatar' => $newName.'.'.$request->image_file->getClientOriginalExtension()]);
        }

        $user->update($request->all());

        return redirect()->back()->with('success', [
            'type' => 'success', 'message' => trans('admin.message.user.update_profile'), 'title' => 'Message'
        ]);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {   
        $user = User::getUserFromID(Auth::user()->id);
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->back()->with('success', [
            'type' => 'success', 'message' => trans('admin.message.user.update_password'), 'title' => 'Message'
        ]);
    }

    public function getAddUser()
    {
        return view('admin.user.create');
    }

}

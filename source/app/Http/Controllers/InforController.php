<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class InforController extends Controller
{
    //
    public function getinfo(){
        $info = App\InfoCompany::find(1);
        return $info;
    }
}

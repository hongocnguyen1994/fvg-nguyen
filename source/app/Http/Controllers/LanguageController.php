<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class LanguageController extends Controller
{
    public function getSwitchLang($locale)
    {
        Session::put('lang', $locale);
        return redirect()->back();
    }
}

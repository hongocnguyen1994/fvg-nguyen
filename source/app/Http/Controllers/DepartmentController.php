<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class DepartmentController extends Controller
{
    //
    public function listactive() {
        $result = App\Department::where('status','1')->get();
        return $result;
    }
}

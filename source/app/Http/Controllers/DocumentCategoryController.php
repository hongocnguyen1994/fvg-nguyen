<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
class DocumentCategoryController extends Controller
{
    //
    public function list() {
       $result = App\DocumentCategory::all();
        return $result;
    }
    public function listactive() {
        $result = App\DocumentCategory::where('status','1')->get();
        return $result;
    }
}

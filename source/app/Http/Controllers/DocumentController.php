<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class DocumentController extends Controller
{
    //
    public $list = 15;
    public function get() {
        $locale = app()->getLocale();
        $document = DB::table('documents')
            ->join('document_categories', 'documents.category_id', '=', 'document_categories.id')
            ->join('document_category_traslations','document_categories.id','=','document_category_traslations.document_category_id')
            ->select('documents.*','document_category_traslations.name as category_name')->where('document_category_traslations.locale', $locale)
            ->get();
        // $document = App\Document::all();
        return $document;
    }

    public function getNewDocument () {
        $document = DB::table('documents')->orderBy('created_at', 'desc')->limit(5)->get();
        return $document;
    }

    public function getDocumentactive(Request $request) {
      
        $category_id = $request->category_id;
        $last_document = $request->last_document;
        if ($last_document ==0 ){
         $document = App\Document::join('document_categories','documents.category_id','=','document_categories.id')->where('document_categories.id',$category_id)->orderBy('documents.id', 'desc')->select('documents.*')->limit($this->list)->get();
        } else {
            $document = App\Document::join('document_categories','documents.category_id','=','document_categories.id')->where('document_categories.id',$category_id)->where('documents.id','<',$last_document)->orderBy('documents.id', 'desc')->select('documents.*')->limit($this->list)->get();   
        }
        return ($document);
    }
}

<?php

namespace App\Http\Requests;

use App\Role;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:32',
            'last_name' => 'required|max:32',
            'username' => 'required|max:32|unique:users,username',
            'email' => 'required|max:128|unique:users,email',
            'role_id' => [
                'required',
                Rule::notIn(User::SYSTEM_ADMIN_ROLE),
                'exists:roles,id'
            ]

        ];
    }
}

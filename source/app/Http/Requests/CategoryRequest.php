<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'nullable|numeric|min:0',
            'name' => 'required|string|max:255|unique:category_translations,name',
            'description' => 'nullable|string',
            'position' => 'nullable|numeric|min:1',
            'status' => 'required|digits_between: 0, 1'
        ];
    }
}

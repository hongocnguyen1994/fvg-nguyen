<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'code' => 'required|max:191',
            'type' => 'required|max:191',
            'is_hot' => 'required|digits_between: 0, 1',
            'description' => 'nullable',
            'product_category' => 'required|exists:product_categories,id',
            'image' => 'nullable'
        ];
    }
}

<?php

namespace App\Http\Middleware\Admin;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role->id == User::SYSTEM_ADMIN_ROLE){
            return $next($request);
        }

        return redirect()->route('login');
    }
}

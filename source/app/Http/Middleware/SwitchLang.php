<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Session;


class SwitchLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('lang')){
            app()->setLocale(Session::get('lang'));
            Carbon::setLocale(app()->getLocale());
        }
        return $next($request);
    }
}

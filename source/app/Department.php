<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;
use Carbon\Carbon;

class Department extends Model
{
    //
    use SoftDeletes;
    use Translatable;
    public $translatedAttributes = ['name', 'description'];
    public function Department_Translation() {
    	return $this->hasMany(DepartmentTranslation::class,'department_id','id');
    }
}

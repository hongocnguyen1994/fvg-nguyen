<?php

namespace App\Providers;

use App\Category;
use App\Product;
use App\ProductCategory;
use App\Post;
use App\Setting;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Session::put('lang', app()->getLocale());
        Relation::morphMap([
            'posts' => Post::class,
            'categories' => Category::class,
            'products' => Product::class,
            'product_categories' => ProductCategory::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

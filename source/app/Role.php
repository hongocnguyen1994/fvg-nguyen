<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    protected $dates = ['deleted_at'];

    const SYSTEM_ADMIN_ROLE = 1;

    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($role) {
           $role->users()->delete();
        });

        static::restoring(function ($role) {
           $role->users()->withTrashed()->restore();
        });
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public static function getListRoles()
    {
        return self::where('id', '<>', self::SYSTEM_ADMIN_ROLE)->get();
    }

    public static function getRoleFromID($id)
    {
        return self::findOrFail($id);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    const PAGINATE = 10;
    const SYSTEM_ADMIN_ROLE = 1;

    const ACTIVED = 1;
    const UNACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'phone', 'role_id', 'avatar','department_id','regency_id',
    ];

    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function Department(){
        return $this->belongsTo(Department::class);
    }
    public function role()
    {
        return $this->belongsTo(Role::class)->withTrashed();
    }

    public static function getListUsers()
    {
        return self::with('role')->where('role_id', '<>', self::SYSTEM_ADMIN_ROLE)->paginate(self::PAGINATE);
    }

    public static function getDeletedUsers()
    {
        return self::with('role')->onlyTrashed()->paginate(self::PAGINATE);
    }

    public static function getUserFromID($id)
    {
        return $user = self::findOrFail($id);
    }

    public static function getDeletedUserFromID($id)
    {
        return self::withTrashed()->findOrFail($id);
    }

    public static function search($keyword)
    {
        $keyword = !empty($keyword) ? $keyword : '';

        $users = self::where(function ($query) use ($keyword) {
           $query->where('first_name', 'like', '%'.$keyword.'%')
               ->orWhere('last_name', 'like', '%'.$keyword.'%')
               ->orWhere('email', 'like', '%'.$keyword.'%')
               ->orWhere('username', 'like', '%'.$keyword.'%')
               ->orWhere('phone', 'like', '%'.$keyword.'%');
        })->where('role_id', '<>', self::SYSTEM_ADMIN_ROLE);
        
        return $users->paginate(self::PAGINATE);

    }

}

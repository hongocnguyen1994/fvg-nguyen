<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;
use Carbon\Carbon;

class ProductCategory extends Model
{
    use SoftDeletes;
    use Translatable;

    const PAGINATE = 10;
    const SYSTEM_ADMIN_ROLE = 1;
    const ACTIVED = 1;
    const UNACTIVE = 2;
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'product_categories';
    protected $fillable = ['parent_id', 'position', 'status'];
    protected $dates = ['deleted_at'];
    public $translatedAttributes = ['name', 'description'];
    public static $lists = [];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($category) {
         $category->product_categories()->delete();
     });

        static::restoring(function ($category) {
         $category->product_categories()->withTrashed()->restore();
     });
    }
    public function product_categories()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id', 'id');
    }
    public function product_category_translations()
    {
        return $this->hasMany('App\ProductCategoryTranslation', 'product_category_id');
    }
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function images()
    {
        return $this->morphMany(MultiImage::class, 'model');
    }

    public static function getAllCategories()
    {
        return self::withTrashed()->get();
    }

    
    public static function create($data)
    {
        $category = new self();
        $category->parent_id = $data->parent_id;
        $category->position = $data->position;
        $category->status = $data->status;
        $category->save();

        ProductCategoryTranslation::create($data, $category->id);
    }

    public static function edit($data, $id)
    {
        $category = self::getCategoryFromID($id);
        $category->product_category_id = $data->category;
        $category->parent_id = $data->parent_id;
        $category->position = $data->position;
        $category->status = self::ACTIVED;
        $category->save();

        ProductCategoryTranslation::edit($data, $id);
    }
    /**
     * [getListCategories description]
     * @return [type] [description]
     */
    public static function getListCategories()
    {
        return self::orderBy('parent_id')->orderBy('position')->get();
    }
    public static function getArrayCategories()
    {
        return self::where('deleted_at', '=', NULL)->orderBy('parent_id')->orderBy('position')->get()->toArray();
    }
    /**
     * [getCategoryFromID description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function getCategoryFromID($id)
    {
        return self::findOrFail($id);
    }
    /**
     * [getDeletedUsers description]
     * @return [type] [description]
     */
    public static function getDeletedCategories()
    {
        return self::onlyTrashed()->paginate(self::PAGINATE);
    }
    
    /**
     * [getDeletedUserFromID description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function getDeletedCategoryFromID($id)
    {
        return self::withTrashed()->findOrFail($id);
    }

    /**
     * [search description]
     * @param  [type] $keyword [description]
     * @param  [type] $role    [description]
     * @return [type]          [description]
     */
    public static function search($keyword)
    {
        $keyword = !empty($keyword) ? $keyword : '';
        $categories = self::whereIn('id', function ($query) use ($keyword) {
           $query->from('product_category_translations')->select('product_category_id')
                ->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%'.$keyword.'%')
                        ->orWhere('description', 'like', '%'.$keyword.'%');
                })->where('locale', app()->getLocale());
        });
        return $categories->orderBy('id', 'desc')->paginate(self::PAGINATE);
    }
    /**
     * [getCategoriesByParentId description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function getCategoriesByParentId($id){
        return self::where([
            ['deleted_at', '=', NULL],
            ['parent_id', '=', $id],
        ])->orderBy('position')->get();
    }
    
    /**
     * [locationInsertion description]
     * @param  [type] $parent_id [description]
     * @param  [type] $position  [description]
     * @return [type]            [description]
     */
    public static function locationInsertion($parent_id, $position){
        $cate_match = self::where('parent_id', '=', $parent_id)->get();
        foreach ($cate_match as $value) {
            if ($value->position >= $position) {
                DB::table('product_categories')
                ->where('id', $value->id)
                ->update(['position' => $value->position + 1]);
            }
        }
    }
    /**
     * [locationEdit description]
     * @param  [type] $parent_id [description]
     * @param  [type] $position  [description]
     * @param  [type] $id        [description]
     * @return [type]            [description]
     */
    public static function locationEdit($parent_id, $position, $id){
        $old = self::findOrFail($id);
        $old_parent = $old->parent_id;
        $cate_match = self::where('parent_id', '=', $parent_id)->get();
        if($parent_id == $old_parent){
            if ($position < $old->position) {
                foreach ($cate_match as $value) {
                    if ($value->position >= $position && $value->position < $old->position) {
                        DB::table('product_categories')
                        ->where('id', $value->id)
                        ->update(['position' => $value->position + 1]);
                    }
                }
            } else if ($position > $old->position) {
                foreach ($cate_match as $value) {
                    if ($value->position <= $position && $value->position > $old->position) {
                        DB::table('product_categories')
                        ->where('id', $value->id)
                        ->update(['position' => $value->position - 1]);
                    }
                }
            }
        } else {
            $old_cate_match = self::where('parent_id', '=', $old_parent)->get();
            foreach ($old_cate_match as $value) {
                if ($value->position > $old->position) {
                    DB::table('product_categories')
                    ->where('id', $value->id)
                    ->update(['position' => $value->position - 1]);
                }
            }
            foreach ($cate_match as $value) {
                if ($value->position >= $position) {
                    DB::table('product_categories')
                    ->where('id', $value->id)
                    ->update(['position' => $value->position + 1]);
                }
            }
        }
    }

    public static function softCategory($categories, $parent_id, $str)
    {
        foreach ($categories as $key => $category)
        {
            $id = $category->id;
            if ($category->parent_id == $parent_id)
            {
                $category->str = $str;
                self::$lists[] = $category;
                self::softCategory($categories, $id, $str."*");
            }
        }
    }
}

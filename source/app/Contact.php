<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;
    const NOT_REPLY = 1;
    const REPLIED = 2;
    const PAGINATE = 10;

    const ACTIVE = 1;
    const UNACTIVE = 2;

    protected $fillable  = ['name', 'email', 'phone', 'company', 'model_type', 'title', 'content', 'status'];

    public function reply()
    {
        return $this->hasOne(ContactReply::class);
    }

    public static function getListContact()
    {
        return self::paginate(self::PAGINATE);
    }

    public static function getContactFromID($id)
    {
        return self::with('reply')->findOrFail($id);
    }

    public static function create($data)
    {
        $contact = new self();
        $contact->name = $data->name;
        $contact->email = $data->email;
        $contact->phone = $data->phone;
        $contact->company = $data->company;
        $contact->model_type = $data->model_type;
        $contact->title = $data->title;
        $contact->content = $data->content;
        $contact->status = self::ACTIVE;
        $contact->save();
    }
}
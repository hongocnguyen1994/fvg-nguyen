function load(){
       var id = $(this).data('id');
       $("#btn-more").html("Loading....");
       $.ajax({
           url : '{{ url("/hiring-list") }}',
           method : "POST",
           data : {id:id, _token:"{{csrf_token()}}"},
           success : function (data)
           {
              var timeUpdated = 0;
              if(data.data != '') {
                for(i=0; i<data.data.length;i++){ 
                  timeUpdated = new Date(data.data[i].updated_at);
                  var hours = timeUpdated.getHours();
                  if(hours < 10) {
                    hours = '0' + hours;
                  }
                  var minutes = timeUpdated.getMinutes();
                  var day = timeUpdated.getDate();
                  var month = timeUpdated.getMonth()+1;
                  if(month < 10) {
                    month = '0' + month;
                  }
                  var year = timeUpdated.getFullYear();
                  if(data.data[i].image == 'default-featured-image.png') {
                    markup=`
                      <div class="col-md-6 col-xl-4 mb-4 pt-4">
                        <div class="news-list__item">
                          <div class="zoom-outer">
                            <img class="w-100 zoom-inner" src="{{ asset('assets/images/default-featured-image.png') }}" alt="">
                          </div>
                          <div class="p-4">
                            <a class="d-inline-block mw-100 mb-2 text-truncate news-list__title" href="#">${data.data[i].name}</a>
                            <div class="d-flex align-items-center mb-3 news-list__time">
                              <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
                              <span class="mr-2 ml-1">${hours}:${minutes}</span>
                              <span class="ml-1">${day}-${month}-${year}</span>
                            </div>
                            <div class="news-list__summary">${data.data[i].description}</div>
                          </div>
                        </div>
                      </div>
                    `;
                  }
                  else {
                    markup=`
                      <div class="col-md-6 col-xl-4 mb-4 pt-4">
                        <div class="news-list__item">
                          <div class="zoom-outer">
                            <img class="w-100 zoom-inner" src="${data.data[i].image}" alt="">
                          </div>
                          <div class="p-4">
                            <a class="d-inline-block mw-100 mb-2 text-truncate news-list__title" href="#">${data.data[i].name}</a>
                            <div class="d-flex align-items-center mb-3 news-list__time">
                              <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
                              <span class="mr-2 ml-1">${hours}:${minutes}</span>
                              <span class="ml-1">${day}-${month}-${year}</span>
                            </div>
                            <div class="news-list__summary">${data.data[i].description}</div>
                          </div>
                        </div>
                      </div>
                    `;
                  }
                  $("#btn-more").remove();
                  $('#more').append(markup);
                }
                    markup=`
                      <div id="remove-row">
                        <button id="btn-more" data-id="${data.data[data.data.length - 1].updated_at}" class="pt-3 text-center" > Load More </button>
                      </div>
                    `;
                  $('#loadMore').append(markup);
              }
           }
       }); 
};
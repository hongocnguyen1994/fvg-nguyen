function deleteWithConfirm(message) {
    $('.delete').on('click', function () {
        var id = $(this).attr('target-id');
        var check = confirm(message);
        if(check){
            $('.form_delete_'+id).submit();
        }
    });
}
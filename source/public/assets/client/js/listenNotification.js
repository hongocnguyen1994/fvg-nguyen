var dataNotification;
if(typeof(EventSource) !== "undefined") {
    var source = new EventSource("/getNotification");
    source.onmessage = function(event) {
        dataNotification = JSON.parse(event.data);   
        $('#notification .header__noti-st').text(dataNotification.count);
        var newDocument = dataNotification.documents;
        var newMessage = dataNotification.messages;
        var newComment = dataNotification.comments;
        $('#notification .postNotification').empty();
        $('#notification .MessageNotification').empty();
        $('#notification .commentNotification').empty();
        if ( newDocument != ""){
        	data = newDocument;
        	 //$('#notification .postNotification').append('1');
        	  for (i=0;i < data.length ; i ++){
	    		markup = `
	    			<a class="d-flex p-3 noti__item" href="#">
                      <div>
                          <p class="mb-1 noti__content">Co 1 bieu mau moi</p>
                          <p class="mb-0 noti__time">2 giờ</p>
                      </div>
                  </a>
	    		`;
	    		$('#notification .postNotification').append(markup);
	    	}
        }
        if ( newMessage != ""){
        	data = newMessage;
        	 //$('#notification .MessageNotification').append('1');
        	 for (i=0;i < data.length ; i ++){
	    		markup = `
	    			<a class="d-flex p-3 noti__item" href="#">
                      <img class="mr-3 noti__ava" src="/assets/images/${data[i].avatar}" alt="">
                      <div>
                          <p class="mb-1 noti__user">${data[i].first_name} ${data[i].last_name}</p>
                          <p class="mb-1 noti__content">Da dang 1 messages</p>
                          <p class="mb-0 noti__time">2 giờ</p>
                      </div>
                  </a>
	    		`;
	    		$('#notification .commentNotification').append(markup);
	    	}

        }
        for (var index in newComment){
		    if (newComment[index] !=""){
		    	data = newComment[index];
		    	for (i=0;i < data.length ; i ++){
		    		markup = `
		    			<a class="d-flex p-3 noti__item" href="#">
	                      <img class="mr-3 noti__ava" src="/assets/images/${data[i].avatar}" alt="">
	                      <div>
	                          <p class="mb-1 noti__user">${data[i].first_name} ${data[i].last_name}</p>
	                          <p class="mb-1 noti__content">Bình luận bài viết của bạn</p>
	                          <p class="mb-0 noti__time">2 giờ</p>
	                      </div>
	                  </a>
		    		`;
		    		$('#notification .commentNotification').append(markup);
		    	}
		    }
		}
    };
} else {
    console.log("Sorry, your browser does not support server-sent events...");
}

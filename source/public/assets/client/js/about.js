$( document ).ready(function() {
  loadlistpost();
});
var DataPost;
function loadlistpost() {
  $.ajax({
    url : 'client/about',
    type : 'GET',
    async : 'false',
    success: function(response){
      DataPost = response;
      filllistpost();
    }
})
}
function filllistpost() {
  for(i = 0; i < DataPost.length ;i++) {
    data = DataPost[i];
    if (i == 0){
     markup =`
        <li>
            <a class="active" href="#tab-${i}" data-toggle="tab">${data.name}</a>
          </li>
      `;
      markupbody = `
      <div class="tab-pane fade show active" id="tab-${i}">
        ${data.content}
      </div>
      `;
     } else {
      markup =`
        <li>
            <a  href="#tab-${i}" data-toggle="tab">${data.name}</a>
          </li>
      `;
      markupbody = `
      <div class="tab-pane fade " id="tab-${i}">
        ${data.content}
      </div>
      `;
      }
     $('#AboutBar .listpost').append(markup);
     $('#TabContent').append(markupbody);
    }
}
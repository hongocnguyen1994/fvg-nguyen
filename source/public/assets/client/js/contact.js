
var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);  
function sendMail() {
  $('#name_alert').empty();
  $('#email_alert').empty();
  $('#title_alert').empty();
  $('#content_alert').empty();
  name = $('#MailBox .name').val();
  email = $('#MailBox .email').val();
  title = $('#MailBox .title').val();
  content = $('#MailBox .content').val();
  kt = 0;
  if (name == "") {
    kt = 1;
    $('#name_alert').text(name_alert);
  }
  if (email == ""){
    $('#email_alert').text(email_null_alert);
  } else {
    if (!pattern.test(email)) {
      kt = 1;
      $('#email_alert').text(email_alert);
    }
  }
  if (title == "") {
    kt = 1;
    $('#title_alert').text(title_alert);
  }
  if (content == "") {
    kt = 1;
    $('#content_alert').text(content_alert);
  }
  if ( kt == 1) {
    return;
  }
    myData = new FormData();
    myData.append('name',name);
    myData.append('email',email);
    myData.append('title',title);
    myData.append('content',content);
    $.ajax({
    url : '/clientSend',
    type: 'POST',
    async : false,
    cache : false,
    contentType : false,
    processData : false,
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    data : myData,
    success: function(response){
      $('#Alert').empty();
      markup = `
      <div class="alert alert-success alert-dismissable">
        <a href="#" class="close " data-dismiss="alert" aria-label="close">&times;</a>
        <a>${success_mess}</a>.
      </div>
    `;
    $('#Alert').append(markup);
    }
    });
}
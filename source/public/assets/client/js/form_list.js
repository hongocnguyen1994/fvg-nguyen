var DocumentCategory;
var ListDocument;
var list = 15;
var slast_document = 0; 
$( document ).ready(function() {
  loadDocumentCategory();
  checklinkactive(DocumentCategory[0].id);
});

function loadDocumentCategory () {
  $.ajax({
    url: "/Documentcategory/listactive",
    type:'GET',
    async : false,
    success: function(response){
      DocumentCategory = response;
      fillDocumentCategory();
    }
  })
}

function fillDocumentCategory() {
  for(i= 0 ; i < DocumentCategory.length; i++){
    data = DocumentCategory[i];
    if (i==0){
      markup = `
      <li class="nav-item">
            <a class="nav-link active" value="${data.id}" onclick="checklinkactive(this.attributes[1].value)" data-toggle="tab" href="javascrip:void(0)" >${data.name}</a>
          </li>
      `;
    } else {
      markup = `
      <li class="nav-item">
            <a class="nav-link"  value="${data.id}" onclick="checklinkactive(this.attributes[1].value)" data-toggle="tab" href="javascrip:void(0)">${data.name}</a>
          </li>
      `;
    }
    $('#listContent').append(markup);
  }
}
function checklinkactive(value){
  slast_document = 0;
  
  loadDocument(value,slast_document);
}

function loadDocument(category_id,last_document){
  $.ajax({
    url : "/getDocumentactive/"+category_id+"/"+last_document,
    type:'GET',
    async : false,
    success: function(response){
      slast_document = response[response.length -1].id;
      ListDocument = response;
      filldocument();
    }
  })
}

function filldocument(){
  $('#tabContent').empty();
  for(i = 0 ; i < ListDocument.length ; i++){
    data = ListDocument[i];
    markup=`
    <div class="col-lg-6 col-xl-4 s-p-10 mb-4">
      <a class="jsCallModalPdf d-flex align-items-center show-list__item show-list__item--form" href="/storage/files/${data.filename} download">
                      <img class="mr-2 ml-3" src="assets/client/images/pdf.svg" alt="">
                      <div class="show-list__text4">${data.name}</div>
                  </a>
    </div>
    `;
    $('#tabContent').append(markup);
  }
}

function loadmore(){
  category_id = $('#listContent .active').attr("value");
  $.ajax({
    url : "/getDocumentactive/"+category_id+"/"+slast_document,
    type:'GET',
    async : false,
    success: function(response){
      if (response.length == 0) {
        return;
      }

      slast_document = response[response.length -1].id;
      ListDocument = response;
      fillmoredocument();
    }
  })
  
}
function fillmoredocument(){
  for(i = 0 ; i < ListDocument.length ; i++){
    data = ListDocument[i];
    markup=`
    <div class="col-lg-6 col-xl-4 s-p-10 mb-4">
      <a class="jsCallModalPdf d-flex align-items-center show-list__item show-list__item--form" href="/storage/files/${data.filename} download">
                      <img class="mr-2 ml-3" src="assets/client/images/pdf.svg" alt="">
                      <div class="show-list__text4">${data.name}</div>
                  </a>
    </div>
    `;
    $('#tabContent').append(markup);
  }
}

function searchDocument(charater){
   if(charater ==""){
    filldocument($(' .active')[1].attributes[1].value);  
   } else {
     category_id = $(' .active')[1].attributes[1].value;
     $.ajax({
      url : "/internal/searchDocument/"+category_id+"/"+charater,
      type:'GET',
      async : false,
      success: function(response){
        ListDocument = response;
        filldocument();
      }
    })
   }
}

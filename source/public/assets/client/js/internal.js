DataMessage = new FormData;
var LastMessage = '';
var PageMessage = 0;
var SystemTimer = new Date($.now());
var listnewDocument;
var listStaffBar;
$( document ).ready(function() {
  loadnewDocument();
  loadStaffBar();
  loadMessage();
});
$.ajaxSetup({
  cache:false,
  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
});

//Message's Functions and Comment's Functions
function loadMessage() {
  $.ajax({
      url : "/internal/loadMessage/"+LastMessage,
      type : 'GET',
      success: function(response){
          if (response.data.length > 0){
            DataMessage[PageMessage] =  response.data;
            showMessage(PageMessage);
            LastMessage = response.lastId;
            PageMessage++;
          }
      }
  })
}
function loadmoreMessage(){
    loadMessage();
}
function loadnewDocument(){
    $.ajax({
        url : "/loadnewDocument",
        type : 'GET',
        success: function(response){
            listnewDocument = response;
            fillnewDocument();
        }
    })
}
function fillnewDocument(){
    $('#listNewDocument').empty();
    for (i = 0; i< listnewDocument.length; i++){
        data = listnewDocument[i];
        markup = `
        <li class="pt-1 pb-1 internal-bar__item-form">
                  <a class="d-flex align-items-center pt-2 pb-2" href="storage/files/${data.filename}" download>
                                      <img class="mr-2" src="assets/client/images/pdf.svg" alt="">
                                      <div class="">${data.name}</div>
                                  </a>
                </li>
        `;
        $('#listNewDocument').append(markup);
    }
}
function loadStaffBar(){
    $.ajax({
        url : "/loadstaffbar",
        type : 'GET',
        success: function(response){
            listStaffBar = response;
            fillStaffBar();
        }
    })
}
function fillStaffBar(){
    $('#StaffBar').empty();
    for (i = 0; i< listStaffBar.length; i++){
        data = listStaffBar[i];
        markup = `
        <li class="pt-1 pb-1 internal-bar__item-staff">
                  <a class="d-flex align-items-center p-2" href="#">
                                      <img class="mr-2" src="assets/images/${data.avatar}" alt="avatar">
                                      <div class="">${data.first_name} ${data.last_name}</div>
                                  </a>
                </li>
        `;
        $('#StaffBar').append(markup);
    }
}

function showMessage(page){
  for( i = 0; i < DataMessage[page].length ; i++){
      data=DataMessage[page][i];
      timeago =Math.abs(SystemTimer - new Date(data.created_at)) ;
      var seconds = Math.floor(timeago/1000); //ignore any left over units smaller than a second
      var minutes = Math.floor(seconds/60); 
      seconds = seconds % 60;
      var hours = Math.floor(minutes/60);
      minutes = minutes % 60;
      markup = `
        <div class="post" id="Message${data.id}">
            <div class="d-flex justify-content-between post__head">
                <div class="d-flex mr-3 post__user">
                    <img class="mr-3 post__avata" src="/assets/images/${data.avatar}" alt="">
                    <div>
                        <a class="post__uname" href="#">${data.first_name} ${data.last_name}</a>
                        <div class="post__time">${hours} giờ ${minutes} phút trước </div>
                    </div>
                </div>`;
       if (data.user_id == user.id)   {     
       markup += `
                <div class='dropdown control'>
                    <button data-toggle="dropdown"><img src="/assets/client/images/ic_keyboard_arrow_down_black_24px.svg" alt=""></button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" onclick="changeToEditForm(${data.id})" href="javascript:void(0)">Chỉnh sửa</a>
                        <a class="dropdown-item" onclick="deleteMessage(${data.id})" href="javascript:void(0)">Xóa</a>
                    </div>
                </div>
                `;
        }
       markup += `
            </div>
            <div class="post__body">
                  <p class="message_content">${data.content}</p>
            </div>
            <div class="post_item">
            </div>
            <div class="comment">
                <h2 class="comment__head">
                    <span><button value="Message${data.id}" onclick="showComment(this.value)">Bình luận</button></span>
                </h2>           
                <div class="comment_body">
  
                </div>
            </div>
        </div> 
      `;
      $('#Post_box').append(markup); 
      if (data.fileUpload) {
        filetype = data.fileUpload.split('.').pop();
        if (filetype == 'jpg' || filetype == 'png' || filetype == 'jpeg') {
            console.log(filetype == 'jpg' || filetype == 'png' || filetype == 'jpeg');
            $("#Message"+data.id+" .post__body").append("<img src='/storage/files/"+data.fileUpload+"' alt=''>");
        } 
        else {
            filename = data.fileUpload.split('ORIGINALNAME.').pop();
            attachfile = ` 
            <a class="post__attach" href="/storage/files/${data.fileUpload}">
                <img class="mr-2" src="/assets/client/images/pdf.svg" alt="">
                <span class="d-inline-block mt-1">${filename}</span>
            </a>
            `;
            $("#Message"+data.id+" .post_item").append(attachfile);
        }
      }   
  }
}
function changeToEditForm(id){
    Mess_id = "Message"+id;
    content = $('#'+Mess_id+" .message_content").text();
    markup = `
    <textarea rows="5" class="working">${content}</textarea>
    <div class="row">
        <div class="col-md-7"></div>
        <div class="col-md-5">
            <button type="button" class="btn btn-link" onclick="unEditMessage(${id},'${content}')">Hủy</button><button  type="button" class="btn btn-link" onclick="editMessage(${id})">Lưu</button>
        </div>
    </div>
    `;
    $('#'+Mess_id+" .post__body").empty();
    $('#'+Mess_id+" .post__body").append(markup);
}

function unEditMessage(id,content){
    Mess_id = "Message"+id;
    markup = `
    <p class="message_content">${content}</p>
    `;
    $('#'+Mess_id+" .post__body").empty();
    $('#'+Mess_id+" .post__body").append(markup);
}
function editMessage(id){
    Mess_id = "Message"+id;
    content = $('#'+Mess_id+" .working").val();
    if (content == "") return;
    myData = new FormData();
    myData.append('Message_id',id);
    myData.append('content',content);
    $.ajax({
        url :'/internal/editMessage/',
        type : 'POST',
        async : false,
        cache : false,
        contentType : false,
        processData : false,
        data : myData,
        success: function(response){
            $('#'+Mess_id+" .post__body").empty();
            markup = `
              <p class="message_content">${content}</p>
             `;
             $('#'+Mess_id+" .post__body").append(markup);
        }
    });
}
function deleteMessage(message_id){
    myData = new FormData();
    myData.append('message_id',message_id);
    $.ajax({
        url :'/internal/deleteMessage/',
        type : 'POST',
        async : false,
        cache : false,
        contentType : false,
        processData : false,
        data : myData,
        success: function(response){
            $("#Message"+message_id).remove();
            // $('#Post_box').empty(); 
            // loadMessage();
        }
    });
}
function showComment(id){
    
    location = '#'+id+" .comment_body";
    $('#'+id+" .comment_body").empty();
    markup = `
    <div class="d-sm-flex mb-4">
        <img class="mb-2 mb-sm-0 comment__avata" src="/assets/images/${user.avatar}" alt="">
        <textarea class="comment__input" rows="1" location="${id}" placeholder="Viết bình luận" onkeyup="checkSendComment(event,this)"></textarea>
    </div>
    <div class="comment_lv1">
    </div>
    `;

    $('#'+id+" .comment_body").append(markup)
    loadCommentlv1(id);
}

function loadCommentlv1(Message) {
    message_id = Message.replace('Message','');
    $.ajax({
        url : "/internal/loadCommentlv1/"+message_id,
        type : 'GET',
        async : false,
        success: function(response){
            $("#"+Message+" .comment_lv1").empty();
            for(i=0;i < response.length;i++){
                data=response[i];
                timeago =Math.abs(SystemTimer - new Date(data.created_at)) ;
                var seconds = Math.floor(timeago/1000); //ignore any left over units smaller than a second
                var minutes = Math.floor(seconds/60); 
                seconds = seconds % 60;
                var hours = Math.floor(minutes/60);
                minutes = minutes % 60;
                markup = `
                <div class="d-flex justify-content-between comment__item comment__item--lv1">
                    <div class="d-sm-flex">
                        <img class="mb-2 mb-sm-0 comment__avata" src="/assets/images/${data.avatar}" alt="">
                        <div class="mr-2">
                            <div class="mb-1">
                                <a class="comment__uname" href="#">${data.first_name} ${data.last_name}</a>
                                <span class='ml-2 comment__time'>${hours} giờ ${minutes} phút trước</span>
                            </div>
                            <p class="comment__content">${data.content}</p>
                            <a class="comment__reply" href="javascript:void(0)" onclick="loadCommentlv2(${data.id})"><img src="/assets/client/images/comment.svg" alt="">Bình luận</a>
                        </div>
                    </div>
             
                </div>
                <div class="comment_lv2" id="commentlv2_${data.id}">
                </div>
                `;
                $("#"+Message+" .comment_lv1").append(markup);               
            }
        }
    })
}

function loadCommentlv2(comment_id){
    $.ajax({
        url : "/internal/loadCommentlv2/"+comment_id,
        type : 'GET',
        async : false,
        success: function(response){
            $("#commentlv2_"+comment_id).empty();
            for(i=0;i < response.length;i++){
                data=response[i];
                timeago =Math.abs(SystemTimer - new Date(data.created_at)) ;
                var seconds = Math.floor(timeago/1000); //ignore any left over units smaller than a second
                var minutes = Math.floor(seconds/60); 
                seconds = seconds % 60;
                var hours = Math.floor(minutes/60);
                minutes = minutes % 60;
                markup = `
                <div class="d-flex justify-content-between comment__item comment__item--lv2">
                    <div class="d-sm-flex">
                      <img class="mb-2 mb-sm-0 comment__avata comment__avata--small" src="/assets/images/${data.avatar}"  alt="">
                      <div class="mr-2">
                        <div class="mb-1">
                          <a class="comment__uname" href="#">${data.first_name} ${data.last_name}</a>
                          <span class="ml-2 comment__time">${hours} giờ ${minutes} phút trước</span>
                        </div>
                        <p class="comment__content">${data.content}</p>
                      </div>
                    </div>
                </div>
                `;
                $("#commentlv2_"+comment_id).append(markup);      
            }
        }
    })
    footerCommentlv2 = `
    <div class="d-sm-flex mb-3 comment__item--lv2">
        <img class="mb-2 mb-sm-0 comment__avata comment__avata--small" src="/assets/images/${user.avatar}" alt="">
        <textarea class="comment__input" rows="1" location="commentlv2_${comment_id}" onkeyup="checkSendComment(event,this)"  placeholder="Viết bình luận" ></textarea>
    </div>
    `;
    $("#commentlv2_"+comment_id).append(footerCommentlv2);   
}

function addMessage(){
    content = $('#Message .contentMessage').val();
    file = $('#Message .fileUpload')[0].files[0];
    console.log(content !="" || file != null);
    if (content !="" || file != null){
        myData = new FormData();
        myData.append('content',content);
        myData.append('fileUpload',file);
        $.ajax({
            url : "/internal/addMessage",
            type : 'POST',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            data : myData,
            success: function(response){
                LastMessage ='';
                PageMessage = 0;
                $('#Post_box').empty(); 
                $('#Message .contentMessage').val('');
                $('#Message .fileUpload').val('');
                loadMessage();
            }
        })
    }
}

function checkSendComment(evt,element){
    if (evt.keyCode == 13) {
        strLocation = element.getAttribute("location");
        content = element.value;
        Comment(strLocation,content);
        $(element).val("");
    }
}

function Comment(location,content){
    message_id = location.replace('Message','');
    comment_id = location.replace('commentlv2_','');
    if ( comment_id == "Message"+message_id){
        comment_id = null;
    }
    if ( message_id == "commentlv2_"+comment_id){
        message_id = null;
    }
    myData = new FormData();
    myData.append('message_id',message_id);
    myData.append('comment_id',comment_id);
    myData.append('content',content);
    $.ajax({
        url : "/internal/Comment",
        type : 'POST',
        data : myData,
        async : false,
        cache : false,
        contentType : false,
        processData : false,
        success: function(response){
            if ( message_id ) {
                loadCommentlv1(location);
            }
            if ( comment_id ) {
                loadCommentlv2(comment_id);
            }
        }
    })
}
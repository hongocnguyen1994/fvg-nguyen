var Department;
var ListStaff;
var list = 15;
var slast_staff = 0; 
$( document ).ready(function() {
  loadDepartment();
  checklinkactive(Department[0].id);
});

function loadDepartment () {
  $.ajax({
    url: "/Department/listactive",
    type:'GET',
    async : false,
    success: function(response){
        Department = response;
      //fillDocumentCategory();
        fillDepartment();
    }
  })
}

function fillDepartment() {
  for(i= 0 ; i < Department.length; i++){
    data = Department[i];
    if (i==0){
      markup = `
      <li class="nav-item">
            <a class="nav-link active" value="${data.id}" onclick="checklinkactive(this.attributes[1].value)" data-toggle="tab" href="javascrip:void(0)" >${data.name}</a>
          </li>
      `;
    } else {
      markup = `
      <li class="nav-item">
            <a class="nav-link"  value="${data.id}" onclick="checklinkactive(this.attributes[1].value)" data-toggle="tab" href="javascrip:void(0)">${data.name}</a>
          </li>
      `;
    }
    $('#listContent').append(markup);
  }
}
function checklinkactive(value){
    slast_staff = 0;
  
  loadStaff(value,slast_staff);
}

function loadStaff(department_id,slast_staff){
  $.ajax({
    url : "/getStaffActive/"+department_id+"/"+slast_staff,
    type:'GET',
    async : false,
    success: function(response){
      slast_document = response[response.length -1].id;
      ListStaff = response;
      fillStaff();
    }
  })
}

function fillStaff(){
  $('#tabContent').empty();
  for(i = 0 ; i < ListStaff.length ; i++){
    data = ListStaff[i];
    markup=`
    <div class="col-lg-6 col-xl-4 s-p-10 mb-4">
                <div class="d-flex show-list__item">
                  <img class="show-list__avata" src="/assets/images/${data.avatar}" alt="">
                  <div class="show-list__fix-width">
                    <a class="d-inline-block mw-100 text-truncate show-list__text1" href="#">${data.first_name} ${data.last_name}</a>
                    <div class="d-flex mb-1">
                      <span class="show-list__text2">ĐTDĐ:</span>
                      <span class="text-truncate show-list__text3">${data.phone}</span>
                    </div>
                    <div class="d-flex">
                      <span class="show-list__text2">Email:</span>
                      <span class="text-truncate show-list__text3">${data.email}</span>
                    </div>
                  </div>
                </div>
              </div>
    `;
    $('#tabContent').append(markup);
  }
}

function loadmore(){
  department_id = $('#listContent .active').attr("value");
  $.ajax({
    url : "/getStaffActive/"+department_id+"/"+slast_staff,
    type:'GET',
    async : false,
    success: function(response){
      if (response.length == 0) {
        return;
      }

      slast_document = response[response.length -1].id;
      ListStaff = response;
      fillmoreStaff();
    }
  })
  
}
function fillmoreStaff(){
  for(i = 0 ; i < ListStaff.length ; i++){
    data = ListStaff[i];
    markup=`
    <div class="col-lg-6 col-xl-4 s-p-10 mb-4">
                <div class="d-flex show-list__item">
                  <img class="show-list__avata" src="/assets/images/${data.avatar}" alt="">
                  <div class="show-list__fix-width">
                    <a class="d-inline-block mw-100 text-truncate show-list__text1" href="#">${data.first_name} ${data.last_name}</a>
                    <div class="d-flex mb-1">
                      <span class="show-list__text2">ĐTDĐ:</span>
                      <span class="text-truncate show-list__text3">${data.phone}</span>
                    </div>
                    <div class="d-flex">
                      <span class="show-list__text2">Email:</span>
                      <span class="text-truncate show-list__text3">${data.email}</span>
                    </div>
                  </div>
                </div>
              </div>
    `;
    $('#tabContent').append(markup);
  }
}

function searchUser(charater){
  if(charater ==""){
    loadStaff($(' .active')[1].attributes[1].value,0);  
  } else {
    department_id = $(' .active')[1].attributes[1].value;
    $.ajax({
     url : "/internal/searchUser/"+department_id+"/"+charater,
     type:'GET',
     async : false,
     success: function(response){
      ListStaff = response;
      fillStaff();
     }
   })
  }
}

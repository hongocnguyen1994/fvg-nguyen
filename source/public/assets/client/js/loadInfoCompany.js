var sPhone;
var sMail;
var sFax;
var sOffice;
var sAddress;
var sProductCategory;
$( document ).ready(function() {
  loadInfoCompany();
  loadCategory();
});
function loadInfoCompany() {
  $.ajax({
    url : "/infoCompany",
    type : 'GET',
    success: function(response){
      data = response;
      sPhone = data.phone;
      sMail = data.mail;
      sFax = data.fax;
      sOffice = data.office;
      sAddress = data.address;
      $('#InfoCompany .phone').append(sPhone);
      $('#InfoCompany .mail').append(sMail);
      $('#InfoCompany .office').append(sOffice);

      $('#ContactInfo .phone').append(sPhone);
      $('#ContactInfo .mail').append(sMail);
      $('#ContactInfo .address').append(sAddress);
      $('#ContactInfo .fax').append(sFax);
    }
  })
}
function loadCategory() {
  $.ajax({
    url : "/loadProductCategory",
    type : 'GET',
    success: function(response){
      sProductCategory = response;
      fillCategoryMenu();
    }
  })
}
function fillCategoryMenu() {
  for (i=0; i< sProductCategory.length ; i++){
    markup = `
      <a class="dropdown-item" href="/productList/${sProductCategory[i].id}">${sProductCategory[i].name}</a>    
    `;
    $('#menuProduct').append(markup);
  }
}
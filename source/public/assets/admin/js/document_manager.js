var DataCategory;
var Trash;
var lisDocument;
$(document).ready(function(){
  loadCategory();
  fillSelectCategory();
  loadTrash();
  fillTrash();
  loadDocument();
});
//Addition
function showAddition() {
  $('#Addition').toggleClass('show');
  $('#Addition .alert-box').empty();
}
function addDocument(){
  category_id = $('#DocumentCategory').val();
  name = $('#Addition .name').val();
  fileUpload = $('#Addition .fileUpload')[0].files[0];
  if (name !="" || fileUpload != null) {
    myData = new FormData;
    myData.append('name',name);
    myData.append('fileUpload',fileUpload);
    myData.append('category_id',category_id);
    $.ajax({
      url : "/admin/document/add",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadDocument();
        fillDocument();
      }
    })
  } else {
      alert('Please Input Category Name or File');
  }
}
window.onclick = function(event) {
  if (!event.target.matches('.addition')) { 
    if (!event.target.matches('#Addition')) {
      $('#Addition').removeClass('show');
      $('#Addition .alert-box').empty();
    }
  }
}
function loadCategory(){
  $.ajax({
    url: "/Documentcategory/list",
    type:'GET',
    async : false,
    success: function(response){
      DataCategory = response;
    }
  })
}
function loadTrash() {
  $.ajax({
    url: "/admin/document_category/trash",
    type:'GET',
    async : false,
    success: function(response){
      Trash = response.data;
    }
  })
}
function fillTrash() {
  $('#Trash tbody').empty();
  for(i=0; i<Trash.length;i++){
    markup=`
      <tr>
        <td class="name">${Trash[i].name}</td>
        <td class="description">${Trash[i].description}</td>
        <td class="position">${Trash[i].position}</td>
        <td class="restore"><button class="btn btn-success" onclick="restoreCategory(${Trash[i].id})">Restore</button></td>
      </tr>

    `;
    $('#Trash tbody').append(markup);
  }
}
function fillSelectCategory() {
  $('#tableCategory tbody').empty();
  for(i=0; i<DataCategory.length;i++){
    markup=`
    <option value="${DataCategory[i].id}">${DataCategory[i].name}</option>
    `;
    $('#DocumentCategory').append(markup);
  }
} 

function loadDocument(){
  $.ajax({
    url : "/getDocument",
    type : 'get',
    async : false,
    success: function(response){
      lisDocument = response;
      fillDocument();
    }
})
}

function fillDocument(){
  $('#tableDocument tbody').empty();
  for(i = 0; i < lisDocument.length ; i++){
    data=lisDocument[i];
    filename = data.filename.split('ORIGINALNAME.').pop();
    markup =`
    <tr>
        <td class="name">${data.name}</td>
        <td class="category">${data.category_name}</td>
        <td class="filepath"><a href="/storage/files/${data.filename}" download>${filename}</a></td>
        <td><button class="btn btn-warning" onclick="">Edit</button></td>
        <td><button class="btn btn-danger" onclick="">Delete</button></td>  
      </tr>
    `;
    $('#tableDocument tbody').append(markup);
  }
}
function showtrash() {
  $('#Trash').toggleClass('show');
}

function restoreCategory(id) {
  myData = new FormData;
  myData.append('id',id);
  $.ajax({
      url : "/admin/document_category/restore",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadCategory();
        fillSelectCategory();
        loadTrash();
        fillTrash();
      }
  })
}
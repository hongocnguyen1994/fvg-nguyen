var DataCategory;
var Trash;
$(document).ready(function(){
  loadCategory();
  fillSelectCategory();
  loadTrash();
  fillTrash();
});
//Addition
function showAddition() {
  $('#Addition').toggleClass('show');
  $('#Addition .alert-box').empty();
}
function addCategory(){
  name = $('#Addition .name').val();
  if (name !="") {
    myData = new FormData;
    myData.append('name',name);
    $.ajax({
      url : "/admin/document_category/add",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadCategory();
        fillSelectCategory();
      }
    })
  } else {
      alert('Please Input Category Name');
  }
}
window.onclick = function(event) {
  if (!event.target.matches('.addition')) { 
    if (!event.target.matches('#Addition')) {
      $('#Addition').removeClass('show');
      $('#Addition .alert-box').empty();
    }
  }
}
function loadCategory(){
  $.ajax({
    url: "/Documentcategory/list",
    type:'GET',
    async : false,
    success: function(response){
      DataCategory = response;
    }
  })
}
function loadTrash() {
  $.ajax({
    url: "/admin/document_category/trash",
    type:'GET',
    async : false,
    success: function(response){
      Trash = response.data;
    }
  })
}
function fillTrash() {
  $('#Trash tbody').empty();
  for(i=0; i<Trash.length;i++){
    markup=`
      <tr>
        <td class="name">${Trash[i].name}</td>
        <td class="description">${Trash[i].description}</td>
        <td class="position">${Trash[i].position}</td>
        <td class="restore"><button class="btn btn-success" onclick="restoreCategory(${Trash[i].id})">Restore</button></td>
      </tr>

    `;
    $('#Trash tbody').append(markup);
  }
}
function fillSelectCategory() {
  $('#tableCategory tbody').empty();
  for(i=0; i<DataCategory.length;i++){
    markup=`
    <tr>
      <td class="name">${DataCategory[i].name}</td>
      <td class="description">${DataCategory[i].description}</td>
      <td class="position"><input type="text" value="${DataCategory[i].position}" onblur="updatePosition(${i},this.value)"/></td>
    `;
    if (DataCategory[i].status ==1){
      markup +=`<td class="status" value="1"><button class="btn btn-success" onclick="changeStatus(${DataCategory[i].id})">Actived</button></td>`;
    } else {
      markup +=`<td class="status" value="0"><button class="btn btn-danger" onclick="changeStatus(${DataCategory[i].id})">Nonactived</button></td>`;
    }
    markup +=`
      <td><button class="btn btn-warning" onclick="editCategory(${i})">Edit</button></td>
      <td><button class="btn btn-danger" onclick="deleteCategory(${DataCategory[i].id})">Delete</button></td>
    </tr>
    `;
    $('#tableCategory tbody').append(markup);
  }
} 

function editCategory(element) {
  console.log($(element));
}
function changeStatus(Category) {
  myData = new FormData;
  myData.append('id',Category);
  $.ajax({
      url : "/admin/document_category/status",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadCategory();
        fillSelectCategory();
      }
    })
}

function deleteCategory(Category) {
  myData = new FormData;
  myData.append('id',Category);
  $.ajax({
      url : "/admin/document_category/delete",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadCategory();
        fillSelectCategory();
        loadTrash();
        fillTrash();
      }
  })
}

function editCategory(id){
  $('#Edidtion').toggleClass('show');
  $('#Edidtion .name').val(DataCategory[id].name);
  $('#Edidtion .description').val(DataCategory[id].description);
  $('#Edidtion .position').val(DataCategory[id].position);
  $('#Edidtion .savebtn').val(DataCategory[id].id);
}

function UpdateCategoryDocument(id){
  name =  $('#Edidtion .name').val();
  description =  $('#Edidtion .description').val();
  position =  $('#Edidtion .position').val();
  myData = new FormData;
  myData.append('id',id);
  myData.append('name',name);
  myData.append('description',description);
  myData.append('position',position);
  $.ajax({
      url : "/admin/document_category/update",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadCategory();
        fillSelectCategory();
        $('#Edidtion').toggleClass('show');
      }
  })
}

function showtrash() {
  $('#Trash').toggleClass('show');
}

function restoreCategory(id) {
  myData = new FormData;
  myData.append('id',id);
  $.ajax({
      url : "/admin/document_category/restore",
      type : 'POST',
      async : false,
      cache : false,
      contentType : false,
      processData : false,
      data : myData,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      success: function(response){
        loadCategory();
        fillSelectCategory();
        loadTrash();
        fillTrash();
      }
  })
}
function updatePosition(index,value){
  myData = new FormData;
  myData.append('id',DataCategory[index].id);
  myData.append('name',DataCategory[index].name);
  myData.append('description',DataCategory[index].description);
  myData.append('position',value);
  $.ajax({
    url : "/admin/document_category/update",
    type : 'POST',
    async : false,
    cache : false,
    contentType : false,
    processData : false,
    data : myData,
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    success: function(response){
      loadCategory();
      fillSelectCategory();
 
    }
})
}
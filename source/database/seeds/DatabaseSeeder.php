<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//creat role customer
        DB::table('roles')->insert([
        	'name' => 'System Admin',
        	'status' => 1
        ]);
        DB::table('roles')->insert([
        	'name' => 'customer',
        	'status' => 1
        ]);

        $user = new \App\User();
        $user->username = 'admin';
        $user->first_name ="Admin";
        $user->last_name ="Admin";
        $user->password = bcrypt('N123!@#');
        $user->email = 'admin@admin.com';
        $user->role_id = \App\User::SYSTEM_ADMIN_ROLE;
        $user->save();
        $this->call(UsersTableSeeder::class);    
        $this->call(PostCategoryTableSeeder::class);
        $this->call(InforCompanyTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class PostCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //
        $this->newPostCategory('Tuyển dụng','Recruitment');
        $this->newPostCategory('Tin tức','News');
        $this->newPostCategory('Giới Thiệu','About Us');
        $this->newPostCategory('Bảng Hiệu Sản Phẩm','Pannel Product');
        $this->newPostCategory('Bảng Hiệu Giới Thiệu','Pannel Introduction');
    }

    public function newPostCategory($viname,$enname){
        $PostCategory = new  App\Category;
        $PostCategory->status = 1;
        $PostCategory->save();
      	$vilocal = new App\CategoryTranslation;
      	$vilocal->category_id = $PostCategory->id;
      	$vilocal->name = $viname;
      	$vilocal->locale = "vi";
      	$vilocal->save();
      	$enlocal = new App\CategoryTranslation;
      	$enlocal->category_id = $PostCategory->id;
      	$enlocal->name = $enname;
      	$enlocal->locale = "en";
      	$enlocal->save();
        
    }
}

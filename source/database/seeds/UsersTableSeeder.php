<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $limit = 10;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'username' => $faker->userName(),
                'email' => $faker->unique()->email,
                'password'     => bcrypt('N123!@#'),
                'phone' => $faker->phoneNumber,
                'role_id' => 2,
            ]);
        }

    }

    public function addUser($username,$email){
        $user = new  App\User;
        $user->last_name = $username;
        $user->username = $username;
        $user->email = $email;
        $user->password = bcrypt('N123!@#');
        $user->role_id = 2;
        $user->save();
        
    }
}

<?php

use Illuminate\Database\Seeder;

class InforCompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->createSeedinfo();
    }
    public function createSeedinfo() {
        $infor = new App\InfoCompany;
        $infor->phone = "0236 3923 911";
        $infor->fax = "0236 3923 911";
        $infor->mail = "info@tva-group.com.vn";
        $infor->save();
        $vilocal = new App\InfoCompanyTranslation;
        $vilocal->info_id = $infor->id;
        $vilocal->address = "Hà My Tây – Điện Dương – Điện Bàn – Quảng Nam";
        $vilocal->office = "TẦNG 15 - 02 QUANG TRUNG, HẢI CHÂU, ĐÀ NẴNG";
        $vilocal->locale = "vi";
        $vilocal->save();
        $enlocal = new App\InfoCompanyTranslation;
        $enlocal->info_id = $infor->id;
        $enlocal->address = "Ha My Tay - Dien Duong - Dien Ban - Quang Nam";
        $enlocal->office = "FLOOR 15 - 02 QUANG TRUNG, HAI CHAU, DA NANG";
        $enlocal->locale = "en";
        $enlocal->save();
    }
}

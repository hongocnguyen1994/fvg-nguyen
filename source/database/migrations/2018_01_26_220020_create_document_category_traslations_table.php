<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentCategoryTraslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_category_traslations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_category_id')->unsigned();
            $table->string('name')->unique();
            $table->text('description')->nullable();
            $table->string('locale');
            //Forein Key
            $table->unique(['document_category_id', 'locale']);
            $table->foreign('document_category_id')->references('id')->on('document_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_category_traslations');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegencyTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regency_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regency_id')->unsigned();
            $table->string('name')->unique();
            $table->text('description')->nullable();
            $table->string('locale');
            $table->unique(['regency_id', 'locale']);
            $table->foreign('regency_id')->references('id')->on('regencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regency_translations');
    }
}

<?php

return [

  'button'=> [
    'logout' => 'Đăng Xuất',
    'login' => 'Đăng Nhập',
    'detail' => 'Xem Chi Tiết',
  ],
  'menu' => [
    'about' => 'Giới Thiệu',
    'product' => 'Sản Phẩm',
    'internal' => 'Nội Bộ',
    'news' => 'Tin Tức',
    'recruitment' => 'Tuyển Dụng',
    'contact' => 'Liên Hệ',
    'productlist' => 'Danh Sách Sản Phẩm',
  ],
  'infomation' => [
    'infomation' => 'Thông Tin',
    'address' => 'Địa Chỉ',
    'phonenumber' => 'Số Điện Thoại',
    'mail' => 'Thư điện tử ',
    'detailinfo' => 'Tầng 15 - 02 Quang Trung, Hải Châu, Đà Nẵng',
  ],
  'follow' => [
    'follow' => 'Theo Dõi',
    'designed' => 'Thiết kế bởi',
  ],
  'name' => [
    'company' => 'công ty',
    'companyTV' => 'TNHH SWINE LINE',
  ],
  'page' => [
    'partner' => 'Đối Tác',
  ],
  'contact' => [
    'withus' => 'Liên hệ với chúng tôi',
    'fullname' => 'Họ và tên',
    'inputname' => 'Nhập tên',
    'email' => 'Email',
    'inputemail' => 'Nhập Email',
    'title' => 'Tiêu đề',
    'inputtitle' => 'Nhập tiêu đề ',
    'content' => 'Nội dung',
    'inputcontent' => 'Nhập nội dung',
    'send' => 'Gửi',
    'address' => 'Địa chỉ',
    'phone' => 'Điện thoại',
    'fax' => 'Fax',
    'namecompany' => 'Công ty TNHH Thái Việt Agri Group',
    'name_alert' => 'Vui lòng nhập tên của bạn',
    'email_alert' => 'Địa chỉ Email không hợp lệ',
    'title_alert' => 'Vui lòng nhập tiêu đề',
    'content_alert' => 'Vui lòng nhập nội dung',
    'success_alert' => 'Thành Công',
    'success_mess' => 'Mail của bạn đã được gửi thành công',
    'email_null_alert' => 'Vui lòng nhập email',
  ],
  'login' => [
    'login_policy' => 'Chúng tôi sẽ không bao giờ chia sẻ email của bạn cho bất kì ai.',
    'login' => 'Đăng nhập',
    'error' => [
      'email' => 'Thông tin tài khoản không chính xác.',
      'password' => 'Yêu cầu nhập mật khẩu.',
  ],
  ],
];
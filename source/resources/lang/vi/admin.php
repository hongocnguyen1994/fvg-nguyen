<?php

return [
    'sidebar' => [
        'dashboard' => [
            'index' => 'Dashboard',
        ],

        'department' => [
            'index' => 'Bộ phận',
        ],

        'user' => [
            'manager' => 'Quản lý người dùng',
            'list' => 'Danh sách người dùng',
            'add' => 'Thêm người dùng',
            'show' => 'Thông tin',
            'edit' => 'Chỉnh sửa',
            'search' => 'Tìm kiếm người dùng',
        ],

        'role' => [
            'manager' => 'Quản lý phân quyền',
            'list' => 'Danh sách phân quyền',
            'add' => 'Thêm phân quyền',
            'show' => 'Chi tiết',
            'edit' => 'Chỉnh sửa',
        ],

        'category' => [
            'manager' => 'Danh mục',
            'list' => 'Danh sách danh mục',
            'add' => 'Thêm mới danh mục',
            'deleted' => 'Các danh mục đã xóa',
        ],

        'post' => [
            'manager' => 'Quản lý bài viết',
            'list' => 'Danh sách bài viết',
            'add' => 'Thêm bài viết',
            'deleted' => 'Các bài viết đã xóa',
            'edit' => 'Chỉnh sửa',
        ],

        'category_product' => [
            'manager' => 'Danh mục sản phẩm',
            'list' => 'Danh sách danh mục',
            'add' => 'Thêm mới danh mục',
            'deleted' => 'Các danh mục đã xóa',
        ],

        'product' => [
            'manager' => 'Quản lý sản phẩm',
            'list' => 'Danh sách sản phẩm',
            'add' => 'Thêm mới sản phẩm',
            'deleted' => 'Các sản phẩm đã xóa',
            'edit' => 'Chỉnh sửa',
        ],

        'contact' => [
            'manager' => 'Liên hệ',
            'list' => 'Danh sách liên hệ',
        ],

        'document' => [
            'manager' => 'Tài liệu',
            'category' => 'Danh mục tài liệu',
            'list' => 'Danh sách tài liệu',
        ],

        'partner' => [
            'manager' => 'Đối tác',
            'list' => 'Danh sách đối tác',
            'add' => 'Thêm đối tác',
        ],
    ],

    'title' => [
        'dashboard' => [
            'index' => 'Dashboard',
        ],

        'department' => [
            'index' => 'Bộ phận',
        ],

        'user' => [
            'index' => 'Danh sách người dùng',
            'add' => 'Thêm mới người dùng',
            'show' => 'Thông tin người dùng',
            'edit' => 'Chỉnh sửa',
            'search' => 'Tìm kiếm người dùng',
        ],

        'role' => [
            'index' => 'Danh sách phân quyền',
            'add' => 'Thêm mới phân quyền',
            'show' => 'Chi tiết',
            'edit' => 'Chỉnh sửa',
        ],

        'category' => [
            'index' => 'Danh sách danh mục',
            'add' => 'Thêm mới danh mục',
            'deleted' => 'Các danh mục đã xóa',
            'show' => 'Chi tiết danh mục',
            'edit' => 'Chỉnh sửa danh mục',
        ],

        'post' => [
            'index' => 'Danh sách bài viết',
            'add' => 'Thêm mới bài viết',
            'restore' => 'Hoàn tác bài viết',
        ],

        'category_product' => [
            'index' => 'Danh sách danh mục sản phẩm',
            'add' => 'Thêm mới danh mục',
            'restore' => 'Các danh mục sản phẩm đã xóa',
            'show' => 'Chi tiết danh mục sản phẩm',
        ],

        'product' => [
            'index' => 'Danh sách sản phẩm',
            'add' => 'Thêm mới sản phẩm',
            'restore' => 'Các sản phẩm đã xóa',
            'edit' => 'Chỉnh sửa sản phẩm',
        ],

        'contact' => [
            'index' => 'Danh sách liên hệ',
        ],

        'document' => [
            'category' => 'Danh mục tài liệu',
            'index' => 'Danh sách tài liệu',
        ],

        'partner' => [
            'index' => 'Danh sách đối tác',
            'add' => 'Thêm mới đối tác',
            'edit' => 'Chỉnh sửa đối tác',
        ],

        'booking' => [
            'index' => 'Tải lên hình ảnh',
        ],
    ],

    'table' => [
        'department_list' => 'Danh sách bộ phận',
        'name' => 'Tên',
        'description' => 'Mô tả',
        'position' => 'Vị trí',
        'status' => 'Trạng thái',
        'edit' => 'Chỉnh sửa',
        'delete' => 'Xóa',

        'id' => 'ID',
        'full_name' => 'Tên đầy đủ',
        'username' => 'Tên người dùng',
        'email' => 'Email',
        'created_at' => 'Ngày tạo',
        'role' => 'Quyền hạng',
        'department' => 'Bộ phận',
        'function' => 'Chức năng',

        'quantity' => 'Số lượng',

        'updated_at' => 'Ngày cập nhật',
        'current_language' => 'Đa ngôn ngữ',

        'parent' => 'Thư mục cha',
        'deleted_at' => 'Ngày xóa',
        'restore' => 'Hoàn tác',

        'category' => 'Danh mục',
        'published_at' => 'Ngày đăng',
        'publish' => 'Đăng bài',

        'code' => 'Mã',
        'product_category' => 'Danh mục sản phẩm',
        'is_hot' => 'Nổi bật',
        'type' => 'Loại',

        'title' => 'Tiêu đề',
        'phone' => 'Số điện thoại',
        'reply' => 'Trả lời',

        'filePatch' => 'Đường dẫn',

        'image' => 'Hình ảnh',
        'link' => 'Đường dẫn',
    ],

    'button' => [
        'add' => 'Thêm',
        'function' => 'Chức năng',
        'edit' => 'Chỉnh sửa',
        'delete' => 'Xóa',
        'create' => 'Tạo',
        'image' => 'Hình ảnh',
        'cancel' => 'Dừng lại',
        'publish' => 'Xuất bản',
        'un_publish' => 'Thu hồi',
        'search' => 'Tìm kiếm',
        'post_image' => 'Chọn hình ảnh bài viết',
        'product_image' => 'Chọn hình ảnh sản phẩm',
        'update' => 'Tải lên',
        'actived' => 'Đã kích hoạt',
        'nonactived' => 'Chưa kích hoạt',
        'save' => 'Lưu',
        'profile' => 'Thông tin cá nhân',
        'logout' => 'Đăng xuất',
        'delete_confirm' => 'Bạn có chắc muốn xóa người dùng này chứ?',
    ],

    'place' => [
        'department' => [
            'name' => 'Tên Bộ phận',
            'description' => 'Mô tả bộ phận',
        ],

        'user' => [
            'search' => 'Tìm kiếm người dùng',
            'first_name' => 'Họ',
            'last_name' => 'Tên',
            'username' => 'Tên người dùng',
            'email' => 'Địa chỉ email',
            'password' => 'Mật khẩu',
        ],

        'role' => [
            'name' => 'Tên phân quyền',
            'description' => 'Mô tả phân quyền',
        ],

        'category' => [
            'search' => 'Tìm kiếm danh mục',
            'name' => 'Tên danh mục',
            'description' => 'Mô tả danh múc',
        ],

        'post' => [
            'search' => 'Tìm kiếm bài viết',
            'name' => 'Tên bài viết',
            'description' => 'Mô tả bài viết',
        ],

        'category_product' => [
            'search' => 'Tìm kiếm danh mục sản phẩm',
            'name' => 'Tên danh mục sản phẩm',
            'description' => 'Mô tả danh mục sản phẩm',
        ],

        'product' => [
            'search' => 'Tìm kiếm sản phẩm',
            'name' => 'Tên sản phẩm',
            'code' => 'Mã sản phẩm',
            'type' => 'Loại sản phẩm',
            'description' => 'Mô tả sản phẩm',
        ],

        'document' => [
            'name' => 'Tên tài liệu',
        ],

        'document_category' => [
            'name' => 'Tên danh mục',
        ],

        'partner' => [
            'name' => 'Tên đối tác',
            'link' => 'Đường dẫn',
        ],
    ],

    'label' => [
        'user' => [
            'first_name' => 'Họ',
            'last_name' => 'Tên',
            'username' => 'Tên tài khoản',
            'email' => 'Email',
            'department' => 'Bộ phận',
            'role' => 'Phân quyền',
            'fullname' => 'Họ tên đầy đủ',
            'phone' => 'Số điện thoại',
            'address' => 'Địa chỉ',
            'password' => 'Mật khẩu',
        ],

        'role' => [
            'name' => 'Tên phân quyền',
            'description' => 'Mô tả',
        ],

        'category' => [
            'name' => 'Tên danh mục',
            'description' => 'Mô tả',
            'parent' => 'Thư mục cha',
            'position' => 'Vị trí',
            'status' => 'Trạng thái',
            'child' => 'Thư mục con',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
        ],

        'post' => [
            'name' => 'Tên bài viết',
            'description' => 'Mô tả',
            'category' => 'Danh mục',
            'content' => 'Nội dung',
            'image' => 'Hình ảnh',
        ],

        'category_product' => [
            'name' => 'Tên danh mục sản phẩm',
            'description' => 'Mô tả',
            'parent' => 'Thư mục cha',
            'position' => 'Vị trí',
            'status' => 'Trạng thái',
            'child' => 'Thư mục con',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
        ],

        'product' => [
            'name' => 'Tên sản phẩm',
            'code' => 'Mã sản phẩm',
            'type' => 'Loại sản phẩm',
            'description' => 'Mô tả',
            'is_hot' => 'Nổi bật',
            'product_category' => 'Danh mục sản phẩm',
            'detail' => 'Chi tiết',
            'image' => 'Hình ảnh',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
        ],

        'document' => [
            'category' => 'Danh mục',
        ],

        'partner' => [
            'name' => 'Tên đối tác',
            'link' => 'Đường dẫn',
            'image' => 'Hình ảnh',
        ],
    ],

    'status' => [
        'active' => 'Hoạt động',
        'deactive' => 'Không hoạt động',
    ],

    //Available
    'lang' => [
        'available' => 'Đang sẵn sàng',
    ],

    'is_hot' => [
        'hot' => 'Nổi bật',
        'nothot' => 'Không nổi bật',
        'active' => 'Hoạt động',
    ],
    
    'message' => [
        'user' => [
            'delete' => 'Xóa thành công',
        ],

        'post' => [
            'publish' => 'Đăng thành công',
            'un_publish' => 'Thu hồi thành công',
        ],
    ],

];
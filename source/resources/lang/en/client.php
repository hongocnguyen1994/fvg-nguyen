<?php

return [
    'button'=> [
      'logout' => 'Logout',
      'login'  =>  'Login',
      'detail' => 'View Detail',
    ],
    'menu' => [
      'about' => 'About Us',
      'product' => 'Products',
      'internal' => 'Internal',
      'news' => 'News',
      'recruitment' => 'Recruitment',
      'contact' => 'Contact',
      'productlist' => 'List Products',
      'menu' => 'Menu',
    ],
    'infomation' => [
      'infomation' => 'Infomation',
      'address' => 'Address',
      'phonenumber' => 'Phone number',
      'mail' => 'Email',
      'detailinfo' => 'Floor 15 - 02 Quang Trung, Hai Chau, Da Nang',
    ],
    'follow' => [
      'follow' => 'Follow',
      'designed' => 'Designed by',
    ],
    'name' => [
       'company' => 'company',
       'companyTV' => 'LLC SWINE LINE',
    ],
    'page' => [
      'partner' => 'Partner',
    ],
    'contact' => [
      'withus' => 'Contact With Us',
      'fullname' => 'Your Full Name',
      'inputname' => 'Input your full name',
      'email' => 'Email',
      'inputemail' => 'Input your email',
      'title' => 'Title',
      'inputtitle' => 'Input tilte',
      'content' => 'Content',
      'inputcontent' => 'Input content',
      'send' => 'Send',
      'address' => 'Address',
      'phone' => 'Tel',
      'fax' => 'Fax',
      'namecompany' => 'Company LLC Thai Viet Agri Group',
      'name_alert' => 'Please input your name',
      'email_alert' => 'Your mail is invalid',
      'title_alert' => 'Please input title',
      'content_alert' => 'Please input content',
      'success_alert' => 'Success',
      'success_mess' => 'The mail has been sent successfully',
      'email_null_alert' => 'Please input your email',
    ],
    'login' => [
      'login_policy' => "We'll never share your email with anyone else",
      'login' => 'Login',
      'error' => [
          'email' => 'These credentials do not match our records.',
          'password' => 'The password field is required.',
      ],
    ],
];
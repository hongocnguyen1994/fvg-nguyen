<?php

return [
    'sidebar' => [
        'dashboard' => [
            'index' => 'Dashboard',
        ],

        'department' => [
            'index' => 'Department',
        ],

        'user' => [
            'manager' => 'User Manager',
            'list' => 'List User',
            'add' => 'Add User',
            'show' => 'Show User',
            'edit' => 'Edit User',
            'search' => 'Search User',
        ],

        'role' => [
            'manager' => 'Role Manager',
            'list' => 'List Role',
            'add' => 'Add Role',
            'show' => 'Show role',
            'edit' => 'Edit role',
        ],

        'category' => [
            'manager' => 'Category Manager',
            'list' => 'List Category',
            'add' => 'Add Category',
            'deleted' => 'Deleted Category',
        ],

        'post' => [
            'manager' => 'Post Manager',
            'list' => 'List Post',
            'add' => 'Add Post',
            'deleted' => 'Deleted Post',
            'edit' => 'Edit Post',
        ],

        'category_product' => [
            'manager' => 'Category Product',
            'list' => 'List Category Product',
            'add' => 'Add Category Product',
            'deleted' => 'Deleted Category Product',
        ],

        'product' => [
            'manager' => 'Product',
            'list' => 'List Product',
            'add' => 'Add Product',
            'deleted' => 'Deleted Product',
            'edit' => 'Edit Product',
        ],

        'contact' => [
            'manager' => 'Contact',
            'list' => 'List Contact',
        ],

        'document' => [
            'manager' => 'Document',
            'category' => 'Document Category',
            'list' => 'List Document',
        ],

        'partner' => [
            'manager' => 'Partner',
            'list' => 'List Partner',
            'add' => 'Add Partner',
        ],
    ],

    'title' => [
        'dashboard' => [
            'index' => 'Dashboard',
        ],

        'department' => [
            'index' => 'Department',
        ],

        'user' => [
            'index' => 'List User',
            'add' => 'Add User',
            'show' => 'Show User',
            'edit' => 'Edit User',
            'search' => 'Search User',
        ],

        'role' => [
            'index' => 'List Role',
            'add' => 'Add Role',
            'show' => 'Show role',
            'edit' => 'Edit role',
        ],

        'category' => [
            'index' => 'List Category',
            'add' => 'Add Category',
            'deleted' => 'List Deleted Categories',
            'show' => 'Show Categories',
            'edit' => 'Edit Categories',
        ],

        'post' => [
            'index' => 'List Post',
            'add' => 'Add Post',
            'restore' => 'List Deleted Post',
        ],

        'category_product' => [
            'index' => 'List Category Product',
            'add' => 'Add Category Product',
            'restore' => 'List Deleted Category Product',
            'show' => 'Show category product',
        ],

        'product' => [
            'index' => 'List Product',
            'add' => 'Add Product',
            'restore' => 'List Deleted Product',
            'edit' => 'Edit Product',
        ],

        'contact' => [
            'index' => 'List Contact',
        ],

        'document' => [
            'category' => 'Document Category',
            'index' => 'List Document',
        ],

        'partner' => [
            'index' => 'List Partner',
            'add' => 'Add Partner',
            'edit' => 'Edit Partner',
        ],

        'booking' => [
            'index' => 'Upload Image',
        ],
    ],

    'table' => [
        'department_list' => 'Department List',
        'name' => 'Name',
        'description' => 'Description',
        'position' => 'Position',
        'status' => 'Status',
        'edit' => 'Edit',
        'delete' => 'Delete',

        'id' => 'ID',
        'full_name' => 'Full Name',
        'username' => 'User Name',
        'email' => 'Email',
        'created_at' => 'Created At',
        'role' => 'Role',
        'department' => 'Department',
        'function' => 'Function',

        'quantity' => 'Quantity',

        'updated_at' => 'Updated At',
        'current_language' => 'Current Language',

        'parent' => 'Parent',
        'deleted_at' => 'Deleted At',
        'restore' => 'Restore',

        'category' => 'Category',
        'published_at' => 'Published At',
        'publish' => 'Publish',

        'code' => 'Code',
        'product_category' => 'Product Category',
        'is_hot' => 'Is Hot',
        'type' => 'Type',

        'title' => 'Title',
        'phone' => 'Phone Number',
        'reply' => 'Reply',

        'filePatch' => 'File Patch',

        'image' => 'Image',
        'link' => 'Link',
    ],

    'button' => [
        'add' => 'Add',
        'function' => 'Function',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'create' => 'Create',
        'image' => 'Image',
        'cancel' => 'Cancel',
        'publish' => 'Publish',
        'un_publish' => 'Unpublish',
        'search' => 'Search',
        'post_image' => 'Browser Image',
        'product_image' => 'Browser Image',
        'update' => 'Update',
        'actived' => 'Actived',
        'nonactived' => 'Non Actived',
        'save' => 'Save',
        'profile' => 'Profile',
        'logout' => 'Logout',
        'delete_confirm' => 'Are you sure you want to delete this user?',
    ],

    'place' => [
        'department' => [
            'name' => 'Name Department',
            'description' => 'Department Description',
        ],

        'user' => [
            'search' => 'Search User',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'User Name',
            'email' => 'Email',
            'password' => 'Password',
        ],

        'role' => [
            'name' => 'Name Role',
            'description' => 'Description',
        ],

        'category' => [
            'search' => 'Search Category',
            'name' => 'Name Category',
            'description' => 'Description',
        ],

        'post' => [
            'search' => 'Search Post',
            'name' => 'Name Post',
            'description' => 'Description',
        ],

        'category_product' => [
            'search' => 'Search Category Product',
            'name' => 'Name Category Product',
            'description' => 'Description',
        ],

        'product' => [
            'search' => 'Search Product',
            'name' => 'Product Name',
            'code' => 'Product Code',
            'type' => 'Product Type',
            'description' => 'Description',
        ],

        'document' => [
            'name' => 'Name document',
        ],

        'document_category' => [
            'name' => 'Name document category',
        ],

        'partner' => [
            'name' => 'Partner Name',
            'link' => 'Partner Link',
        ],
    ],

    'label' => [
        'user' => [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'User Name',
            'email' => 'Email',
            'department' => 'Department',
            'role' => 'Role',
            'fullname' => 'Full Name',
            'phone' => 'Phone Number',
            'address' => 'Address',
            'password' => 'Password',
        ],

        'role' => [
            'name' => 'Name',
            'description' => 'Description',
        ],

        'category' => [
            'name' => 'Name',
            'description' => 'Description',
            'parent' => 'Parent',
            'position' => 'Position',
            'status' => 'Status',
            'child' => 'Child',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],

        'post' => [
            'name' => 'Name',
            'description' => 'Description',
            'category' => 'Category',
            'content' => 'Content',
            'image' => 'Image',
        ],

        'category_product' => [
            'name' => 'Name',
            'description' => 'Description',
            'parent' => 'Parent',
            'position' => 'Position',
            'status' => 'Status',
            'child' => 'Child',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],

        'product' => [
            'name' => 'Product Name',
            'code' => 'Product Code',
            'type' => 'Product Type',
            'description' => 'Description',
            'is_hot' => 'Is Hot',
            'product_category' => 'Product Category',
            'detail' => 'Detail',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],

        'document' => [
            'category' => 'Category',
        ],

        'partner' => [
            'name' => 'Name',
            'link' => 'Link',
            'image' => 'Image',
        ],
    ],

    'status' => [
        'active' => 'Active',
        'deactive' => 'Deactive',
    ],

    'lang' => [
        'available' => 'Available',
    ],

    'is_hot' => [
        'hot' => 'Hot',
        'nothot' => 'Not Hot',
        'active' => 'Active',
    ],
    
    'message' => [
        'user' => [
            'delete' => 'Delete successfully',
        ],

        'post' => [
            'publish' => 'Publish successfully',
            'un_publish' => 'Unpublish successfully',
        ],
    ],

];
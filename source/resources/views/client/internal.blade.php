@extends('layouts.client.master')
@section('main')
<style>
  .working{
    border: 1px solid rgba(0,0,0,0.4);
    width :100%;
  }
</style>
<main class="background-light-grey internal">
    <!-- Begin banner -->
    <section class="banner" style="background:url({{ asset('assets/client/images/b18.png')}}) center center / cover">
      <div class="d-flex align-items-center banner__bg mask">
        <div class="container">
          <h1 class="text-center banner__title">TVA GROUP nội bộ</h1>
        </div>
      </div>
    </section>
    <!-- End banner -->

    <div class="container mt-4 pt-2">
      <div class="row s-r-10">
        <div class="col-lg-8 col-xl-9 mb-4 pb-3 col-xl-72 s-p-10">
          <!-- Begin internal time line -->
          <div class="internal-time-line">
            <!-- Begin posting -->
            <div class="posting" id="Message">
              <textarea class="posting__ta contentMessage" rows="5" placeholder="Nội dung bạn muốn đăng bài…"></textarea>
              <div class="posting__action">
                <label class="posting__upload">
                  <input class="d-none fileUpload" type="file">
                  <img src="{{ asset('assets/client/images/upload.svg')}}" alt="">Upload file
                </label>
                <button class="btn-blue" onclick="addMessage()">Đăng tin</button>
              </div>
            </div>
            <!-- End posting -->
            <!-- // -->
            {{--  Begin Post_box  --}}
            <div id="Post_box">
            </div>
              {{--  End Post_box  --}}
          </div>
          <div class="pt-3 text-center" id="remove-row">
              <button id="btn-more" onclick="loadmoreMessage()" ><img src="{{ asset('assets/client/images/load.svg') }}" alt=""></button>
            </div>
          <!-- End internal time line -->
        </div>
        <div class="col-lg-4 col-xl-3 mb-4 col-xl-28 s-p-10">
          <!-- Begin internal bar -->
          <div class="internal-bar">
            <h2 class="internal-bar__title">
              <img src="{{ asset('assets/client/images/ic_receipt_white_24px.svg')}}" alt="">BIỂU MẪU</h2>
            <div class="internal-bar__content">
              <div class="mb-2 internal-bar__des">Các biểu mẫu bạn muốn xem</div>
              <ul id="listNewDocument">
                
              </ul>
              <a class="btn-blue" href="/form-list">Xem tất cả</a>
            </div>

          </div>

          <div class="internal-bar">
            <h2 class="internal-bar__title">
              <img src="{{ asset('assets/client/images/ic_list_white_24px.svg')}}" alt="">DANH SÁCH NHÂN VIÊN</h2>
            <div class="internal-bar__content">
              <div class="mb-3 internal-bar__des">Gợi ý các nhân viên bạn muốn xem</div>
              <ul id="StaffBar">

              </ul>
              <a class="btn-blue" href="/staff-list">Xem tất cả</a>
            </div>
          </div>
          <!-- End internal bar -->
        </div>
      </div>
    </div>
  </main>
@endsection
@section('script')
<script>
  var user = {!! json_encode(Auth::user()) !!};
</script>
<script src="{{ asset('assets/client/js/internal.js') }}"></script>
@endsection
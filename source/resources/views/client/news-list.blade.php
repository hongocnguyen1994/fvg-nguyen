@extends('layouts.client.master')
@section('main')
<script type="text/javascript">
  $(document).ready(function(){
   $(document).on('click', '#btn-more', function(){
       var id = $(this).data('id');
       $("#btn-more").html("Loading....");
       $.ajax({
           url : '{{ url("/news-list") }}',
           method : "POST",
           data : {id:id, _token:"{{csrf_token()}}"},
           success : function (data)
           {
              var timeUpdated = 0;
              if (data.data =='' ) return;
              if(data.data != '') {
                for(i=0; i<data.data.length;i++){ 
                  timeUpdated = new Date(data.data[i].published_at);
                  var hours = timeUpdated.getHours();
                  if(hours < 10) {
                    hours = '0' + hours;
                  }
                  var minutes = timeUpdated.getMinutes();
                  var day = timeUpdated.getDate();
                  var month = timeUpdated.getMonth()+1;
                  if(month < 10) {
                    month = '0' + month;
                  }
                  var year = timeUpdated.getFullYear();
                  if(data.data[i].image == 'default-featured-image.png') {
                    markup=`
                      <div class="col-md-6 col-xl-4 mb-4 pt-4">
                        <div class="news-list__item">
                          <div class="zoom-outer">
                            <img class="w-100 zoom-inner" src="{{ asset('assets/images/default-featured-image.png') }}" alt="">
                          </div>
                          <div class="p-4">
                            <a class="d-inline-block mw-100 mb-2 text-truncate news-list__title" href="/newsDetail/${data.data[i].id}">${data.data[i].name}</a>
                            <div class="d-flex align-items-center mb-3 news-list__time">
                              <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
                              <span class="mr-2 ml-1">${hours}:${minutes}</span>
                              <span class="ml-1">${day}-${month}-${year}</span>
                            </div>
                            <div class="news-list__summary">${data.data[i].description}</div>
                          </div>
                        </div>
                      </div>
                    `;
                  }
                  else {
                    markup=`
                      <div class="col-md-6 col-xl-4 mb-4 pt-4">
                        <div class="news-list__item">
                          <div class="zoom-outer">
                            <img class="w-100 zoom-inner" src="${data.data[i].image}" alt="">
                          </div>
                          <div class="p-4">
                            <a class="d-inline-block mw-100 mb-2 text-truncate news-list__title" href="/newsDetail/${data.data[i].id}">${data.data[i].name}</a>
                            <div class="d-flex align-items-center mb-3 news-list__time">
                              <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
                              <span class="mr-2 ml-1">${hours}:${minutes}</span>
                              <span class="ml-1">${day}-${month}-${year}</span>
                            </div>
                            <div class="news-list__summary">${data.data[i].description}</div>
                          </div>
                        </div>
                      </div>
                    `;
                  }
                  $("#btn-more").remove();
                  $('#more').append(markup);
                } 
                    markup=`
                      <div class="pt-3 text-center" id="remove-row">
                        <button id="btn-more" data-id="${data.data[data.data.length - 1].published_at}" ><img src="{{ asset('assets/client/images/load.svg') }}" alt=""></button>
                      </div>
                    `;
                  $('#loadMore').append(markup);
              }
           }
       });
   });  
}); 
</script>


    <main>
      <!-- Begin banner -->
      <section class="banner" style="background:url({{ asset('assets/client/images/b9.png') }}) center center / cover">
        <div class="d-flex align-items-center banner__bg mask">
          <div class="container">
            <h1 class="text-center banner__title">Tin tức TVA GROUP</h1>
          </div>
        </div>
      </section>
      <!-- End banner -->

      <!-- Begin news list -->
      <section class="news-list">
        <div class="container" id="loadMore">
          <h2 class="mb-4 text-center news-list__head">Tin tức tuyển dụng</h2>
          <div class="row pt-2" id="more">
            @foreach($posts as $post)
              <div class="col-md-6 col-xl-4 mb-4 pt-4">
                <div class="news-list__item">
                  <div class="zoom-outer">
                    @if($post->image == 'default-featured-image.png')
                      <img class="w-100 zoom-inner" src="{{ asset('assets/images/default-featured-image.png') }}" alt="">
                    @else
                      <img class="w-100 zoom-inner" src="{{ $post->image }}" alt="">
                    @endif
                  </div>
                  <div class="p-4">
                    <a class="d-inline-block mw-100 mb-2 text-truncate news-list__title" href="/newsDetail/{{ $post->id }}">{{ $post->name }}</a>
                    <div class="d-flex align-items-center mb-3 news-list__time">
                      <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
                      <?php $time = strtotime($post->updated_at); ?>
                      <span class="mr-2 ml-1">{{ date("H:i", $time) }}</span>
                      <span class="ml-1">{{ date("d-m-Y", $time) }}</span>
                    </div>
                    <div class="news-list__summary">{{ $post->description }}</div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="pt-3 text-center" id="remove-row">
            <button id="btn-more" data-id="<?php if(isset($post->updated_at)) echo $post->updated_at; ?>" ><img src="{{ asset('assets/client/images/load.svg') }}" alt=""></button>
          </div>
        </div>
      </section>
      <!-- End news list -->
    </main>
@endsection
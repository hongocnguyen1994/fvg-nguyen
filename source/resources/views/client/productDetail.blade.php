@extends('layouts.client.master')
@section('main')
    <main>
      <!-- Begin product detail -->
      <section class="product-detail">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 text-center">
              <img class="mw-100" src="{{ asset($product->image) }}" alt="">
            </div>
            <div class="col-lg-6">
              <h2 class="mb-3 product-detail__code">{{$product->code}}</h2>
              <h2 class="product-detail__title">{{$product->name}}</h2>
              <p class="product-detail__text">{{$product->type}}</p>
              <div class="mb-4 pt-2 line-grey"></div>
              <div>
                  @php
                    echo $product->detail;
                  @endphp
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End product detail -->

      <!-- Begin product one -->
      <section class="product-one pt-5 pb-4 m-2">
        <div class="container">
          <h2 class="text-center mb-4 pt-4 product-one__head">{{ $category->name}}</h2>
          <p class="text-center mx-auto mb-5 pb-3 product-one__des">
              {{ $category->description}}
          </p>
          <div id="productOne" class="owl-carousel owl-theme">
            @foreach($listproduct as $product)
            <div class="product-one__item" style="">
                <img src="{{ asset($product->image) }}" alt="" >
                <a class="d-flex align-items-center p-3 product-one__title" href="{{ route('productDetail',['id'=>$product->id]) }}">
                    <span class="product-one__text1" style="color: #3E4095;">{{ str_limit($product->code, 4) }}</span>
                      <span class="product-one__text2">{{ $product->name }}
                          <br>({{ $product->type }})</span>
                  </a>
              </div>
            @endforeach          
          </div>
        </div>
      </section>
      <!-- End product one -->

    </main>
@endsection
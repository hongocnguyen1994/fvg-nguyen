@extends('layouts.client.master')
@section('main')
  <style>
    .alertinfo{
      color : red;
    }
    #Alert{
      max-width:400px;
      display:block;
      position : absolute;
      top : 300px;
      left : 0;
      right : 0;
      margin : auto;
      z-index: 4;
    }
  </style>
    <main class="pt-5 contact">
      <div class="container pt-2">
        <h2 class="mb-4 pb-2 contact__text4">{{ trans('client.contact.withus')}}</h2>
        {{--  Liên hệ với chúng tôi  --}}
        <div class="row">
          <div class="col-md-6 mb-4 pb-3">
            <div class="contact__input" id="MailBox">
              <div class="mb-3 contact__text2">{{ trans('client.contact.fullname')}}</div><i id="name_alert" class="alertinfo"></i>
              <input class="mb-2 name" type="text" placeholder="{{ trans('client.contact.inputname')}}">
              <div class="mb-3 pt-3 contact__text2">{{ trans('client.contact.email')}}</div><i id="email_alert" class="alertinfo"></i>
              <input class="mb-2 email" type="email" placeholder="{{ trans('client.contact.inputemail')}}">
              <div class="mb-3 pt-3 contact__text2">{{ trans('client.contact.title')}}</div><i id="title_alert" class="alertinfo"></i>
              <input class="mb-2 title" type="text" placeholder="{{ trans('client.contact.inputtitle')}}">
              <div class="mb-3 pt-3 contact__text2">{{ trans('client.contact.content')}}</div><i id="content_alert" class="alertinfo"></i>
              <textarea class="mb-2 content" rows="5" placeholder="{{ trans('client.contact.inputcontent')}}"></textarea>
            </div>
            <div class="pt-3">
              <a class="btn-blue" href="javascript:void(0)" onclick="sendMail()">{{ trans('client.contact.send')}}</a>
            </div>
          </div>
          <div class="col-md-6 mb-4 pb-3" id="ContactInfo">
            <div class="mb-4 pt-4 contact__text3">{{ trans('client.contact.namecompany')}}</div>
            <div class="mb-3 pt-1">
              <span class="mr-1 contact__text2">{{ trans('client.contact.address')}}:</span>
              <span class="contact__text1 address"></span>
              {{--  Hà My Tây – Điện Dương – Điện Bàn – Quảng Nam  --}}
            </div>
            <div class="mb-3 pt-1">
              <span class="mr-1 contact__text2">{{ trans('client.contact.phone')}}:</span>
              <span class="contact__text1 phone"></span>
            </div>
            <div class="mb-3 pt-1">
              <span class="mr-1 contact__text2">{{ trans('client.contact.fax')}}:</span>
              <span class="contact__text1 fax"></span>
            </div>
            <div class="mb-3 pt-1">
              <span class="mr-1 contact__text2">{{ trans('client.contact.email')}}:</span>
              <span class="contact__text1 mail"></span>
            </div>
          </div>
        </div>      
      </div>
    </main>
    <div id="Alert">
       
    </div>
@endsection
@section('script')
    <script>
       name_alert = "{{ trans('client.contact.name_alert')}}";
       email_alert = "{{ trans('client.contact.email_alert')}}";
       title_alert = "{{ trans('client.contact.title_alert')}}";
       content_alert = "{{ trans('client.contact.content_alert')}}";
       success_alert = "{{ trans('client.contact.success_alert')}}";
       success_mess = "{{ trans('client.contact.success_mess')}}";
       email_null_alert = "{{ trans('client.contact.email_null_alert') }}";
    </script>
  <script src="{{ asset('assets/client/js/contact.js') }}"></script>
@endsection
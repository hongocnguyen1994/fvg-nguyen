@extends('layouts.client.master')
@section('main')
    <main>
      <!-- Begin product-new -->
      <section class="product-new">
        <div class="container">
          <div id="carouselOne" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              @php
                $index=0;
              @endphp
              @foreach($listproduct as $product)
                @if($product->is_hot)
                  @if($index==0)
                  <div class="carousel-item active">
                  @else 
                  <div class="carousel-item">
                  @endif
                  <div class="row">
                    <div class="col-md-6 mb-4">
                      <h2 class="text-center text-md-left mb-4 pt-2 product-new__head">{{$product->name}}</h2>
                      <p class="text-center text-md-left mx-auto ml-md-0 product-new__des">{{$product->description}}
                        </p>             
                      <div class="text-center text-md-left">
                        <a class="mt-3 btn-white" href="{{ route('productDetail',['id'=>$product->id]) }}">{{ trans('client.button.detail')}}</a>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4 text-center">
                      <img class="mw-100" src="{{ asset($product->image) }}" alt="">
                    </div>
                  </div>
                </div>  
                @php
                $index++;
                @endphp
                @endif
              @endforeach
            </div>

            <ol class="carousel-indicators">
              @for($j =0 ; $j < $index ; $j++)
                @if($j == 0)
                <li data-target="#carouselOne" data-slide-to="{{$j}}" class="active"></li>
                @else 
                <li data-target="#carouselOne" data-slide-to="{{$j}}"></li>
                @endif
              @endfor

            </ol>
          </div>
        </div>
      </section>
      <!-- End product-new -->

      <!-- Begin product one -->
      <section class="product-one pt-5 pb-4 m-2">
        <div class="container">
          <h2 class="text-center mb-4 pt-4 product-one__head">{{ $category->name}}</h2>
          <p class="text-center mx-auto mb-5 pb-3 product-one__des">
              {{ $category->description}}
          </p>
          <div id="productOne" class="owl-carousel owl-theme">
            @foreach($listproduct as $product)
            <div class="product-one__item" style="">
                <img src="{{ asset($product->image) }}" alt="" >
                <a class="d-flex align-items-center p-3 product-one__title" href="{{ route('productDetail',['id'=>$product->id]) }}">
                    <span class="product-one__text1" style="color: #3E4095;">{{ str_limit($product->code, 4) }}</span>
                      <span class="product-one__text2">{{ $product->name }}
                          <br>({{ $product->type }})</span>
                  </a>
              </div>
            @endforeach          
          </div>
        </div>
      </section>
      <!-- End product one -->

      <!-- Begin product two -->
      <!-- End product two -->

      <!-- Bengin partner -->

      <!-- End partner -->
    </main>
@endsection
@extends('layouts.client.master')
@section('main')
      <!-- Begin banner -->
      <main>
      <!-- Begin banner -->
      
      <section class="banner" style="background:url({{ asset('assets/client/images/b2.png') }}) center center / cover">
        
        <div class="banner__bg--lg d-flex align-items-center">
          <div class="container">
              
              @if (count($brand_product))
              @php
                $brand_product = $brand_product[0]
              @endphp 
            <h1 class="mb-4 banner__title banner__title--lg">{{ $brand_product->name }}</h1>
            <div class="mb-4 banner__text banner__text--lg">@php echo $brand_product->content @endphp</div>
            <a class="btn-white" href="{{ route('productList',['category_id'=>$brand_product->description]) }}">{{ trans('client.button.detail')}}</a>
            @endif
          </div>
        </div>
        
      </section>
      <!-- End banner -->

      <!-- Begin info -->
      <section class="info">
        <div class="row">
          <div class="col-lg-6 pr-0 pl-0">
            <div class="zoom-outer">
              <div class="jsSetRate-7-5 zoom-inner" style="background:url({{ asset('assets/client/images/b3.png') }}) center center / cover"></div>
            </div>
          </div>
          <div class="col-lg-6 info__bg">
            <div class="d-flex flex-column justify-content-center align-items-center align-items-lg-start h-100 pt-5 pb-5 mr-lg-4 ml-lg-4 mr-xl-5 ml-xl-5 pr-xl-5 text-center text-lg-left">
                @if (count($brand_intro))
                @php
                $brand_intro = $brand_intro[0]
              @endphp
              <h2 class="mb-4 info__title">{{ $brand_intro->name }}</h2>
              <div class="mb-3 info__text">@php echo $brand_intro->content @endphp</div>
                @endif
              <a class="mt-3 btn-white" href="{{asset('about')}}">{{ trans('client.button.detail')}}</a>
            </div>
          </div>
        </div>
      </section>
      <!-- End info -->

      <!-- Bengin partner -->
      <section class="partner">
        <div class="container">
          <h2 class="text-center partner__title">{{ trans('client.page.partner')}}</h2>
          <div class="jsPartnerOwlCarousel owl-carousel owl-theme">
            @if (count($partners))
            @foreach($partners as $partner)
                <div class="p-1">
                  @if(substr($partner->link, 0, 7) == "http://" || substr($partner->link, 0, 8) == "https://")
                    <a class="partner__item" href="{{ $partner->link }}">
                      <img src="{{ asset('storage/'.$partner->image) }}" alt="">
                    </a>
                  @else
                    <a class="partner__item" href="http://{{ $partner->link }}">
                      <img src="{{ asset('storage/'.$partner->image) }}" alt="">
                    </a>
                  @endif
                </div>
            @endforeach
            @endif
          </div>
        </div>
      </section>
      <!-- End partner -->
    </main>


@endsection

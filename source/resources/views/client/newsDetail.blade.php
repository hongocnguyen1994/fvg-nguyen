@extends('layouts.client.master')
@section('main')
    <main>
      @foreach($posts as $post)
      <!-- Begin banner -->
      <section class="banner" style="background:url({{ asset('assets/client/images/b16.png') }}) center center / cover">
        <div class="d-flex align-items-center banner__bg mask">
          <div class="container">
            <h1 class="text-center banner__title">{{ $post->name }}</h1>
          </div>
        </div>
      </section>
      <!-- End banner -->
      <div class="container pt-5 mt-2">
        <div class="row">
          <div class="col-xl-8 mb-5">
            <h2 class="mb-3 news-detail__title1">{{ $post->description }}</h2>
            <div class="d-flex align-items-center mb-4 pt-1 news-list__time">

              <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
              <?php $time = strtotime($post->published_at); ?>
              <span class="mr-2 ml-1">{{ date("H:i", $time) }}</span>
              <span class="ml-1">{{ date("d-m-Y", $time) }}</span>
            </div>
            <div class="pt-2 new-detail__content">
              {{ strip_tags($post->content, 'b') }}
            </div>
          </div>
        @endforeach
        
          <div class="col-xl-4 mb-5">
            <!-- Begin news involve -->
            <h2 class="mb-4 news-detail__title2">Tin tức gần nhất</h2>
            <div class="row">
              @foreach($postsRelate as $postRelate)
              <div class="col-md-6 col-xl-12 mb-4 pt-1">
                <div class="news-list__item">
                  <div class="zoom-outer">
                    @if($postRelate->image == 'default-featured-image.png')
                      <img class="w-100 zoom-inner" src="{{ asset('assets/images/default-featured-image.png') }}" alt="">
                    @else
                      <img class="w-100 zoom-inner" src="{{ $postRelate->image }}" alt="">
                    @endif
                  </div>
                  <div class="p-4">
                    <a class="d-inline-block mw-100 mb-2 text-truncate news-list__title" href="/newsDetail/{{ $postRelate->id }}">{{ $postRelate->name }}</a>
                    <div class="d-flex align-items-center news-list__time">
                      <img class="mr-2" src="{{ asset('assets/client/images/time.svg') }}" alt="">
                      <?php $time = strtotime($postRelate->published_at); ?>
                      <span class="mr-2 ml-1">{{ date("H:i", $time) }}</span>
                      <span class="ml-1">{{ date("d-m-Y", $time) }}</span>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              <div class="w-100 pt-1 text-center">
                <a class="btn-blue" href="{{ asset('/news-list') }}">Xem thêm</a>
              </div>
            </div>
            <!-- End news involve -->
          </div>
        </div>
      </div>
    </main>

@endsection
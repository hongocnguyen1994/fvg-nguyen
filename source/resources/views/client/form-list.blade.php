@extends('layouts.client.master')
@section('main')
<main class="show-list">
    <div class="container">
      <div class="d-md-flex justify-content-md-between align-items-md-center pb-2 show-list__head">
        <h2 class="mb-2 show-list__title">
          <img src="{{ asset('/assets/client/images/ic_receipt_white_24px.svg') }}" alt=""> DANH SÁCH BIỂU MẪU</h2>
        <input class="mb-2 show-list__search" type="text" onkeyup="searchDocument(this.value)" onkeydown="searchDocument(this.value)" placeholder="Tìm kiếm biểu mẫu">
      </div>
      <div class="show-list__content">
        <!-- Begin nav tabs -->
        <ul class="nav d-flex flex-wrap justify-content-center" id="listContent">
        </ul>
        <!-- End nav tabs -->

        <!-- Begin tab panes -->
        <div class="tab-content">
          <div class="tab-pane fade show active">
            <div class="row s-r-10" id="tabContent">
              
            </div>
            <div class="pt-2 text-center">
              <a class="show-list__load" onclick="loadmore()" href="javascript:void(0)">
                              <img src="{{ asset('assets/client/images/load.svg') }}" alt="">Tải thêm</a>
            </div>
          </div>
          
        </div>
        <!-- End tab panes -->
      </div>
    </div>
  </main>
@endsection
@section('script')
<script src="{{asset('assets/client/js/form_list.js')}}"></script>
@endsection
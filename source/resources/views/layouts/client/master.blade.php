<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
   <link rel="shortcut icon" href="{{ asset('assets/client/images/favicon.ico') }}" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>THÁI VIỆT</title>
  <link rel="stylesheet" href="{{ asset('assets/client/css/vendor.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/client/css/main.css') }}">
  <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  
  
</head>

<body>
  <div class="wrapper">
    <!-- Begin header -->
    <header class="header">
      <div class="container">
        <div class="row align-items-center pt-3 pb-3">
          <div class="col-sm-8 col-xl-1 mb-3 mb-xl-0 text-center text-sm-left">
              
            <a class="header__logo" href="/"><img src="{{ asset('assets/client/images/logo1.png') }}" alt=""></a>
          </div>
          <div class="order-xl-2 col-sm-4 col-xl-2 d-flex justify-content-center align-items-center justify-centent-sm-end mb-3 mb-xl-0">
            @guest
              <a class="header__login jsCallModalLogin" href="javascript:void(0)">{{ trans('client.button.login') }}</a>
            @else
              <div class="dropdown header__noti" id="notification">
                <div class="header__noti-btn" data-toggle="dropdown">
                  <img src="{{ asset('assets/client/images/noti.svg')}}" alt="">
                  <span class="header__noti-st"></span>
                </div>
                <div class="dropdown-menu dropdown-menu-header">
                  <div class="postNotification">
                    
                  </div>
                  <div class="MessageNotification">
                    
                  </div>
                  <div class="commentNotification">
                    
                  </div>
                  <div class="text-center pt-2"><a class="noti__more" href="#">Xem tất cả</a></div>
                  <span class="dropdown-menu-triangle"></span>
                </div>
              </div>
              <div class="dropdown ml-3 header__avata">
                <img class="" src="{{ asset('assets/images\/').Auth::user()->avatar }}" alt="" data-toggle="dropdown">
                <div class="dropdown-menu dropdown-menu-header">
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">{{ trans('client.button.logout') }}</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                  <span class="dropdown-menu-triangle"></span>
                </div>
              </div>
            @endguest
            <a class="header__lan" href="@if(app()->getLocale() == 'vi') {{ route('get_switch_lang', 'en')}} @else {{ route('get_switch_lang', 'vi')}} @endif">
              @if(app()->getLocale() == 'vi')
                  <img src="{{ asset('assets/images/en.png') }}" class="user-image" alt="User Image">
                  {{--  <span class="hidden-xs">Switch To English</span>  --}}
              @else
                  <img src="{{ asset('assets/images/vi.png') }}" class="user-image" alt="User Image">
                  {{--  <span class="hidden-xs">Switch To Vietnamese</span>  --}}
              @endif
          </a>
            {{--  <a class="header__lan" href="#">
                    <img src="{{ asset('assets/client/images/flag1.png') }}" alt="">
                </a>  --}}
          </div>
          <div class="order-xl-1 col-xl-9 d-xl-flex justify-content-xl-end pr-xl-0">
            <nav class="navbar navbar-expand-lg justify-content-end pr-xl-0">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <img src="{{ asset('assets/client/images/ic_reorder_black_24px.svg') }}" alt="">
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ asset('about')}}">{{ trans('client.menu.about') }}</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ trans('client.menu.product') }}
                                </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="menuProduct">
                      {{--  <a class="dropdown-item" href="/productList">{{ trans('client.menu.productlist') }}</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>  --}}
                    </div>
                  </li>
                  <li class="nav-item">
                    @guest
                      <a class="nav-link jsCallModalLogin" href="javascript:void(0)">{{ trans('client.menu.internal') }}</a>
                    @else 
                      <a class="nav-link" href="{{ asset('/internal')}}">{{ trans('client.menu.internal') }}</a>
                    @endguest
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ asset('news-list')}}">{{ trans('client.menu.news') }}</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ asset('hiring-list')}}">{{ trans('client.menu.recruitment') }}</a>
                  </li>
                  <li class="nav-item mr-xl-0">
                    <a class="nav-link" href="{{ asset('contact')}}">{{ trans('client.menu.contact') }}</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>

        </div>
      </div>
    </header>
    <!-- End header -->
      @yield('main')
    <!-- Begin footer -->
    <footer class="footer" style="background:url({{ asset('assets/client/images/b1.png') }}) center center / cover">
      <div class="pt-5 pb-5 footer__bg">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-xl-4">
              <div class="row">
                <div class="col-sm-7 col-xl-8 mb-4 mb-xl-0">
                  <img class="mw-100 mb-3" src="{{ asset('assets/client/images/logo2.png') }}" alt="">
                  <div class="footer__text">Copyright © 2017 {{ trans('client.name.company') }}
                    <br>{{ trans('client.name.companyTV') }}</div>
                </div>
                <div class="col-sm-5 col-xl-4 d-flex flex-column mb-4 mb-xl-0">
                  <div class="mb-3 footer__title">{{ trans('client.menu.menu') }}</div>
                  <a class="mb-2 footer__text" href="{{ asset('about')}}">{{ trans('client.menu.about') }}</a>
                  <a class="mb-2 footer__text" href="#">{{ trans('client.menu.product') }}</a>
                  @guest
                      <a class="mb-2 footer__text jsCallModalLogin" href="javascript:void(0)">{{ trans('client.menu.internal') }}</a>
                    @else 
                      <a class="mb-2 footer__text" href="{{ asset('internal')}}">{{ trans('client.menu.internal') }}</a>
                    @endguest
                  <a class="mb-2 footer__text" href="{{ asset('news-list')}}">{{ trans('client.menu.news') }}</a>
                  <a class="mb-2 footer__text" href="{{ asset('hiring-list')}}">{{ trans('client.menu.recruitment') }}</a>
                  <a class="mb-2 footer__text" href="{{ asset('contact')}}">{{ trans('client.menu.contact') }}</a>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-xl-2 pr-xl-0 mb-4 mb-xl-0" id="InfoCompany">
              <div class="mb-3 footer__title">{{ trans('client.infomation.infomation') }}</div>
              <div class="mb-1 footer__title">{{ trans('client.infomation.address') }}</div>
              {{--  <div class="mb-2 footer__text">Tầng 15 - 02 Quang Trung, Hải Châu, Đà Nẵng</div>  --}}
              <div class="mb-1 footer__title office"></div>
              <div class="mb-1 footer__title">{{ trans('client.infomation.phonenumber') }}</div>
              <div class="mb-2 footer__text phone"></div>
              <div class="mb-1 footer__title">{{ trans('client.infomation.mail') }}</div>
              <div class="mb-2 footer__text mail"></div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-2 mb-4 mb-sm-0">
              <div class="mb-3 footer__title">{{ trans('client.follow.follow') }}</div>
              <div class="mb-3 footer__social">
                <a href="#">
                            <img src="{{ asset('assets/client/images/facebook-with-circle.svg') }}" alt="">
                        </a>
                <a href="#">
                            <img src="{{ asset('assets/client/images/youtube-with-circle.svg') }}" alt="">
                        </a>
                <a href="#">
                            <img src="{{ asset('assets/client/images/google+-with-circle.svg') }}" alt="">
                        </a>
              </div>
              <div class="d-flex align-items-center">
                <div class="mr-1 footer__text">{{ trans('client.follow.designed') }}</div>
                <a class="footer__author" href="http://minaworks.vn/" target="_blank"><img src="{{ asset('assets/client/images/mw.png') }}" alt=""></a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-4">
              <img class="w-100" src="{{ asset('assets/client/images/gmap.png') }}" alt="">
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- End footer -->

    <!-- Begin modal login -->
    <div class="jsModalLogin modal fade modal-login" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">{{ trans('client.login.login') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
          </div>
          <div class="modal-body">
            <div class="container">
              <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="" placeholder="Email" name="email">
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ trans('client.login.error.email') }}</strong>
                      </span>
                  @endif
                  {{--  We'll never share your email with anyone else.  --}}
                  <small id="emailHelp" class="form-text text-muted">{{ trans('client.login.login_policy') }}</small>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ trans('client.login.error.password') }}</strong>
                      </span>
                  @endif
                </div>
                <p class="mb-2 pt-1 pb-1 text-right"><button type="submit" class="btn btn-primary">{{ trans('client.button.login') }}</button></p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End modal login -->
  </div>
  <script src="{{ asset('assets/client/js/vendor.js') }}"></script>
  <script src="{{ asset('assets/client/js/main.js') }}"></script>
  @guest
  @else
    <script src="{{ asset('assets/client/js/listenNotification.js') }}"></script>
  @endguest
  <script src="{{ asset('assets/client/js/loadInfoCompany.js') }}"></script>
  @if ($errors->has('email') || $errors->has('password') )
    <script>
      $( document ).ready(function() {
        $(".jsModalLogin").modal("show");
      });
    </script>
  @endif 
  @yield('script')
</body>

</html>
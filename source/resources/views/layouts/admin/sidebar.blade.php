<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/images/'.Auth::user()->avatar) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->username }}</p>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
                <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>{{ trans('admin.sidebar.dashboard.index') }}</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                          </span>
                      </a>
                      <ul class="treeview-menu">
                        <li class="side_link"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.dashboard.index') }}</a></li>
                        
                    </ul>
                </li>
                <li class="side_link">
                    <a href="{{ route('admin.department')}}">
                        <i class="fa fa-dashboard"></i> <span>{{ trans('admin.sidebar.department.index') }}</span>
                  </a>
            </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>{{ trans('admin.sidebar.user.manager') }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="side_link"><a href="{{ route('admin.user.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.user.list') }}</a></li>
                    <li class="side_link"><a href="{{ route('admin.user.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.user.add') }}</a></li>
                    @if(isset($user) && !isset($users))
                        @if(url()->current() == route('admin.user.show', $user))
                            <li class="side_link"><a href="{{ route('admin.user.show', $user) }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.user.show') }} {{ str_limit($user->email, 10) }}</a></li>
                        @else
                            <li class="side_link"><a href="{{ route('admin.user.edit', $user) }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.user.edit') }}: {{ str_limit($user->email, 10) }}</a></li>
                        @endif
                    @endif
                    @if(isset($users) && isset($request))
                         <li class="side_link"><a href="{{ route('admin.user.search') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.user.search') }}</a></li>
                    @endif
                </ul>
            </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-users"></i> <span>{{ trans('admin.sidebar.role.manager') }}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="side_link"><a href="{{ route('admin.role.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.role.list') }}</a></li>
            <li class="side_link"><a href="{{ route('admin.role.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.role.add') }}</a></li>
            @if(isset($role) && !isset($roles))
                @if(url()->current() == route('admin.role.show', $role))
                    <li class="side_link"><a href="{{ route('admin.role.show', $role) }}"><i class="fa fa-circle-o"></i>{{ trans('admin.sidebar.role.show').": ".$role->name }} </a></li>
                 @else
                    <li class="side_link"><a href="{{ route('admin.role.edit', $role) }}"><i class="fa fa-circle-o"></i>{{ trans('admin.sidebar.role.edit').": ".$role->name }}</a></li>
                @endif
            @endif
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-list"></i> <span>{{ trans('admin.sidebar.category.manager') }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li class="side_link"><a href="{{ route('category.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.category.list') }}</a></li>
        <li class="side_link"><a href="{{ route('category.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.category.add') }}</a></li>
        <li class="side_link"><a href="{{ route('category.get_deleted') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.category.deleted') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-send"></i> <span>{{ trans('admin.sidebar.post.manager') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="side_link"><a href="{{ route('admin.post.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.post.list') }}</a></li>
            <li class="side_link"><a href="{{ route('admin.post.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.post.add') }}</a></li>
            <li class="side_link"><a href="{{ route('admin.post.get_deleted') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.post.deleted') }}</a></li>
            @if(isset($post) && !isset($posts))
                @if(url()->current() == route('admin.post.show', $post))
                    <li class="side_link"><a href="{{ route('admin.post.show', $post) }}"><i class="fa fa-circle-o"></i> {{ str_limit($post->name, 20) }}</a></li>
                @else
                    <li class="side_link"><a href="{{ route('admin.post.edit', $post) }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.post.edit') }}</a></li>
                @endif
            @endif
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
            <i class="fa fa-list"></i> <span>{{ trans('admin.sidebar.category_product.manager') }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li class="side_link"><a href="{{ route('product-category.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.category_product.list') }}</a></li>
        <li class="side_link"><a href="{{ route('product-category.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.category_product.add') }}</a></li>
        <li class="side_link"><a href="{{ route('product-category.get_deleted') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.category_product.deleted') }}</a></li>
      </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-send"></i> <span>{{ trans('admin.sidebar.product.manager') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="side_link"><a href="{{ route('admin.product.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.product.list') }}</a></li>
            <li class="side_link"><a href="{{ route('admin.product.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.product.add') }}</a></li>
            <li class="side_link"><a href="{{ route('admin.product.get_deleted') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.product.deleted') }}</a></li>
            @if(isset($product) && !isset($products))
                @if(url()->current() == route('admin.product.show', $product))
                    <li class="side_link"><a href="{{ route('admin.product.show', $product) }}"><i class="fa fa-circle-o"></i> {{ str_limit($product->name, 20) }}</a></li>
                @else
                    <li class="side_link"><a href="{{ route('admin.product.edit', $product) }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.product.edit') }}</a></li>
                @endif
            @endif
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-address-card-o"></i> <span>{{ trans('admin.sidebar.contact.manager') }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li class="side_link"><a href="{{ route('admin.contact.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.sidebar.contact.list') }}</a></li>
      </ul>
    </li>

    <li class="treeview option">
        <a href="#">
          <i class="fa fa-file"></i> <span>{{ trans('admin.sidebar.document.manager') }}</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
          <ul class="treeview-menu">
            <li class="side_link">
              <a href="{{ route('admin.document_category') }}"><i class="fa fa-circle-o"></i>{{ trans('admin.sidebar.document.category') }}</a>
            </li>
            <li class="side_link">
                <a href="{{ route('admin.document') }}"><i class="fa fa-circle-o"></i>{{ trans('admin.sidebar.document.list') }}</a>
              </li>
        </ul>
    </li>
    <li class="treeview option">
        <a href="#">
          <i class="fa fa-address-book"></i> <span>{{ trans('admin.sidebar.partner.manager') }}</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
          <ul class="treeview-menu">
            <li class="side_link">
              <a href="{{ route('admin.partner.index') }}"><i class="fa fa-circle-o"></i>{{ trans('admin.sidebar.partner.list') }}</a>
            </li>
            <li class="side_link">
                <a href="{{ route('admin.partner.create') }}"><i class="fa fa-circle-o"></i>{{ trans('admin.sidebar.partner.add') }}</a>
              </li>
        </ul>
    </li>
</ul>
</section>
<!-- /.sidebar -->
</aside>
<script>
    $(document).ready(function () {
       function getPathFromUrl(url) {
           return url.split("?")[0];
       }
       var href = getPathFromUrl(window.location.href);
       $('.side_link').each(function () {
        var link_href = $(this).find('a').attr('href');
        if(link_href == href){
            $(this).addClass('active');
            $(this).parent().parent().addClass('active menu-open');
        }
    });
   });
</script>
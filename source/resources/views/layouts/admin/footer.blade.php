<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; {{ date('Y', time()) }} <a href="http://minacms.com">MINACMS</a>.</strong> All rights
    reserved.
</footer>
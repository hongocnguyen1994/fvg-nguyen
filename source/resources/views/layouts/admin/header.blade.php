<header class="main-header">
    <!-- Logo -->
    <a href="/admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CMS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>MINA</b>CMS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="user user-menu">
                    <a href="@if(app()->getLocale() == 'vi') {{ route('get_switch_lang', 'en')}} @else {{ route('get_switch_lang', 'vi')}} @endif">
                        @if(app()->getLocale() == 'vi')
                            <img src="{{ asset('assets/images/en.png') }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">Switch To English</span>
                        @else
                            <img src="{{ asset('assets/images/vi.png') }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">Switch To Vietnamese</span>
                        @endif
                    </a>
                </li>
                <!-- Messages: style can be found in dropdown.less-->
                {{--@include('layouts.admin.message')--}}
                {{--<!-- Notifications: style can be found in dropdown.less -->--}}
                {{--@include('layouts.admin.notification')--}}
                {{--<!-- Tasks: style can be found in dropdown.less -->--}}
                {{--@include('layouts.admin.task')--}}
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('assets/images/'.Auth::user()->avatar) }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->email }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset('assets/images/'.Auth::user()->avatar) }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->email }}
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('admin.user.get_profile') }}" class="btn btn-default btn-flat">{{ trans('admin.button.profile') }}</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat">{{ trans('admin.button.logout') }}</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>
@extends('layouts.admin.master')
@section('title', 'Search Result')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ trans('admin.search.result') }}</h3>
                    @include('admin.category.search_form')
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('admin.table.name') }}</th>
                                <th>{{ trans('admin.table.description') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                <th>{{ trans('admin.table.updated_at') }}</th>
                                <th>{{ trans('admin.table.status') }}</th>
                                <th>{{ trans('admin.table.function') }}</th>
                                <th>{{ trans('admin.table.current_language') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td><a href="{{ route('product-category.show', $category) }}">{{ str_limit($category->str.$category->name,20) }}</a></td>
                                <td>{{ str_limit($category->description) }}</td>
                                <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($category->created_at))->diffForHumans() }}</td>
                                <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($category->updated_at))->diffForHumans() }}</td>
                                <td>
                                    @if($category->status == \App\Category::ACTIVED)
                                    <span class="label label-success">{{ trans('admin.status.active') }}</span>
                                    @else
                                    <span class="label label-danger">{{ trans('admin.status.deactive') }}</span>
                                    @endif
                                </td>
                                <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        <span class="caret"></span> {{ trans('admin.button.function') }}
                                    </button>
                                    <ul class="dropdown-menu">
                                       
                                        <li>
                                            <a href="{{ route('product-category.edit', $category) }}">{{ trans('admin.button.edit')  }}</a>
                                        </li>
                                        <li>
                                            <form class="form_delete_{{ $category->id }}" action="{{ route('product-category.destroy', $category) }}" method="post">
                                                {{ method_field('delete') }}
                                                {{ csrf_field() }}
                                            </form>
                                            <a target-id="{{ $category->id }}" href="javascript:void(0)" class="delete">{{ trans('admin.button.delete') }}</a>
                                        </li>
                                    </ul>
                                </div>
                                </td>
                                <td>
                                    @if($category->hasTranslation())
                                    <span class="label label-success">{{ trans('admin.lang.available') }}</span>
                                    @else
                                    <span class="label label-danger">{{ trans('admin.lang.unvailable') }}</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
            <div class="box-footer clearfix">
            </div>
        </div>
    </div>
</div>
</section>
@endsection
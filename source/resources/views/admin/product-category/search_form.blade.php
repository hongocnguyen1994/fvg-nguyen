<div class="box-tools">
    <form action="{{ route('product-category.search') }}" method="get">
        <div class="input-group input-group-sm" style="width: 150px;">
            <input value="@if(isset($request)){{ $request->keyword }}@endif" type="text" name="keyword" class="form-control pull-right" placeholder="{{ trans('admin.place.category_product.search') }}">

            <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</div>
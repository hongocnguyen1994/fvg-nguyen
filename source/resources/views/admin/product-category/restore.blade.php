@extends('layouts.admin.master')
@section('title', trans('admin.title.category_product.restore'))
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>{{ trans('admin.table.parent') }}</th>
								<th>{{ trans('admin.table.name') }}</th>
								<th>{{ trans('admin.table.position') }}</th>
								<th>{{ trans('admin.table.created_at') }}</th>
								<th>{{ trans('admin.table.deleted_at') }}</th>
								<th>{{ trans('admin.table.status') }}</th>
								<th>{{ trans('admin.table.restore') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($categories as $category)
							<tr>
								<td>
									@foreach($list as $value)
										@if($category->parent_id == $value->id)
											{{ str_limit($value->name, 20) }}
										@endif
									@endforeach
								</td>
								<td>{{ str_limit($category->name, 20) }}</td>
								<td>{{ $category->position }}</td>							
								<td>{{ $category->created_at }}</td>
								<td>{{ $category->deleted_at }}</td>
								<td>
									@if($category->status == 1)
									<span class="label label-success">{{ trans('admin.status.active') }}</span>
									@else
									<span class="label label-danger">{{ trans('admin.status.deactive') }}</span>
									@endif
								</td>
								<td>
									<form action="{{ route('product-category.restore', $category->id) }}" method="post">
										{{ method_field('put') }}
										{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">Restore</button>
									</form>
								</td>
							</tr>
							@endforeach							
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix">
					{{ $categories->render() }}
				</div>
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
@endsection
@extends('layouts.admin.master')
@section('title', trans('admin.title.product.add'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-9">
                <form id="product" action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            {{ csrf_field() }}
                            <div class="form-group @if($errors->has('name')) has-error @endif">
                                <label for="name">{{ trans('admin.label.product.name') }}<span class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control required" placeholder="{{ trans('admin.place.product.name') }}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('code')) has-error @endif">
                                <label for="code">{{ trans('admin.label.product.code') }}<span class="text-danger">*</span></label>
                                <input type="text" name="code" class="form-control required" placeholder="{{ trans('admin.place.product.code') }}">
                                @if($errors->has('code'))
                                    <p class="text-danger">{{ $errors->first('code') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('type')) has-error @endif">
                                <label for="type">{{ trans('admin.label.product.type') }}<span class="text-danger">*</span></label>
                                <input type="text" name="type" class="form-control required" placeholder="{{ trans('admin.place.product.type') }}">
                                @if($errors->has('type'))
                                    <p class="text-danger">{{ $errors->first('type') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('description')) has-error @endif">
                                <label for="name">{{ trans('admin.label.product.description') }}<span class="text-danger">*</span></label>
                                <textarea class="form-control required" name="description" rows="5" placeholder="{{ trans('admin.place.product.description') }}"></textarea>
                                @if($errors->has('description'))
                                    <p class="text-danger">{{ $errors->first('description') }}</p>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('is_hot') ? ' has-error' : '' }}">
                                <label>{{ trans('admin.label.product.is_hot') }}<span class="text-danger">*</span></label><br>
                                <input type="radio" name="is_hot" id="hot" value="1"> <label for="is_hot">{{ trans('admin.is_hot.hot') }}</label>
                                <input type="radio" name="is_hot" id="nothot" value="0" checked> <label for="nothot">{{ trans('admin.is_hot.nothot') }}</label>
                                @if ($errors->has('is_hot'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('is_hot') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('product_category')) has-error @endif">
                                <label for="product_category">{{ trans('admin.label.product.product_category') }}<span class="text-danger">*</span></label>
                                <select id="category" class="form-control" name="product_category">
                                    @php
                                        \App\ProductCategory::softCategory(\App\ProductCategory::all(), null, '');
                                        $product_categories = \App\ProductCategory::$lists;
                                    @endphp
                                    @foreach($product_categories as $product_category)
                                        <option value="{{ $product_category->id }}">{{ $product_category->str.' '.$product_category->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('product_category'))
                                    <p class="text-danger">{{ $errors->first('product_category') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <input name="image" id="image" type="hidden" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="detail">{{ trans('admin.label.product.detail') }}<span class="text-danger">*</span></label>
                                <textarea class="required" id="detail" name="detail">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> {{ trans('admin.button.cancel') }}</a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('admin.button.create') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="image_featured">{{ trans('admin.label.product.image') }}</label>
                            <button class="btn btn-success btn-block" id="image_featured">{{ trans('admin.button.product_image') }}</button>
                            <br>
                            <img src="" style="width:100%; height: 100%; display: none" id="image_demo">
                            <p class="text-center">
                                <a style='display: none' href="javascript:void(0)" id="reset" class="btn btn-link">{{ trans('admin.button.cancel') }}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#product").validate({
            errorClass: 'text-danger',
            errorElement: 'p',
            highlight: function (element) {
                $(element).parents('div.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).parents('div.form-group').removeClass('has-error');
            },
            ignore: [],
            rules: {
                cktext: {
                    required: function() {
                        CKEDITOR.instances.cktext.updateElement();
                    },
                    minlength: 10
                }
            }
        });
        $(document).ready(function () {
            function reset() {
                $("#image_demo").attr('src', '').hide();
                $("#image").val('');
            }

            $("#reset").on('click', function () {
                reset();
                $(this).hide();
            });

            var button1 = document.getElementById( 'image_featured' );
            button1.onclick = function() {
                selectFileWithCKFinder( 'image' );
            };
            function selectFileWithCKFinder( elementId ) {
                CKFinder.modal( {
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function( finder ) {
                        finder.on( 'files:choose', function( evt ) {
                            var file = evt.data.files.first();
                            var output = document.getElementById( elementId );
                            output.value = file.getUrl();
                            document.getElementById("image_demo").src = file.getUrl();
                            $("#image_demo").show();
                            $("#reset").show();
                        } );

                        finder.on( 'file:choose:resizedImage', function( evt ) {
                            var output = document.getElementById( elementId );
                            output.value = evt.data.resizedUrl;
                        } );
                    }
                } );
            }
        });
    </script>
    <script>
        CKEDITOR.replace( 'detail', {
            filebrowserBrowseUrl: '{{ asset('assets/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('assets/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserUploadUrl: '{{ asset('assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}', // Define the toolbar: http://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_toolbar,
            toolbar: [
                { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                { name: 'styles', items: [ 'Styles', 'Format' ] },
                { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
                { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                { name: 'insert', items: [ 'Image', 'EmbedSemantic', 'Table' ] },
                { name: 'tools', items: [ 'Maximize' ] },
                { name: 'editing', items: [ 'Scayt' ] },
                { name: 'source', items: ['Source'  ]}
            ],
            customConfig: '',
            allowedContent: true,
            height: 500
        } );
    </script>
@endsection
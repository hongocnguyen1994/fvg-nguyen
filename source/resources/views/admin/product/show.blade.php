@extends('layouts.admin.master')
@section('title', str_limit($product->name, 20))
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @include('admin.product.search_form')
                </div>
                <div class="box-body">
                    <a href="{{ route('admin.product.edit', $product) }}" class="btn btn-warning">{{ trans('admin.button.edit') }}</a>
                    <h4><strong>{{ trans('admin.label.product.name').": ". $product->name }} : </strong></h4>
                    <p>
                        <small>{{ trans('admin.label.product.created_at') }} : {{ \Carbon\Carbon::createFromTimestamp(strtotime($product->created_at))->format('h:i:s d-m-Y') }}</small>
                    </p>
                    <p><strong>Code: {{ $product->code }}</strong></p>
                    <label>{{ trans('admin.label.product.type') }}</label>
                    <p>{{ $product->type }}</p>
                    <label>{{ trans('admin.label.product.is_hot') }}</label>
                    <p>
                        @if($product->is_hot == \App\Product::HOT)
                        <span class="label label-success">{{ trans('admin.is_hot.active') }}</span>
                        @else
                        <span class="label label-danger">{{ trans('admin.is_hot.deactive') }}</span>
                        @endif
                    </p>
                    <label>{{ trans('admin.label.product.product_category') }}</label>
                    <p>{{ $product->category->name }}</p>                        
                    <label>{{ trans('admin.label.product.description') }}</label>
                    <p>{{ $product->description }}</p>
                    <label>{{ trans('admin.label.product.detail') }}</label>
                    <p>
                        {!! $product->detail !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
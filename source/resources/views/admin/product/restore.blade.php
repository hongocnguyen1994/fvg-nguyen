@extends('layouts.admin.master')
@section('title', trans('admin.title.product.restore'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        {{--@include('admin.product.search_form')--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.name') }}</th>
                                <th>{{ trans('admin.table.code') }}</th>
                                <th>{{ trans('admin.table.type') }}</th>
                                <th>{{ trans('admin.table.category') }}</th>
                                <th>{{ trans('admin.table.status') }}</th>
                                <th>{{ trans('admin.table.deleted_at') }}</th>
                                <th>{{ trans('admin.table.restore') }}</th>
                            </tr>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td><a target="_blank" href="{{ route('admin.product.show', $product) }}">{{ str_limit($product->name,20) }}</a></td>
                                    <td>{{ str_limit($product->code) }}</td>
                                    <td>{{ str_limit($product->type) }}</td>
                                    <td>{{ str_limit($product->category->name, 20) }}</td>
                                    <td>
                                        @if($product->status == \App\ProductCategory::ACTIVED)
                                            <span class="label label-success"><i class="fa fa-check"></i> {{ trans('admin.status.active') }}</span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('admin.status.deactive') }}</span>
                                        @endif
                                    </td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($product->deleted_at))->diffForHumans() }}</td>
                                    <td>
                                        <form action="{{ route('admin.product.restore', $product) }}" method="post">
                                            {{ method_field('put') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-undo"></i> {{ trans('admin.button.restore') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $products->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
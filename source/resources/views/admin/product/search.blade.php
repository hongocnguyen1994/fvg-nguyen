@extends('layouts.admin.master')
@section('title', trans('admin.title.product.search'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('admin.title.product.search') }}</h3>
                        @include('admin.product.search_form')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.name') }}</th>
                                <th>{{ trans('admin.table.category') }}</th>
                                <th>{{ trans('admin.table.position') }}</th>
                                <th>{{ trans('admin.table.status') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                
                                <th>{{ trans('admin.table.function') }}</th>
                                <th>{{ trans('admin.table.current_language') }}</th>
                            </tr>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td><a target="_blank" href="{{ route('admin.product.show', $product) }}">{{ str_limit($product->name,20) }}</a></td>
                                    <td>{{ $product->category->name }}</td>
                                    <td>
                                        <input target-id="{{ $product->id }}" type="text" class="form-control position position_{{ $product->id }}" value="{{ $product->position }}">
                                    </td>
                                    <td>
                                        @if($product->status == \App\ProductCategory::ACTIVED)
                                            <span class="label label-success">{{ trans('admin.status.active') }}</span>
                                        @else
                                            <span class="label label-danger">{{ trans('admin.status.deactive') }}</span>
                                        @endif
                                    </td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($product->created_at))->diffForHumans() }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                <span class="caret"></span> {{ trans('admin.button.function') }}
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.image.list', ['model' => 'products', 'id' => $product->id]) }}"><i class="fa fa-image"></i> {{ trans('admin.button.image') }}</a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('admin.product.edit', $product) }}"><i class="fa fa-edit"></i> {{ trans('admin.button.edit') }}</a>
                                                </li>
                                                <li>
                                                    <form class="form_delete_{{ $product->id }}" action="{{ route('admin.product.destroy', $product) }}" method="post">
                                                        {{ method_field('delete') }}
                                                        {{ csrf_field() }}
                                                    </form>
                                                    <a target-id="{{ $product->id }}" href="javascript:void(0)" class="delete"><i class="fa fa-trash"></i> {{ trans('admin.button.delete') }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        @if($product->hasTranslation(app()->getLocale()))
                                            <span class="label label-success"><i class="fa fa-check"></i> {{ trans('admin.language.available') }}</span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('admin.language.unvailable') }}</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $products->appends(['keyword' => $request->keyword, 'category' => $request->category])->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script_extra')
    <script src="{{ asset('js/delete_confirm.js') }}"></script>
    <script>
        $(document).ready(function () {
            deleteWithConfirm('{{ trans('admin.button.delete_confirm') }}');
            $('.position').on('focus', function () {
                $(this).blur(function () {
                    var position = $(this).val();
                    var id = $(this).attr('target-id');
                    $(this).attr('disabled', true);
                    $.ajax({
                        url: '{{ route('admin.product.product_position') }}',
                        type: 'post',
                        data: {
                            id: id,
                            position: position,
                            _token: '{{ csrf_token() }}'
                        }
                    }).done(function (result) {
                        $('.position_'+id).prop('disabled', false);
                    });
                });
            });
        });
    </script>
@endsection
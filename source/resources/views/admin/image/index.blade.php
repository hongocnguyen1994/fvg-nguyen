@extends('layouts.admin.master')
@section('title', trans('admin.title.booking.index'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('admin.title.booking.index') }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('admin.image.position', ['model' => $model, 'id' => $id]) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <div class="row">
                                @foreach($images as $image)
                                    <div class="col-md-3">
                                        <img src="{{ asset('assets/images/thumb_'.$image->image) }}">
                                        <br>
                                        <div class="form-group">
                                            <label>{{ trans('admin.label.image.position') }}</label>
                                            <input id="{{ $image->id }}" type="text" name="position[{{ $image->id }}]" value="{{ $image->position }}" class="form-control position">
                                        </div>
                                        <hr>
                                    </div>
                                @endforeach
                            </div>
                            @if(isset($image))
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Position</button>
                                </div>
                            @endif
                        </form>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="upload" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                            Upload Image
                        </button>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('admin.image.upload', ['model' => $model, 'id' => $id]) }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label>Image</label>
                                                @if($errors->has('image'))
                                                    <p class="danger">{{ $errors->first('image') }}</p>
                                                @endif
                                                <input type="file" name="image">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-success" type="submit"><i class="fa fa-upload"></i> Upload</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($errors->has('image'))
        <script>
            $(document).ready(function () {
                $("#upload").click();
            });
        </script>
    @endif
@endsection
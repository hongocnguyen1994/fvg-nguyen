@extends('layouts.admin.master')
@section('title', trans('admin.title.category.add'))
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-9">
				<!-- general form elements disabled -->
				<div class="box box-warning">
					<div class="box-header with-border">
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form action="{{ url('category') }}" method="POST" id="create-category">
							{{ csrf_field() }}
							<!-- text input -->
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.category.name') }} <span class="text-danger">*</span></label>
								<input type="text" class="form-control" placeholder="{{ trans('admin.place.category.name') }}" name="name" value="{{ old('name') }}">
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
							<!-- textarea -->
							<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.category.description') }}</label>
								<textarea class="form-control" rows="3" placeholder="{{ trans('admin.place.category.description') }}" name="description">{{ old('description') }}</textarea>
								@if ($errors->has('description'))
								<span class="help-block">
									<strong>{{ $errors->first('description') }}</strong>
								</span>
								@endif
							</div>
							<!-- text input -->
							<div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.category.parent') }}<span class="text-danger">*</span></label>
								<select class="form-control" name="parent_id" id="parent">
									<option value="">None</option>
									@foreach($categories as $category)
									@if($category->id == old('parent_id'))
									<option value="{{$category->id}}" selected>{{ $category->str.$category->name }}</option>
									@else
									<option value="{{$category->id}}">{{ $category->str.$category->name }}</option>
									@endif
									@endforeach
								</select>
								@if ($errors->has('parent_id'))
								<span class="help-block">
									<strong>{{ $errors->first('parent_id') }}</strong>
								</span>
								@endif
							</div>
							<!-- text input -->
							<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.category.position') }}<span class="text-danger">*</span></label>
								<select class="form-control" name="position" id="position">
									
								</select>
								@if ($errors->has('position'))
								<span class="help-block">
									<strong>{{ $errors->first('position') }}</strong>
								</span>
								@endif
							</div>
							<!-- select -->
							<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.category.status') }}</label><br>
								<input type="radio" name="status" id="active" value="1"> <label for="active">{{ trans('admin.status.active') }}</label>
								<input type="radio" name="status" id="deactive" value="0" checked> <label for="deactive">{{ trans('admin.status.deactive') }}</label>
								@if ($errors->has('status'))
								<span class="help-block">
									<strong>{{ $errors->first('status') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group">
                                <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> {{ trans('admin.button.cancel') }}</a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('admin.button.create') }}</button>
                            </div>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script>
	$('#create-category').validate({
		errorClass: 'text-danger',
		errorElement: 'span',
		highlight: function (element) {
			$(element).parents('div.form-group').addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parents('div.form-group').removeClass('has-error');
		},
		rules : {
			name : {
				required : true,
				maxlength : 255
			},
			parent_id: {
				number: true,
				min: 0,
			},
			position: {
				number: true,
				min: 1,
			},
			status: {
				required: true,
				number: true,
				range: [0, 1],
			},
		},
	});
</script>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		var parent_id_start = $("#parent").val();
		if (parent_id_start == "") {parent_id_start = null;}	
		$.get("{{ url('getCateByParent')}}/"+parent_id_start, function(data){
			var option = "<option value=''>At the beginning of category</option>";
			$.each( data, function( key, value ) {
				option = option + ("<option value='"+ (value.id+1) +"'>After "+ value.name +"</option>");
			});
			$("#position").html(option);
		});
		$("#parent").change(function(){
			var parent_id = $(this).val();
			if (parent_id == "") {parent_id = null;}		
			$.get("{{ url('getCateByParent')}}/"+parent_id, function(data){
				var option = "<option value=''>At the beginning of category</option>";
				$.each( data, function( key, value ) {
					option = option + ("<option value='"+ (value.id+1) +"'>After "+ value.name +"</option>");
				});
				$("#position").html(option);
			});
		});
	});
</script>
@endsection
@extends('layouts.admin.master')
@section('title', trans('admin.title.category.show').": ".$category->name)
@section('content')
<div class="content">
	<div class="col-xs-6">
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>{{ trans('admin.label.category.name') }}:</th>
					<td><b>{{ $category->name }}</b></td>
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.description') }}:</th>
					<td>{{ $category->description }}</td>
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.parent') }}:</th>
					@foreach($categories as $value)
					@if($value->id == $category->parent_id)
					<td><a href="{{ url('category/'.$value->id) }}">{{$value->name}}</a></td>
					@break
					@endif
					@endforeach
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.child') }}:</th>
					<td>
						@foreach($categories as $value)
						@if($value->parent_id == $category->id)
						<a href="{{ url('category/'.$value->id) }}">{{$value->name}}</a>, 
						@continue
						@endif
						@endforeach
					</td>
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.position') }}:</th>
					<td>{{ $category->position }}</td>
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.status') }}:</th>
					<td>
						@if($category->status == \App\Category::ACTIVED)
						<span class="label label-success">{{ trans('admin.status.active') }}</span>
						@else
						<span class="label label-danger">{{ trans('admin.status.deactive') }}</span>
						@endif
					</td>
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.created_at') }}:</th>
					<td>{{ $category->created_at }}</td>
				</tr>
				<tr>
					<th>{{ trans('admin.label.category.updated_at') }}:</th>
					<td>{{ $category->updated_at }}</td>
				</tr> 
			</table>
			<a href="{{ route('category.edit', $category) }}" class="btn btn-warning">{{ trans('admin.button.edit') }}</a>
			
			<form action="{{ route('category.destroy', $category) }}" method="post">
				{{ csrf_field() }}
				{{ method_field('delete') }}
				<button type="submit" class="btn btn-danger delete">{{ trans('admin.button.delete') }}</button>
			</form>
		</div>
	</div>
</div>
@endsection
@extends('layouts.admin.master')
@section('title', trans('admin.title.partner.add'))
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-9">
				<!-- general form elements disabled -->
				<div class="box box-warning">
					<div class="box-header with-border">
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form action="{{ url('admin/partner/create/submit') }}" method="POST" id="create-partner" enctype="multipart/form-data">
							{{ csrf_field() }}
							<!-- text input -->
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.partner.name') }} <span class="text-danger">*</span></label>
								<input type="text" class="form-control" placeholder="{{ trans('admin.place.partner.name') }}" name="name" value="{{ old('name') }}" required>
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
							<!-- textarea -->
							<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.partner.link') }}</label>
								<textarea class="form-control" rows="3" placeholder="{{ trans('admin.place.partner.link') }}" name="link" required>{{ old('link') }}</textarea>
								@if ($errors->has('link'))
								<span class="help-block">
									<strong>{{ $errors->first('link') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
								<label>{{ trans('admin.label.partner.image') }}</label>
								<input type="file" name="image" id="image" required>
								<span class="help-block">
									<strong>{{ $errors->first('image') }}</strong>
								</span>
							</div>
							
							<div class="form-group">
                                <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> {{ trans('admin.button.cancel') }}</a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('admin.button.create') }}</button>
                            </div>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@endsection
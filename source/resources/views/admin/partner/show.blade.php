@extends('layouts.admin.master')
@section('title', str_limit($partners->name, 20))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <img src='{{ asset('storage/'.$partners->image) }}' alt="" style="width: 50%;">
                    </div>
                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
        </div>
    </section>
@endsection
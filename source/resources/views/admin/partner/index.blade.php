@extends('layouts.admin.master')
@section('title', trans('admin.title.partner.index'))
@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>{{ trans('admin.table.name') }}</th>
								<th>{{ trans('admin.table.image') }}</th>
								<th>{{ trans('admin.table.link') }}</th>
								<th>{{ trans('admin.table.created_at') }}</th>
								<th>{{ trans('admin.table.updated_at') }}</th>
								<th>{{ trans('admin.table.function') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($partners as $partner)
								<tr>
									<th><a href="{{ route('admin.partner.show', $partner->id) }}">{{ $partner->name }}</a></th>
									<th>{{ $partner->image }}</th>
									<th>{{ $partner->link }}</th>
									<th>{{ $partner->created_at }}</th>
									<th>{{ $partner->updated_at }}</th>
									<th>
					                    <div class="btn-group">
					                		<a href="{{ url('/admin/partner/edit/'.$partner->id) }}" class="btn btn-primary">Edit</a>
					                      	<a href="{{ url('/admin/partner/delete/'.$partner->id) }}" class="btn btn-primary">Delete</a>
					                    </div>
									</th>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
@endsection
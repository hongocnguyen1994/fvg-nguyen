@extends('layouts.admin.master')
@section('title', trans('admin.title.document.category'))
@section('content')
<style>
#Addition{
	padding: 5px;
	position: absolute;
	z-index: 3;
	top:200px;
	right: 0;
	left: 0;
	margin: auto;
	background-color: rgba(255,255,255,6);
	box-shadow: 2px 2px 2px 2px rgba(0,0,0,0.5);

}

#Edidtion{
	max-width :400px;
	max-height :400px;
	padding: 5px;
	position: absolute;
	z-index: 2;
	top:200px;
	right: 0;
	left: 0;
	margin: auto;
	background-color: rgba(255,255,255,3);
	box-shadow: 2px 2px 2px 2px rgba(0,0,0,0.5);
}
#Trash{
	max-width :400px;
	max-height :400px;
	padding: 5px;
	position: absolute;
	z-index: 2;
	top:200px;
	right: 0;
	left: 0;
	margin: auto;
	background-color: rgba(255,255,255,3);
	box-shadow: 2px 2px 2px 2px rgba(0,0,0,0.5);
}
.show {display:block;}
</style>
<section class="content">
	<div class="col-xs-12">
		<div class="box" style="padding: 5px">
			<div class="box-header">
				<button class="btn btn-primary addition col-xs-1" onclick="showAddition()">{{ trans('admin.button.add') }}</button>
				<div class="col-xs-10"></div>
				<button class="btn btn-default btn-sm" onclick="showtrash()"><span class="glyphicon glyphicon-trash"></span></button>    
      </div>
      <div class="box-body">
          <table class="table table-bordered table-hover" id="tableCategory">
              <thead>
                <tr>
                	<th>{{ trans('admin.table.name') }}</th>
					<th>{{ trans('admin.table.description') }}</th>
					<th>{{ trans('admin.table.position') }}</th>
					<th>{{ trans('admin.table.status') }}</th>
					<th>{{ trans('admin.table.edit') }}</th>
					<th>{{ trans('admin.table.delete') }}</th>
                </tr>
              </thead>
              <tbody>
                  
              </tbody>
          </table>
      </div>
		</div>	
	</div>	
	<div id="Addition"  style="display: none" class="col-xs-2 col-lg-2 col-md-2 col-sm-3 addition">
		<div class=" addition" style="margin-bottom: 5px">
			<input type="text" class="form-control addition name" name="msg" placeholder="{{ trans('admin.place.document_category.name') }}" >
		</div>
		<button class="btn btn-primary addition" onclick="addCategory()">{{ trans('admin.button.add') }}</button>
	</div>

	<div id="Edidtion" style="display: none" class="addition">
		<div class=" addition" style="margin-bottom: 5px">
			<input type="text" class="form-control addition name" name="name" placeholder="Category's Name" >
			<br>
			<textarea type="text" class="form-control addition description" name="description" placeholder="Category Description" ></textarea>
			<br>
			<input type="text" class="form-control addition position" name="name" placeholder="position" >
			<br>
			<button class="btn btn-primary addition savebtn" value="" onclick="UpdateCategoryDocument(this.value)">Save</button>
		</div>
	</div>
	<div id="Trash" style="display: none" class="addition">
		<div class=" addition" style="margin-bottom: 5px">
				<table class="table table-bordered table-hover" id="tableCategoryTrash">
						<thead>
							<tr>
								<th>name</th>
								<th>descriptin</th>
								<th>position</th>
								<th>Restore</th>
							</tr>
						</thead>
						<tbody>
								
						</tbody>
				</table>
		</div>
	</div>
</section>
<script src="{{ asset('assets/admin/js/document.js') }}"></script>
@endsection
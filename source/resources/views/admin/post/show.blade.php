@extends('layouts.admin.master')
@section('title', str_limit($post->name, 20))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('admin.title.post.show') }}</h3>
                        @include('admin.post.search_form')
                    </div>
                    <div class="box-body">
                        <a href="{{ route('admin.post.edit', $post) }}" class="btn btn-warning">{{ trans('admin.button.edit') }}</a>
                        <h4>Name : {{ $post->name }}</h4>
                        <p>
                            <small>Writed At : {{ \Carbon\Carbon::createFromTimestamp(strtotime($post->created_at))->format('h:i:s d-m-Y') }}</small>
                            @if(!empty($post->published_at))
                               -  <small>Published At : {{ \Carbon\Carbon::createFromTimestamp(strtotime($post->published_at))->format('h:i:s d-m-Y') }}</small>
                            @endif
                        </p>
                        <p><strong>{{ $post->description }}</strong></p>
                        <p>
                            {!! $post->content !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
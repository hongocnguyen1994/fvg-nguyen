@extends('layouts.admin.master')
@section('title', trans('admin.title.post.index'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.post.search_form')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.name') }}</th>
                                <th>{{ trans('admin.table.category') }}</th>
                                <th>{{ trans('admin.table.position') }}</th>
                                <th>{{ trans('admin.table.status') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                <th>{{ trans('admin.table.published_at') }}</th>
                                <th>{{ trans('admin.table.publish') }}</th>
                                <th>{{ trans('admin.table.function') }}</th>
                                <th>{{ trans('admin.table.current_language') }}</th>
                            </tr>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td><a target="_blank" href="{{ route('admin.post.show', $post) }}">{{ str_limit($post->name,20) }}</a></td>
                                    <td>{{ $post->category->name }}</td>
                                    <td>
                                        <input target-id="{{ $post->id }}" type="text" class="form-control position position_{{ $post->id }}" value="{{ $post->position }}">
                                    </td>
                                    <td>
                                        @if($post->status == \App\Category::ACTIVED)
                                            <span class="label label-success">{{ trans('admin.status.active') }}</span>
                                        @else
                                            <span class="label label-danger">{{ trans('admin.status.deactive') }}</span>
                                        @endif
                                    </td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($post->created_at))->diffForHumans() }}</td>
                                    <td>
                                        @if(!empty($post->published_at))
                                            {{ \Carbon\Carbon::createFromTimestamp(strtotime($post->published_at))->diffForHumans() }}
                                        @else
                                            Not Published
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($post->published_at))
                                            <form method="post" action="{{ route('admin.post.publish', $post->id) }}">
                                                {{ method_field('patch') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-mail-reply"></i> {{ trans('admin.button.un_publish') }}</button>
                                            </form>
                                            @else
                                            <form method="post" action="{{ route('admin.post.publish', $post->id) }}">
                                                {{ method_field('put') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-mail-forward"></i> {{ trans('admin.button.publish') }}</button>
                                            </form>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                <span class="caret"></span> {{ trans('admin.button.function') }}
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.image.list', ['model' => 'posts', 'id' => $post->id]) }}"><i class="fa fa-image"></i> {{ trans('admin.button.image') }}</a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('admin.post.edit', $post) }}"><i class="fa fa-edit"></i> {{ trans('admin.button.edit') }}</a>
                                                </li>
                                                <li>
                                                    <form class="form_delete_{{ $post->id }}" action="{{ route('admin.post.destroy', $post) }}" method="post">
                                                        {{ method_field('delete') }}
                                                        {{ csrf_field() }}
                                                    </form>
                                                    <a target-id="{{ $post->id }}" href="javascript:void(0)" class="delete"><i class="fa fa-trash"></i> {{ trans('admin.button.delete') }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        @if($post->hasTranslation(app()->getLocale()))
                                            <span class="label label-success"><i class="fa fa-check"></i> {{ trans('admin.lang.available') }}</span>
                                            @else
                                            <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('admin.lang.unvailable') }}</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
        
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $posts->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script_extra')
    <script src="{{ asset('js/delete_confirm.js') }}"></script>
    <script>
        $(document).ready(function () {
            deleteWithConfirm('{{ trans('admin.button.delete_confirm') }}');
            $('.position').on('focus', function () {
                $(this).blur(function () {
                    var position = $(this).val();
                    var id = $(this).attr('target-id');
                    $(this).attr('disabled', true);
                    $.ajax({
                        url: '{{ route('admin.post.post_position') }}',
                        type: 'post',
                        data: {
                            id: id,
                            position: position,
                            _token: '{{ csrf_token() }}'
                        }
                    }).done(function (result) {
                        $('.position_'+id).prop('disabled', false);
                    });
                });
            });
        });
    </script>
@endsection

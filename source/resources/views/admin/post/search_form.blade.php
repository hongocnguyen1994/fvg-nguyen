<div class="box-tools">
    <form class="form-inline" action="{{ route('admin.post.search') }}" method="get">
        <div class="form-group">
            <input @if(isset($request)) value="{{ $request->keyword }}" @endif class="form-control" placeholder="{{ trans('admin.place.post.search') }}" type="text" name="keyword">
        </div>
        <div class="form-group">
            <select class="form-control" name="category">
                <option @if(!isset($request)) selected @endif value="0">Category</option>
                @foreach(\App\Category::all() as $category)
                    @if(isset($request) && $request->category == $category->id)
                        <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                    @else
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">{{ trans('admin.button.search') }}</button>
        </div>
    </form>
</div>
@extends('layouts.admin.master')
@section('title', trans('admin.title.post.restore'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        {{--@include('admin.post.search_form')--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.name') }}</th>
                                <th>{{ trans('admin.table.category') }}</th>
                                <th>{{ trans('admin.table.status') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                <th>{{ trans('admin.table.published_at') }}</th>
                                <th>{{ trans('admin.table.publish') }}</th>
                                <th>{{ trans('admin.table.edit') }}</th>
                                <th>{{ trans('admin.table.restore') }}</th>
                            </tr>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td><a target="_blank" href="{{ route('admin.post.show', $post) }}">{{ str_limit($post->name,20) }}</a></td>
                                    <td>{{ $post->category->name }}</td>
                                    <td>
                                        @if($post->status == \App\Category::ACTIVED)
                                            <span class="label label-success"><i class="fa fa-check"></i> {{ trans('admin.status.active') }}</span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('admin.status.deactive') }}</span>
                                        @endif
                                    </td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($post->deleted_at))->diffForHumans() }}</td>
                                    <td>
                                        @if(!empty($post->published_at))
                                            {{ \Carbon\Carbon::createFromTimestamp(strtotime($post->published_at))->diffForHumans() }}
                                        @else
                                            Not Published
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($post->published_at))
                                            <form method="post" action="{{ route('admin.post.publish', $post->id) }}">
                                                {{ method_field('patch') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-mail-reply"></i> {{ trans('admin.button.un_publish') }}</button>
                                            </form>
                                        @else
                                            <form method="post" action="{{ route('admin.post.publish', $post->id) }}">
                                                {{ method_field('put') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-mail-forward"></i> {{ trans('admin.button.publish') }}</button>
                                            </form>
                                        @endif
                                    </td>
                                    <td><a class="btn btn-warning btn-xs" href="{{ route('admin.post.edit', $post) }}">{{ trans('admin.button.edit') }}</a></td>
                                    <td>
                                        <form action="{{ route('admin.post.restore', $post) }}" method="post">
                                            {{ method_field('put') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-undo"></i> {{ trans('admin.button.restore') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $posts->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
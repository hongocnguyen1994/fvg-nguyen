@extends('layouts.admin.master')
@section('title', trans('admin.title.post.edit'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-9">
                <form id="post" action="{{ route('admin.post.update', $post) }}" method="post" enctype="multipart/form-data">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <div class="form-group @if($errors->has('name')) has-error @endif">
                                <label for="name">{{ trans('admin.label.post.name') }}<span class="text-danger">*</span></label>
                                <input value="{{ $post->name }}" type="text" name="name" class="form-control required" placeholder="{{ trans('admin.place.post.name') }}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('description')) has-error @endif">
                                <label for="name">{{ trans('admin.label.post.description') }}<span class="text-danger">*</span></label>
                                <textarea class="form-control required" name="description" rows="5" placeholder="{{ trans('admin.place.post.description') }}">{{ $post->description }}</textarea>
                                @if($errors->has('description'))
                                    <p class="text-danger">{{ $errors->first('description') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('category')) has-error @endif">
                                <label for="category">{{ trans('admin.label.post.category') }}<span class="text-danger">*</span></label>
                                <select class="form-control" name="category">
                                    @php
                                        \App\Category::softCategory(\App\Category::all(), null, '');
                                        $categories = \App\Category::$lists;
                                    @endphp
                                    @foreach($categories as $category)
                                        <option @if($category->id == $post->category_id) selected @endif value="{{ $category->id }}">{{ $category->str.' '.$category->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category'))
                                    <p class="text-danger">{{ $errors->first('category') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <input value="{{ $post->image }}" name="image" id="image" type="hidden" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="description">{{ trans('admin.label.post.content') }}<span class="text-danger">*</span></label>
                                <textarea class="required" id="content" name="content">
                                    {{ $post->content }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> {{ trans('admin.button.cancel') }}</a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('admin.button.edit') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="image_featured">{{ trans('admin.label.post.image') }}</label>
                            <button class="btn btn-success btn-block" id="image_featured">{{ trans('admin.button.post_image') }}</button>
                            <br>
                            <img src="{{ $post->image }}" style="width:100%; height: 100%; @if($post->image == 'default-featured-image.png') display: none @endif" id="image_demo">
                            <p class="text-center">
                                <a @if($post->image == 'default-featured-image.png') style='display: none' @endif href="javascript:void(0)" id="reset" class="btn btn-link">{{ trans('admin.button.cancel') }}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#post").validate({
            errorClass: 'text-danger',
            errorElement: 'p',
            highlight: function (element) {
                $(element).parents('div.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).parents('div.form-group').removeClass('has-error');
            },
            ignore: [],
            rules: {
                cktext: {
                    required: function() {
                        CKEDITOR.instances.cktext.updateElement();
                    },
                    minlength: 10
                }
            }
        });
        $(document).ready(function () {
            function reset() {
                $("#image_demo").attr('src', '').hide();
                $("#image").val('');
            }

            $("#reset").on('click', function () {
                reset();
                $(this).hide();
            });

            var button1 = document.getElementById( 'image_featured' );
            button1.onclick = function() {
                selectFileWithCKFinder( 'image' );
            };
            function selectFileWithCKFinder( elementId ) {
                CKFinder.modal( {
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function( finder ) {
                        finder.on( 'files:choose', function( evt ) {
                            var file = evt.data.files.first();
                            var output = document.getElementById( elementId );
                            output.value = file.getUrl();
                            document.getElementById("image_demo").src = file.getUrl();
                            $("#image_demo").show();
                            $("#reset").show();
                        } );

                        finder.on( 'file:choose:resizedImage', function( evt ) {
                            var output = document.getElementById( elementId );
                            output.value = evt.data.resizedUrl;
                        } );
                    }
                } );
            }
        });
    </script>
    <script>
        CKEDITOR.replace( 'content', {
            filebrowserBrowseUrl: '{{ asset('assets/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('assets/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserUploadUrl: '{{ asset('assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}', // Define the toolbar: http://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_toolbar,
            toolbar: [
                { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                { name: 'styles', items: [ 'Styles', 'Format' ] },
                { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
                { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                { name: 'insert', items: [ 'Image', 'EmbedSemantic', 'Table' ] },
                { name: 'tools', items: [ 'Maximize' ] },
                { name: 'editing', items: [ 'Scayt' ] },
                { name: 'source', items: ['Source'  ]}
            ],
            customConfig: '',
            allowedContent: true,
            height: 500
        } );
    </script>
@endsection
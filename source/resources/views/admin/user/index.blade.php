@extends('layouts.admin.master')
@section('title', trans('admin.title.user.index'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.user.search_form')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.full_name') }}</th>
                                <th>{{ trans('admin.table.username') }}</th>
                                <th>{{ trans('admin.table.email') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                <th>{{ trans('admin.table.role') }}</th>
                                <th>{{ trans('admin.table.department') }}</th>
                                <th>{{ trans('admin.table.function') }}</th>
                            </tr>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                    <td><a href="{{ route('admin.user.show', $user) }}">{{ $user->username }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($user->created_at))->format('d-m-Y') }}</td>
                                    <td><a href="{{ route('admin.role.show', $user->role) }}">{{ $user->role->name }}</a></td>
                                    <td>
                                        
                                        @if($user->department_id)
                                           {{$user->department->name}}
                                        @endif
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                <span class="caret"></span> {{ trans('admin.button.function') }}
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.user.edit', $user) }}">{{ trans('admin.button.edit') }}</a>
                                                </li>
                                                <li>
                                                    <form class="form_delete_{{ $user->id }}" action="{{ route('admin.user.destroy', $user) }}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                    </form>
                                                    <a type="button" target-id="{{ $user->id }}" href="javascript:void(0)" class="delete">{{ trans('admin.button.delete') }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $users->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script_extra')
    <script src="{{ asset('js/delete_confirm.js') }}"></script>
    <script>
        $(document).ready(function () {
            deleteWithConfirm('{{ trans('admin.button.delete_confirm') }}');
        });
    </script>
@endsection
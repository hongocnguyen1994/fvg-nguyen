@extends('layouts.admin.master')
@section('title', trans('admin.title.user.add'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form id="user" action="{{ route('admin.user.store') }}" method="post" enctype="multipart/form-data">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            {{ csrf_field() }}
                            <div class="form-group @if($errors->has('first_name')) has-error @endif">
                                <label>{{ trans('admin.label.user.first_name') }} <span class="text-danger">*</span></label>
                                <input type="text" name="first_name" class="form-control" placeholder="{{ trans('admin.place.user.first_name') }}">
                                @if($errors->has('first_name'))
                                    <p class="text-danger">{{ $errors->first('first_name') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('last_name')) has-error @endif">
                                <label>{{ trans('admin.label.user.last_name') }} <span class="text-danger">*</span></label>
                                <input type="text" name="last_name" class="form-control" placeholder="{{ trans('admin.place.user.last_name') }}">
                                @if($errors->has('last_name'))
                                    <p class="text-danger">{{ $errors->first('last_name') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('username')) has-error @endif">
                                <label>{{ trans('admin.label.user.username') }} <span class="text-danger">*</span></label>
                                <input type="text" name="username" class="form-control" placeholder="{{ trans('admin.place.user.username') }}">
                                @if($errors->has('username'))
                                    <p class="text-danger">{{ $errors->first('username') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('email')) has-error @endif">
                                <label>{{ trans('admin.label.user.email') }} <span class="text-danger">*</span></label>
                                <input type="text" name="email" class="form-control" placeholder="{{ trans('admin.place.user.email') }}">
                                @if($errors->has('email'))
                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="department">{{ trans('admin.label.user.department') }} </label>
                                <select class="form-control" name="role_id">
                                    @foreach(\App\Department::all() as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group @if($errors->has('role_id')) has-error @endif">
                                <label>{{ trans('admin.label.user.role') }} <span class="text-danger">*</span></label>
                                <select class="form-control" name="role_id">
                                    @foreach(\App\Role::where('id', '<>', 1)->get() as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('role_id'))
                                    <p class="text-danger">{{ $errors->first('role_id') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">{{ trans('admin.button.create') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $("#user").validate({
            errorClass: 'text-danger',
            errorElement: 'p',
            highlight: function (element) {
                $(element).parents('div.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).parents('div.form-group').removeClass('has-error');
            },
            ignore: [],
            rules: {
                first_name:{
                    required: true,
                    maxlength: 32
                },
                last_name: {
                    required: true,
                    maxlength: 32
                },
                username: {
                    required: true,
                    maxlength: 32
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 128
                },
                role_id :{
                    required: true
                }
            }
        });
    </script>
@endsection
@extends('layouts.admin.master')
@section('title', trans('admin.title.user.restore'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('admin.title.user.restore') }}</h3>
                        @include('admin.user.search_form')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.username') }}</th>
                                <th>{{ trans('admin.table.full_name') }}</th>
                                <th>{{ trans('admin.table.email') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                <th>{{ trans('admin.table.role') }}</th>
                                <th>{{ trans('admin.table.restore') }}</th>
                            </tr>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td><a href="{{ route('admin.user.show', $user) }}">{{ $user->username }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($user->created_at))->format('d-m-Y') }}</td>
                                    <td>{{ $user->role->name }}</td>
                                    <td>
                                        <form action="{{ route('admin.user.restore', $user->id) }}" method="post">
                                            {{ method_field('put') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-primary">{{ trans('admin.button.restore') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $users->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
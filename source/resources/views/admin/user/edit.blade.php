@extends('layouts.admin.master')
@section('title', trans('admin.title.user.edit').": ".$user->email)
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.user.search_form')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form role="form" action="{{ route('admin.user.update', $user) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <div class="form-group">
                                <label for="first_name">{{ trans('admin.label.user.first_name') }}</label>
                                <input value="{{ $user->first_name }}" type="text" name="first_name" class="form-control" placeholder="{{ trans('admin.place.user.firstname') }}">
                            </div>
                            <div class="form-group">
                                <label  for="last_name">{{ trans('admin.label.user.last_name') }}</label>
                                <input value="{{ $user->last_name }}" type="text" name="last_name" class="form-control" placeholder="{{ trans('admin.place.user.lastname') }}">
                            </div>
                            <div class="form-group">
                                <label  for="password">{{ trans('admin.label.user.password') }}</label>
                                <input  type="text" name="password" class="form-control" placeholder="{{ trans('admin.place.user.password') }}">
                            </div>
                            <div class="form-group">
                                <label for="role">{{ trans('admin.label.user.role') }} </label>
                                <select class="form-control" name="role_id">
                                    @foreach(\App\Role::where('id', '<>', \App\Role::SYSTEM_ADMIN_ROLE)->get() as $value)
                                        <option @if($value->id == $user->role->id) selected @endif value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="department">{{ trans('admin.label.user.department') }} </label>
                                <select class="form-control" name="department_id">
                                    @foreach(\App\Department::all() as $value)
                                        <option @if($value->id == $user->department_id) selected @endif value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">{{ trans('admin.button.update') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
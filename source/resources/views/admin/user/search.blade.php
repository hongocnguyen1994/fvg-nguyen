@extends('layouts.admin.master')
@section('title', trans('admin.title.user.search'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.user.search_form')
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.full_name') }}</th>
                                <th>{{ trans('admin.table.username') }}</th>
                                <th>{{ trans('admin.table.email') }}</th>
                                <th>{{ trans('admin.table.created_at') }}</th>
                                <th>{{ trans('admin.table.role') }}</th>
                                <th>{{ trans('admin.table.status') }}</th>
                                <th>{{ trans('admin.table.edit') }}</th>
                                <th>{{ trans('admin.table.delete') }}</th>
                            </tr>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                    <td><a href="{{ route('admin.user.show', $user) }}">{{ $user->username }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($user->created_at))->format('d-m-Y') }}</td>
                                    <td><a href="{{ route('admin.role.show', $user->role) }}">{{ $user->role->name }}</a></td>
                                    <td>
                                        @if($user->status == \App\User::ACTIVED)
                                            <span class="label label-success">{{ trans('admin.status.active') }}</span>
                                        @else
                                            <span class="label label-danger">{{ trans('admin.status.deactive') }}</span>
                                        @endif
                                    </td>
                                    <td><a href="{{ route('admin.user.edit', $user) }}" class="btn btn-warning">{{ trans('admin.button.edit') }}</a></td>
                                    <td>
                                        <form action="{{ route('admin.user.destroy', $user) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="btn btn-danger delete">{{ trans('admin.button.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{ $users->appends(['keyword' => $request->keyword])->render() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
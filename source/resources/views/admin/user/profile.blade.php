@extends('layouts.admin.master')
@section('title', trans('admin.title.user.profile'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img id="img_image" class="profile-user-img img-responsive img-circle" src="{{ asset('assets/images/'.$user->avatar) }}" alt="User profile picture">
                        <div class="text-center">
                            <a id="image" href="#">Change Image</a>
                        </div>
                        <h3 class="profile-username text-center">{{ $user->username }}</h3>
                        <p class="text-muted text-center">{{ $user->role->name }}</p>
                        <form id="profile" action="" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <input type="file" name="image_file" id="input_image" style="display: none">
                            <div class="form-group @if($errors->has('first_name')) has-error @endif">
                                <label>{{ trans('admin.label.user.first_name') }}</label>
                                <input type="text" name="first_name" class="form-control" placeholder="{{ trans('admin.place.user.first_name') }}" value="{{ $user->first_name }}">
                                @if($errors->has('first_name'))
                                    <p class="text-danger">{{ $errors->first('first_name') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('last_name')) has-error @endif">
                                <label>{{ trans('admin.label.user.last_name') }}</label>
                                <input type="text" name="last_name" class="form-control" placeholder="{{ trans('admin.place.user.last_name') }}" value="{{ $user->last_name }}">
                                @if($errors->has('last_name'))
                                    <p class="text-danger">{{ $errors->first('last_name') }}</p>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary btn-block"><b>{{ trans('admin.button.update') }}</b></button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-xs-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <form id="password" action="" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <div class="form-group @if($errors->has('password')) has-error @endif">
                                <label>{{ trans('admin.label.user.password') }}</label>
                                <input class="form-control" type="password" id="pwd" name="password" placeholder="{{ trans('admin.place.user.password') }}">
                                @if($errors->has('password'))
                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('re_password')) has-error @endif">
                                <label>{{ trans('admin.label.user.retype_password') }}</label>
                                <input class="form-control" type="password" id="re_pwd" name="re_password" placeholder="{{ trans('admin.place.user.retype_password') }}">
                                @if($errors->has('re_password'))
                                    <p class="text-danger">{{ $errors->first('re_password') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">{{ trans('admin.button.update') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#profile").validate({
            errorClass: 'text-danger',
            errorElement: 'p',
            highlight: function (element) {
                $(element).parents('div.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).parents('div.form-group').removeClass('has-error');
            },
            rules: {
                first_name: {
                    maxlength: 30
                },
                last_name: {
                    maxlength: 30
                }
            }
        });
        $("#password").validate({
            errorClass: 'text-danger',
            errorElement: 'p',
            highlight: function (element) {
                $(element).parents('div.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).parents('div.form-group').removeClass('has-error');
            },
            rules: {
                password:{
                    maxlength: 30,
                    minlength: 6
                },
                re_password: {
                    maxlength: 30,
                    minlength: 6,
                    equalTo: '#pwd'
                }
            }
        });
        $(document).ready(function () {
            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#'+id+'').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('#image').on('click', function () {
                $('#input_image').trigger('click');
                $('#input_image').change(function () {
                    readURL(this, 'img_image')
                });
            });
        });
    </script>
@endsection
@extends('layouts.admin.master')
@section('title', trans('admin.title.user.show').": ".$user->email)
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p>{{ trans('admin.label.user.fullname') }} : {{ $user->first_name.' '.$user->last_name }}</p>
                        <p>{{ trans('admin.label.user.username') }} : {{ $user->username }}</p>
                        <p>{{ trans('admin.label.user.email') }} : {{ $user->email }}</p>
                        <p>{{ trans('admin.label.user.phone') }} : {{ $user->phone }}</p>
                        <p>{{ trans('admin.label.user.address') }} : {{ $user->address }}</p>
                        <p>{{ trans('admin.label.user.role') }} : {{ $user->role->name }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
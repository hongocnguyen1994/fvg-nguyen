@extends('layouts.admin.master')
@section('title', str_limit($contact->title, 20))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <ul class="timeline">
                            <li>
                                <i class="fa fa-envelope bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::createFromTimestamp(strtotime($contact->created_at))->format('h:i:s d-m-Y') }}</span>
                                    <h3 class="timeline-header"><a href="#">{{ $contact->name }}</a> {{ trans('admin.contact.sent') }}</h3>
                                    <div class="timeline-body">
                                        {{ $contact->content }}
                                    </div>
                                    @if($contact->status == \App\Contact::NOT_REPLY)
                                        <div class="timeline-footer">
                                            <hr>
                                            @include('admin.contact.reply_box')
                                        </div>
                                    @endif
                                </div>
                            </li>
                            @if($contact->status == \App\Contact::REPLIED)
                                <li>
                                    <i class="fa fa-check bg-blue"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::createFromTimestamp(strtotime($contact->reply->updated_at))->format('h:i:s d-m-Y') }}</span>
                                        <div class="timeline-body">
                                            {!! $contact->reply->content !!}
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
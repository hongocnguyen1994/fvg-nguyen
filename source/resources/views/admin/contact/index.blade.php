@extends('layouts.admin.master')
@section('title', trans('admin.title.contact.index'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>{{ trans('admin.table.id') }}</th>
                                    <th>{{ trans('admin.table.title') }}</th>
                                    <th>{{ trans('admin.table.name') }}</th>
                                    <th>{{ trans('admin.table.email') }}</th>
                                    <th>{{ trans('admin.table.phone') }}</th>
                                    <th>{{ trans('admin.table.created_at') }}</th>
                                    <th>{{ trans('admin.table.reply') }}</th>
                                    <th>{{ trans('admin.table.delete') }}</th>
                                </tr>
                            </tbody>
                            @foreach($contacts as $contact)
                                <tr>
                                    <td>{{ $contact->id }}</td>
                                    <td><a href="{{ route('admin.contact.show', ['id'=>$contact->id]) }}">{{ str_limit($contact->title, 30) }}</a></td>
                                    <td>{{ str_limit($contact->name, 20) }}</td>
                                    <td>{{ str_limit($contact->email, 20) }}</td>
                                    <td>{{ str_limit($contact->phone, 20) }}</td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($contact->created_at))->format('h:i:s d-m-Y') }}</td>
                                    <td>
                                        @if($contact->status == \App\Contact::NOT_REPLY)
                                            <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('admin.status.not_reply') }}</span>
                                        @else
                                            <span class="label label-success"><i class="fa fa-check"></i> {{ trans('admin.status.replied') }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <form class="form_delete_{{ $contact->id }}" action="{{ route('admin.contact.delete', ['id'=>$contact->id]) }}" method="post">
                                            {{ csrf_field() }}
                                            <button target-id="{{ $contact->id }}" type="button" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i> {{ trans('admin.button.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script_extra')
    <script src="{{ asset('js/delete_confirm.js') }}"></script>
    <script>
        $(document).ready(function () {
            deleteWithConfirm('{{ trans('admin.button.delete_confirm') }}');
        });
    </script>
@endsection
<form action="{{ route('admin.contact.reply', ['id'=>$contact->id]) }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label>{{ trans('admin.label.reply.content') }}</label>
        <textarea name="content" id="reply"></textarea>
    </div>
    <div class="form-group">
        <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> {{ trans('admin.button.cancel') }}</a>
        <button type="submit" class="btn btn-success"><i class="fa fa-mail-forward"></i> {{ trans('admin.button.reply') }}</button>
    </div>
</form>
<script>
    CKEDITOR.replace('reply', {
        filebrowserBrowseUrl: '{{ asset('assets/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('assets/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}', // Defi
    });
</script>
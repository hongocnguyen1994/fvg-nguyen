@extends('layouts.admin.master')
@section('title', trans('admin.title.role.show').": ".$role->name)
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p>{{ trans('admin.label.role.name') }} : {{ $role->name }}</p>
                        <p>{{ trans('admin.label.role.description') }} : {{ $role->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
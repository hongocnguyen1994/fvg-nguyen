@extends('layouts.admin.master')
@section('title', trans('admin.title.role.index'))
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>{{ trans('admin.table.id') }}</th>
                                <th>{{ trans('admin.table.name') }}</th>
                                <th>{{ trans('admin.table.quantity') }}</th>
                                <th>{{ trans('admin.table.edit') }}</th>
                                <th>{{ trans('admin.table.delete') }}</th>
                            </tr>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td><a href="{{ route('admin.role.show', $role) }}">{{ $role->name }}</a></td>
                                    <td>{{ $role->users->count() }}</td>
                                    <td><a href="{{ route('admin.role.edit', $role) }}" class="btn btn-warning">{{ trans('admin.button.edit') }}</a></td>
                                    <td>
                                        <form action="{{ route('admin.role.destroy', $role) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger">{{ trans('admin.button.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('layouts.admin.master')
@section('title', trans('admin.title.role.edit').": ".$role->name)
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form id="role" action="{{ route('admin.role.update', $role) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <div class="form-group @if($errors->has('name')) has-error @endif">
                                <label for="name">{{ trans('admin.label.role.name') }}</label>
                                <input value="{{ $role->name }}" type="text" name="name" class="form-control required" placeholder="{{ trans('admin.place.role.name') }}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('description')) has-error @endif">
                                <label for="description">{{ trans('admin.label.role.description') }}</label>
                                <textarea rows="5" class="form-control required" placeholder="{{ trans('admin.place.role.description') }}" name="description">{{ $role->description }}</textarea>
                                @if($errors->has('description'))
                                    <p class="text-danger">{{ $errors->first('description') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">{{ trans('admin.button.update') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#role").validate({
            errorClass: 'text-danger',
            errorElement: 'p',
            highlight: function (element) {
                $(element).parents('div.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).parents('div.form-group').removeClass('has-error');
            }
        });
    </script>
@endsection
<?php 
// Contact
	Route::group(['middleware' => 'admin'], function () {
		Route::get('admin/contact', 'Admin\ContactController@index')->name('admin.contact.index');
		Route::get('admin/show/{id}', 'Admin\ContactController@show')->name('admin.contact.show');
		Route::post('admin/contact/reply/{id}', 'Admin\ContactController@postReply')->name('admin.contact.reply');
		Route::post('admin/contact/delete/{id}', 'Admin\ContactController@destroy')->name('admin.contact.delete');
	});

//admin.partner
	Route::group(['middleware' => 'admin'], function () {
		Route::get('admin/partner/list', 'Admin\PartnerController@index')->name('admin.partner.index');
		Route::get('admin/partner/create', 'Admin\PartnerController@create')->name('admin.partner.create');
		Route::post('admin/partner/create/submit', 'Admin\PartnerController@submit');
		Route::get('admin/partner/delete/{id}', 'Admin\PartnerController@destroy');
		Route::get('admin/partner/edit/{id}', 'Admin\PartnerController@edit');
		Route::post('admin/partner/edit/submit/{id}', 'Admin\PartnerController@editSubmit');

		Route::get('admin/partner/list/show/{id}', 'Admin\PartnerController@show')->name('admin.partner.show');
	});
<?php
Route::resources([
    'category' => 'Admin\CategoryController',
    'product-category' => 'Admin\ProductCategoryController',
]);
//category
Route::get('category/deleted/list', 'Admin\CategoryController@getDeletedCategories')->name('category.get_deleted');
Route::put('category/deleted/{id}', 'Admin\CategoryController@restore')->name('category.restore');
Route::get('search/category', 'Admin\CategoryController@search')->name('category.search');
Route::get('getCateByParent/{parentId}', 'Admin\CategoryController@getCategoriesByParent')->name('getCateByParent');
//product category
Route::get('product-category/deleted/list', 'Admin\ProductCategoryController@getDeletedCategories')->name('product-category.get_deleted');
Route::put('product-category/deleted/{id}', 'Admin\ProductCategoryController@restore')->name('product-category.restore');
Route::get('search/product-category', 'Admin\ProductCategoryController@search')->name('product-category.search');
Route::get('getProductCateByParent/{parentId}', 'Admin\ProductCategoryController@getCategoriesByParent')->name('getProductCateByParent');
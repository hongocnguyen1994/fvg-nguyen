<?php
Route::group(['middleware' => 'admin'], function () {
  // Document admin.document_category'
  Route::get('/admin/document_category','Admin\DocumentCategoryController@index')->name('admin.document_category');
  Route::post('/admin/document_category/status','Admin\DocumentCategoryController@changestatus');
  Route::post('/admin/document_category/delete','Admin\DocumentCategoryController@delete');
  Route::post('/admin/document_category/update','Admin\DocumentCategoryController@update');
  Route::get('/admin/document_category/trash','Admin\DocumentCategoryController@trash');
  Route::post('/admin/document_category/restore','Admin\DocumentCategoryController@restore');
  Route::post('/admin/document_category/add','Admin\DocumentCategoryController@addCategory');
  Route::get('/admin/document','Admin\DocumentController@index')->name('admin.document');
  Route::post('/admin/document/add','Admin\DocumentController@add');
  // admin.document


  //admin.partner
  
  //admin.department
  Route::get('/admin/department/list','Admin\DepartmentController@list');
  Route::get('/admin/department','Admin\DepartmentController@index')->name('admin.department');
  Route::post('/admin/department/add','Admin\DepartmentController@add');
  Route::post('/admin/department/update','Admin\DepartmentController@update');
});
Route::group(['middleware'=>'auth'], function() {

  Route::get('/Documentcategory/list','DocumentCategoryController@list');
  Route::get('/Documentcategory/listactive','DocumentCategoryController@listactive');
  Route::get('/getDocumentactive/{category_id}/{last_document}','DocumentController@getDocumentactive');
  // /Department/listactive
  Route::get('/Department/listactive','DepartmentController@listactive');
  Route::get('/getStaffActive/{department_id}/{last_staff}','Client\UserController@getStaffActive');

});
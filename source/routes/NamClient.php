<?php 
Route::get('/hiring-list','Client\HiringController@index');
Route::post('/clientSend','Client\contactController@clientSend');


Route::get('/client/about','Client\aboutController@listpost');
Route::post('/hiring-list','Client\HiringController@loadDataAjax');
Route::get('/infoCompany','InforController@getinfo');
Route::get('/loadProductCategory','ProductCategoryController@getCategory');
Route::get('/getDocument','DocumentController@get');
Route::get('/loadnewDocument','DocumentController@getNewDocument');
//Load Staffbar
Route::get('/loadstaffbar','Client\UserController@loadStaffBar');
//Product list 
Route::get('/productList/{category_id}', 'Client\ProductListController@view')->name('productList');
Route::get('/productDetail/{id}','Client\productDetailController@index')->name('productDetail');


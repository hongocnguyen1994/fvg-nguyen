<?php
//client
Route::get('switch/{locale}', 'LanguageController@getSwitchLang')->name('get_switch_lang');
// Route::get('/',function(){
// 	return view('client.homepage');
// })->name('homepage');
require_once 'NguyenClient.php';
require_once 'NamClient.php';

Route::group(['middleware'=>'auth'], function() {
	Route::get('/internal','Client\InternalController@index');
	Route::get('/form-list','Client\FormController@index');
	Route::get('/staff-list','Client\StaffController@index');


	//Internal's Routes
	//Message's Routes
	Route::get('/internal/loadMessage/','Client\InternalController@loadMessage');
	Route::get('/internal/loadMessage/{LastMessage}','Client\InternalController@loadMessage');
	Route::post('/internal/addMessage','Client\InternalController@addMessage');
	Route::post('/internal/deleteMessage/','Client\InternalController@deleteMessage');
	Route::post('/internal/editMessage/','Client\InternalController@editMessage');
	//Comment's Routes
	Route::get('/internal/loadCommentlv1/{message_id}','Client\InternalController@loadCommentlv1');
	Route::get('/internal/loadCommentlv2/{comment_id}','Client\InternalController@loadCommentlv2');
	Route::post('/internal/Comment','Client\InternalController@Comment');
	Route::get('/internal/searchDocument/{category_id}/{character}','Client\InternalController@searchDocument');
	Route::get('/internal/searchUser/{department_id}/{character}','Client\InternalController@searchUser');
	
	Route::get('/getNotification','Client\ClientNotificationController@run');
	
});

<?php 
	Route::get('/about', 'Client\aboutController@view')->name('about');
	Route::get('/news-list', 'Client\newsListController@view')->name('news-list');
	Route::post('/news-list','Client\newsListController@loadDataAjax');
	Route::get('/contact', 'Client\contactController@view')->name('contact');

	Route::get('/newsDetail/{id}', 'Client\newsDetailController@view')->name('newsDetail');
	Route::get('/hiringDetail/{id}', 'Client\hiringDetailController@view')->name('hiringDetail');


	Route::get('/productDetail', 'Client\productDetailController@view')->name('productDetail');
	Route::get('/', 'HomeController@index')->name('homePage');
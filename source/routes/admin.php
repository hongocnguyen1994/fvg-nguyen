<?php
Route::get('/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return redirect()->route('login');
})->name('logout');

Route::get('switch/{locale}', 'LanguageController@getSwitchLang')->name('get_switch_lang');

Route::group(['middleware' => 'admin'], function () {

    Route::get('/admin','Admin\DashBoardController@index')->name('admin.dashboard');
    /*User*/
    
    Route::resource('/admin/user', 'Admin\UserController', ['as' => 'admin']);
    Route::get('/admin/user/profile/show', 'Admin\UserController@getProfile')->name('admin.user.get_profile');
    Route::put('/admin/user/profile/show', 'Admin\UserController@updateProfile');
    Route::patch('/admin/user/profile/show', 'Admin\UserController@updatePassword');
    Route::get('/admin/user/deleted/list', 'Admin\UserController@getDeletedUser')->name('admin.user.get_deleted');
    Route::put('/admin/user/deleted/{id}', 'Admin\UserController@restore')->name('admin.user.restore');
    Route::get('/admin/search/user', 'Admin\UserController@search')->name('admin.user.search');
    Route::get('/admin/user/add/show', 'Admin\UserController@getAddUser')->name('admin.user.add');


    /*Role*/
    Route::resource('role', 'Admin\RoleController', ['as' => 'admin']);

    /*Post*/
    Route::resource('/admin/post', 'Admin\PostController', ['as' => 'admin']);
    Route::put('/admin/publish/post/{id}', 'Admin\PostController@postPublishPost')->name('admin.post.publish');
    Route::patch('/admin/publish/post/{id}', 'Admin\PostController@postUnPublishPost')->name('admin.post.unpublish');
    Route::get('/admin/search/post', 'Admin\PostController@search')->name('admin.post.search');
    Route::get('/admin/post/deleted/list', 'Admin\PostController@getDeletedPost')->name('admin.post.get_deleted');
    Route::put('/admin/post/deleted/{id}', 'Admin\PostController@restore')->name('admin.post.restore');
    Route::post('/admin/post/position', 'Admin\PostController@postPosition')->name('admin.post.post_position');
    /*Image*/
    Route::get('/admin/image/{model}/{id}', 'Admin\MultiImageController@getListImage')->name('admin.image.list');
    Route::post('/admin/image/{model}/{id}', 'Admin\MultiImageController@postUploadImage')->name('admin.image.upload');
    Route::put('/admin/image/{model}/{id}', 'Admin\MultiImageController@putChangePosition')->name('admin.image.position');
    /*Product*/
    Route::resource('/admin/product', 'Admin\ProductController', ['as' => 'admin']);
    Route::get('/admin/search/product', 'Admin\ProductController@search')->name('admin.product.search');
    Route::get('/admin/product/deleted/list', 'Admin\ProductController@getDeletedProduct')->name('admin.product.get_deleted');
    Route::put('/admin/product/deleted/{id}', 'Admin\ProductController@restore')->name('admin.product.restore');
    Route::post('/admin/product/position', 'Admin\ProductController@productPosition')->name('admin.product.product_position');
});

Route::get('/tst', 'Admin\CategoryController@getTest');
require_once 'NamAdmin.php';
require_once 'NguyenAdmin.php';
require_once 'NguyenClient.php';



